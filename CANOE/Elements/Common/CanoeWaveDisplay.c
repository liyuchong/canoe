//
//  CanoeWaveDisplay.c
//  CANOE
//
//  Created by NT on 3/11/15.
//  Copyright (c) 2015 nt. All rights reserved.
//

#include "CanoeWaveDisplay.h"

#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"

static inline float
calculateLevel(const float topMargin, const float bottomMargin,
               const float max, const float min, const float value)
{
    return (value - min) / (max - min) * (topMargin - bottomMargin) + bottomMargin;
}

static inline void
drawSignalCurve(DataEntry *data,
                const int startIndex, const int endIndex,
                const float width, const float leftMargin,
                const float topMargin, float bottomMargin,
                const float signalMax, const float signalMin)
{
    float xRatio = (width - leftMargin - 2 - 10) / (float)(endIndex - startIndex - 1);
    
    setColor(data->r, data->g, data->b);
    glLineWidth(2.0);
    
    lineStripBegin();
    {
        size_t end = endIndex > data->maxLen ? data->maxLen : endIndex;
        
        for(int i = startIndex; i < end; i++)
        {
            addVertex2f((i - startIndex) * xRatio + leftMargin + 2,
                        calculateLevel(topMargin, bottomMargin,
                                       signalMax, signalMin,
                                       data->data[i])
                        );
        }
    }
    lineStripEnd();
}

static inline void
renderSignals(CanoeWaveDisplay *element, float leftMargin, float signalTopMargin, float signalBottomMargin)
{
    // draw signal curve
    setColor(230, 230, 230);
    
    glPushAttrib(GL_LINE_BIT);
    for(int i = 0; i < SV_SIZE(&element->dataList); i++)
    {
        drawSignalCurve(SV_AT(&element->dataList, i), element->startIndex, element->endIndex,
                        element->base.size.x,
                        leftMargin, signalTopMargin, signalBottomMargin,
                        element->max, element->min);
        
        float value = SV_AT(&element->dataList, i)->data[element->endIndex - 1];
        
        // draw signal level
        float signalLevel = calculateLevel(signalTopMargin, signalBottomMargin, element->max, element->min, value);
        float xBias = leftMargin - 10;
        drawLine(xBias - 5, signalLevel - 2.5, xBias, signalLevel);
        drawLine(xBias - 5, signalLevel + 2.5, xBias, signalLevel);
        drawLine(xBias - 5, signalLevel - 2.5, xBias - 5, signalLevel + 2.5);
        
        // latest level
        drawLine(element->base.size.x - 10, signalLevel, element->base.size.x - 5, signalLevel + 3);
        drawLine(element->base.size.x - 10, signalLevel, element->base.size.x - 5, signalLevel - 3);
    }
    glPopAttrib();
}

CanoeWaveDisplay *canoeWaveDisplaySetMinMax(CanoeWaveDisplay *display, const float min, const float max)
{
    display->max = max;
    display->min = min;
    
    SET_DIRTY(display);
    
    return display;
}

#define DIRTY_TEST if(IS_DIRTY(element)) {

#define DIRTY_TEST_END }

void canoeWaveDisplayDraw(CanoeWaveDisplay *element)
{
    pushMatrix();
    
    renderWaveDisplay(element);
    
    
    // TODO: use world transform
    translatef(EXPLODE_COMPONENT2D(element->base.location), 0.0f);
    
    
    int scaleCount = 10;
    
    // draw scale
    scaleCount = 5;
    int minorScale = 2;
    
    if(element->base.size.y > 100)
    {
        scaleCount = 5;
        minorScale = 5;
    }
    if(element->base.size.y > 300)
    {
        scaleCount = 10;
        minorScale = 10;
    }
    if(element->base.size.y > 500)
    {
        scaleCount = 20;
        minorScale = 10;
    }
    
    
    int leftMargin = 65;
    int bottomMargin = 15;
    
    glColor3f(0.1, 0.1, 0.1);
    glBegin(GL_QUADS);
    {
        glVertex2f(leftMargin, bottomMargin);
        glVertex2f(leftMargin, element->base.size.y);
        glVertex2f(element->base.size.x, element->base.size.y);
        glVertex2f(element->base.size.x, bottomMargin);
    }
    glEnd();
    
    setLineWidth(1.0);
    
    int signalLength = element->endIndex - element->startIndex;
    
    // signal range margin
    float signalTopMargin = element->base.size.y - 10;
    float signalBottomMargin = bottomMargin + 15;
    
    
    // draw y signal scale
    for(int i = 0; i < scaleCount; i++)
    {
        float y = (element->max - element->min) * ((float) i / scaleCount) + element->min;
        float yPos = calculateLevel(signalTopMargin, signalBottomMargin, element->max, element->min, y);
        
        setColor(65, 65, 65);
        drawLine(leftMargin + 2, yPos, element->base.size.x, yPos);
        setColor(230, 230, 230);
    }
    
    float xRatio = (float)(element->base.size.x - leftMargin - 2 - 10) / (float)(element->endIndex - element->startIndex);
    
    int sampleScaleCount = 5;
    
    if(element->base.size.x > 500)
        sampleScaleCount = 10;
    if(element->base.size.x > 800)
        sampleScaleCount = 20;
    
    // draw x signal scale
    for(int i = 1; i < sampleScaleCount; i++)
    {
        float sample = (i / (float)sampleScaleCount) * (float)signalLength;
        float x = sample * xRatio + leftMargin;
        
        setColor(65, 65, 65);
        drawLine(x, bottomMargin, x, element->base.size.y);
    }
    
    // draw base line
    if(element->showBaseLine)
    {
        setColor(120, 120, 120);
        float yPos = calculateLevel(signalTopMargin, signalBottomMargin, element->max, element->min, element->baseLineValue);
        drawLine(leftMargin + 2, yPos, element->base.size.x, yPos);
    }
    
    // -------------------------------------------------------------------------
    if(IS_DIRTY(element)) {
    DIRTY_COUNTER_DEC(element);
    
    // left margin
    setColor(123, 123, 123);
    drawLine(leftMargin, 0, leftMargin, element->base.size.y);
    setColor(102, 102, 102);
    drawLine(leftMargin + 1, 0, leftMargin + 1, element->base.size.y);
    
    setColor(102, 102, 102);
    drawRect(0, 0, leftMargin, element->base.size.y);
    
    // bottom margin
    setColor(97, 97, 97);
    drawLine(leftMargin + 1, bottomMargin + 1, element->base.size.x, bottomMargin + 1);
    setColor(102, 102, 102);
    drawLine(leftMargin + 1, bottomMargin, element->base.size.x, bottomMargin);
    setColor(80, 80, 80);
    drawRect(leftMargin + 1, 0, element->base.size.x, bottomMargin);
    setColor(78, 78, 78);
    drawLine(0, 0, element->base.size.x, 0);
    
    // top line
    setColor(123, 123, 123);
    drawLine(0, element->base.size.y - 1, leftMargin, element->base.size.y - 1);
    
    setColor(230, 230, 230);
    

    
    // left signal range indicator
    const float baseLeft = 10;
    drawLine(baseLeft, signalBottomMargin, baseLeft, signalTopMargin);
    drawLine(baseLeft, signalTopMargin, baseLeft + 5, signalTopMargin);
    drawLine(baseLeft, signalBottomMargin, baseLeft + 5, signalBottomMargin);

    
    
    // TODO: change to reference count
//    CanoeFont font;
//    fontClearBackend(&font);
//    fontSetFont(&font, "Helvetica", 9, canoeColor4f(1, 1, 1, 1));
    
    //fontInstanceRender(instance, 10, 10, "123");
    
    for(int i = 0; i < scaleCount + 1; i++)
    {
        char str[128];
        memset(str, 0, 128);
        
        float y = (element->max - element->min) * ((float) i / scaleCount) + element->min;
        float yPos = calculateLevel(signalTopMargin, signalBottomMargin, element->max, element->min, y);
        drawLine(leftMargin - 2, yPos, leftMargin - 8, yPos);
        
        sprintf(str, "%.2f", y);
        
        sharedFontInstanceRender(element->scaleFont, leftMargin - 40, yPos - 3, str, NULL);
    }
    
    //fontRelease(instance);
    
    for(int i = 0; i < minorScale * scaleCount; i++)
    {
        float y = (element->max - element->min) * ((float) i / (minorScale * scaleCount)) + element->min;
        float yPos = calculateLevel(signalTopMargin, signalBottomMargin, element->max, element->min, y);
        drawLine(leftMargin - 2, yPos, leftMargin - 5, yPos);
    }
    
    // sample count scale
    
    setColor(230, 230, 230);
    
    int sampleMinorScaleCount = sampleScaleCount * 10;
    
    // draw 0 sample point
    float zeroSampleX = xRatio * (signalLength) + leftMargin;
    drawLine(zeroSampleX, 5, zeroSampleX, bottomMargin);
    drawLine(zeroSampleX, 5, zeroSampleX - 5, 5);
    drawLine(zeroSampleX, bottomMargin, zeroSampleX - 1.5, bottomMargin - 2.5);
    drawLine(zeroSampleX, bottomMargin, zeroSampleX + 1.5, bottomMargin - 2.5);
    
    sharedFontInstanceRender(element->scaleFont, element->base.size.x - 24, 2, "0", NULL);
    
    for(int i = 1; i < sampleScaleCount; i++)
    {
        char str[128];
        memset(str, 0, 128);
        
        float sample = (i / (float)sampleScaleCount) * (float)signalLength;
        float x = sample * xRatio + leftMargin;
        
        setColor(230, 230, 230);
        drawLine(x , 8, x, bottomMargin);
        
        sprintf(str, "%.0f", signalLength - sample);
        sharedFontInstanceRender(element->scaleFont, x + 2, 2, str, NULL);
    }
    
    setColor(230, 230, 230);
    for(int i = 1; i < sampleMinorScaleCount; i++)
    {
        float sample = (i / (float)sampleMinorScaleCount) * signalLength;
        float x = sample * xRatio + leftMargin;
        
        drawLine(x , 12, x, bottomMargin);
    }
    
}

    renderSignals(element, leftMargin, signalTopMargin, signalBottomMargin);
    
    popMatrix();
    
    // draw children
    
    pushMatrix();
    translatef(EXPLODE_COMPONENT2D(element->base.location), 0.0f);
    
    for(int i = 0; i < SV_SIZE(element->base.children); i++)
    {
        CanoeDrawableBase *base = SV_AT(element->base.children, i);
        base->draw(base);
    }
    
    popMatrix();
    
}


CanoeWaveDisplay *canoeWaveDisplayAddChild(CanoeWaveDisplay *display, void *child)
{
    if(!display)
        return NULL;
    
    /* add child */
    SV_PUSH_BACK(display->base.children, child);
    ((CanoeDrawableBase *)child)->parent = display;
    
    if(((CanoeDrawableBase *)child)->addedToParent)
        ((CanoeDrawableBase *)child)->addedToParent(child, display);
    
    /* add the child to it's parent's indexer */
    spatialIndexerAddElement(&display->base.indexer,
                             &((CanoeDrawableBase *)child)->spatialNode);
    
    return display;
}

CanoeWaveDisplay *canoeWaveDisplaySetSize(CanoeWaveDisplay *display, CanoeVector2f size)
{
    display->base.size = size;
    display->base.boundaryDirty = true;
    
    return display;
}

static void addedTo(void *element, void *parent)
{
    ((CanoeWaveDisplay *)element)->scaleFont = sharedFontRegisterUsage(parent, "Helvetica", 9);
    
    canoeLabelSetFont(((CanoeWaveDisplay *)element)->titleLabel, "Helvetica", 9);
    canoeLabelSetFont(((CanoeWaveDisplay *)element)->sampleRateLabel, "Helvetica", 9);
}

CanoeWaveDisplay *canoeWaveDisplayNew(CanoeVector2f location, CanoeVector2f size)
{
    CanoeWaveDisplay *display = malloc(sizeof(CanoeWaveDisplay));
    CANOE_MALLOC_CHK(display);
    
    canoeDrawableBaseInit(SUPER(display),
                          NULL,
                          (DrawFunction)canoeWaveDisplayDraw,
                          (DestroyFunction)canoeWaveDisplayDestroy,
                          location,
                          size,
                          addedTo,
                          NULL,
                          "WAVE_DISPLAY");
    
    /* initialize data buffer information */
    display->startIndex = -1;
    display->endIndex   = -1;

    SV_INIT(&display->dataList, 2, DataEntry *);
    
    display->showBaseLine = true;
    display->baseLineValue = 0.0;
    
    display->max        = 1.0;
    display->min        = -1.0;
    
    SET_DIRTY(display);
    
//    display->scaleFont = sharedFontRegisterUsage(NULL, "Helvetica", 9);
    
    stringInit(&display->title);
    stringAssign(&display->title, "Untitled");
    
    display->titleLabel = canoeLabelNew(canoeVector2f(5, 14), stringCStr(&display->title));
    display->sampleRateLabel = canoeLabelNew(canoeVector2f(5, 5), "--- Hz");
    
    canoeWaveDisplayAddChild(display, display->titleLabel);
    canoeWaveDisplayAddChild(display, display->sampleRateLabel);
    
    return display;
}


CanoeWaveDisplay *canoeWaveDisplaySetTitle(CanoeWaveDisplay *display, char *title)
{
    canoeLabelSetTitle(display->titleLabel, title);
    
    return display;
}

CanoeWaveDisplay *canoeWaveDisplaySetLocation(CanoeWaveDisplay *display, CanoeVector2f location)
{
    display->base.location = location;
    display->base.boundaryDirty = true;
    canoeWaveDisplayUpdateBoundary(display);
    
    return display;
}

CanoeWaveDisplay *canoeWaveDisplayUpdateViewRange(CanoeWaveDisplay *display, int startIndex, int endIndex)
{
    display->startIndex = startIndex;
    display->endIndex = endIndex;
    
    return display;
}

void canoeWaveDisplayDestroy(CanoeWaveDisplay *display)
{
#ifdef CANOE_LOG_DESTROY
    printf("destroy wave\n");
#endif /* CANOE_LOG_DESTROY */
    
    sharedFontRelease(display->scaleFont->context, display->scaleFont);
    
    for(size_t i = 0; i < SV_SIZE(display->base.children); i++)
    {
        ((CanoeDrawableBase *)SV_AT(display->base.children, i))->destroy(SV_AT(display->base.children, i));
    }
    
    SV_RELEASE(display->base.children);
    free(display->base.children);
}

void canoeWaveDisplayUpdateBoundary(CanoeWaveDisplay *display)
{
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(display->base.location),
                       EXPLODE_COMPONENT2D(canoeVector2fAdd(display->base.location, display->base.size)));
    
    spatialIndexerUpdateBoundary(&display->base.spatialNode, newBoundary);
}
