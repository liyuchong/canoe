/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef __CANOE_BUTTON_H__
#define __CANOE_BUTTON_H__

#include "CanoeDrawableBase.h"
#include "CanoeWindow.h"
#include "CanoeSharedFont.h"

#define CANOE_BUTTON_TITLE_MAX_LENGTH   1024
#define CANOE_BUTTON_DEFAULT_FONT_NAME  "Helvetica"
#define CANOE_BUTTON_DEFAULT_FONT_SIZE  12
#define CANOE_BUTTON_DEFAULT_FONT_COLOR canoeColor4f(1.0, 1.0, 1.0, 1.0)
#define CANOE_BUTTON_DEFAULT_SIZE       canoeVector2f(100, 20)

#define CANOE_BUTTON_STATUS_PRESSED          0x01
#define CANOE_BUTTON_STATUS_IDLE             0x02

#define __CANOE_BUTTON_PRESSED_BODY_COLOR    55
#define __CANOE_BUTTON_IDLE_BODY_COLOR       75

#define __CANOE_BUTTON_PRESSED_BORDER_COLOR  110
#define __CANOE_BUTTON_IDLE_BORDER_COLOR     90

struct CanoeButton;
typedef struct CanoeButton CanoeButton;

struct CanoeButton
{
    EXTEND(CanoeDrawableBase);
    
    CanoeSharedFont *font;
    char title[CANOE_BUTTON_TITLE_MAX_LENGTH];
    
    int status;
};

/**
 *  Create a new instance of button
 *
 *  @param location Location of the button
 *  @param string   Caption of the button
 *
 *  @return A pointer to the new button
 */
CanoeButton *canoeButtonNew(CanoeVector2f location, char *string);

/**
 *  Set font of a given button
 *
 *  @param button A pointer to the button
 *  @param font   Font name
 *  @param size   Font size
 *
 *  @return A pointer to the input button
 */
CanoeButton *canoeButtonSetFont(CanoeButton *button, char *font, int size);

CanoeButton *canoeButtonSetSize(CanoeButton *button, CanoeVector2f size);

/**
 *  Update bounding box, which is a spatial identifier of the label
 *
 *  @param label The pointer to the label
 */
void canoeButtonUpdateBoundingBox(CanoeButton *button);

/**
 *  Draw function, called by the renderer
 *
 *  @param label The pointer to the label
 */
void canoeButtonDraw(CanoeButton *button);

/**
 *  Clean allocated memory
 *
 *  @param button A pointer to the button
 */
void canoeButtonDestroy(CanoeButton *button);

#endif /* defined(__CANOE_BUTTON_H__) */
