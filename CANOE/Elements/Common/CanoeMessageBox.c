/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeMessageBox.h"

static void okPressed(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_MOUSE_BUTTON_EXTRACT_ARGUMENTS(
        CanoeButton *, button, pos, mouseButton, act, mods
    );
    
    if(mouseButton == CANOE_MOUSE_BUTTON_LEFT && act == CANOE_MOUSE_RELEASE)
    {
        canoeWindowClose(button->base.parent);
    }
}

CanoeMessageBox *canoeMessageBoxNew(char *title, char *message)
{
    CanoeMessageBox *mbox = malloc(sizeof(CanoeMessageBox));
    CANOE_MALLOC_CHK(mbox);
    
    mbox->window = canoeWindowNew(title, CANOE_MESSAGE_BOX_DEFAULT_SIZE, canoeVector2f(10, 10));
    SV_PUSH_BACK(&rootWindowContainer, mbox->window);
    
    CanoeVector2f windowLocation = {
        .x = SCREEN_RESOLUTION.x / 2 - mbox->window->base.size.x / 2,
        .y = SCREEN_RESOLUTION.y / 2 - mbox->window->base.size.y / 2,
    };
    
    canoeWindowSetLocation(mbox->window, windowLocation);
    
    mbox->okButton = canoeButtonNew(canoeVector2f(420 / 2 - 50, 20), "OK");
    canoeAddChild(&mbox->window->base, mbox->okButton);
    
    mbox->messageLabel = canoeLabelNew(canoeVector2f(20, 100), message);
    canoeAddChild(&mbox->window->base, mbox->messageLabel);
    
    canoeRegisterEvent(mbox->okButton, CANOE_EVENT_MOUSE_BUTTON_ACTION, okPressed);
    
    return mbox;
}
