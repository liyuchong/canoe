/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeTextBox.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"
#include <math.h>

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeTextBox, ele)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(ele->base.location),
                       EXPLODE_COMPONENT2D(canoeVector2fAdd(ele->base.location, ele->base.size))
                       );
    
    spatialIndexerUpdateBoundary(&ele->base.spatialNode, newBoundary);
}

static void addedTo(void *element, void *parent)
{
    ((CanoeTextBox *)element)->font = sharedFontRegisterUsage(parent, "Helvetica", 12);
}

static void gotFocus(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_FOCUS_CHANGED_EXTRACT_ARGUMENTS(CanoeTextBox, ele, status);
    SET_DIRTY(ele);
}

static void charPressed(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_CHAR_PRESSED_EXTRACT_ARGUMENTS(CanoeTextBox, ele, key);
    
    char ch = (char)key;
    strncat(ele->str, &ch, 1);
    
    SET_DIRTY(ele);
}

static void keyPressed(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_KEY_ACTION_EXTRACT_ARGUMENTS(CanoeTextBox *, ele, key, code, action, mod);
    
    SET_DIRTY(ele);
    
    if(action == CANOE_KEY_PRESSED)
    {
        if(key == 259)
        {
            if(strlen(ele->str) > 0)
                ele->str[strlen(ele->str) - 1] = '\0';
        }
    }
    
}

CanoeTextBox *canoeTextBoxNew(CanoeVector2f location)
{
    CanoeTextBox *ele = malloc(sizeof(CanoeTextBox));
    CANOE_MALLOC_CHK(label);
    
    canoeDrawableBaseInit(
                          &ele->base,
                          NULL,
                          (DrawFunction)CANOE_FOUNDATION_FUNC_DRAW(CanoeTextBox),
                          (DestroyFunction)CANOE_FOUNDATION_FUNC_DESTROY(CanoeTextBox),
                          location,
                          canoeVector2f(80, 20),
                          addedTo,
                          NULL,
                          "TEXTB"
                          );

    canoeRegisterEvent(ele, CANOE_EVENT_INPUT_FOCUS_CHANGED, gotFocus);
    canoeRegisterEvent(ele, CANOE_EVENT_KEY_ACTION, keyPressed);
    canoeRegisterEvent(ele, CANOE_EVENT_CHAR_ACTION, charPressed);
    
    ele->counter = 0;
    
    SET_DIRTY(ele);
    
    strcpy(ele->str, "");
    
    return ele;
}

CANOE_FOUNDATION_FUNC_DRAW_DECL(CanoeTextBox, ele)
{
    if(ele->base.boundaryDirty == true)
    {
        ele->base.boundaryDirty = false;
        CANOE_FOUNDATION_FUNC_UPDATE_BBOX(CanoeTextBox)(ele);
    }
    
    ele->counter++;
    
    if(ele->counter > 60)
        ele->counter = 0;
    
    bool focused = &ele->base == currentInputFocusedElement;

    if(((ele->counter == 0 || ele->counter == 30) && focused))
    {
        SET_DIRTY(ele);
    }
    
    if(IS_DIRTY(ele))
    {
        DIRTY_COUNTER_DEC(ele);

        glPushAttrib(GL_LINE_BIT);
        glPushMatrix();
        {
            CanoeVector2f world = canoeCalculateWorldCoordinate((CanoeDrawableBase *)ele);
            glTranslatef(world.x, world.y, 0.0);
            
            glEnable(GL_SCISSOR_TEST);
            canoeScissor(world.x, world.y, SUPER(ele)->size.x, SUPER(ele)->size.y);
            
            
            // --------
            glClear(GL_COLOR_BUFFER_BIT);
            
            
            /* draw text box background */
            
            if(focused)
                setColor(40, 40, 40);
            else
                setColor(45, 45, 45);
            
            drawRect(0, 0, ele->base.size.x, ele->base.size.y);
            
            /* draw rect */
            
            if(focused)
                setColorGray(120);
            else
                setColor(100, 100, 100);
            
            drawLine(0, 0, ele->base.size.x, 0);
            drawLine(0, ele->base.size.y, ele->base.size.x, ele->base.size.y);
            drawLine(0, 0, 0, ele->base.size.y);
            drawLine(ele->base.size.x, 0, ele->base.size.x, ele->base.size.y);
            
            CanoeVector2f lower, upper;
            sharedFontCalculateBoundingBox(ele->font, ele->str, &lower, &upper);
            
            sharedFontInstanceRender(ele->font, 5,
                                     (ele->base.size.y - upper.y - lower.y) / 2.0,
                                     ele->str, NULL);
            
            if(focused && ele->counter > 20)
            {
                setLineWidth(2.0);
                setColor(220, 220, 220);
                drawLine(6 + upper.x, 5, 6 + upper.x, ele->base.size.y - 5);
            }
            
            glDisable(GL_SCISSOR_TEST);
        }
        glPopMatrix();
        glPopAttrib();
    }
    
    //drawBoundary(ele);
}

char *canoeTextBoxGetString(CanoeTextBox *ele, char *buf)
{
    strcpy(buf, ele->str);
    return buf;
}

char *canoeTextBoxSetString(CanoeTextBox *ele, char *buf)
{
    SET_DIRTY(ele);
    strcpy(ele->str, buf);
    return buf;
}

CANOE_FOUNDATION_FUNC_DESTROY_DECL(CanoeTextBox, ele)
{
#ifdef CANOE_LOG_DESTROY
    printf("textbox destroy\n");
#endif /* CANOE_LOG_DESTROY */
    
    sharedFontRelease(ele->font->context, ele->font);
    
    SV_RELEASE(ele->base.children);
    free(ele->base.children);
}
