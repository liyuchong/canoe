/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_LABEL_H__
#define CANOE_LABEL_H__

#include "CanoeDrawableBase.h"
#include "CanoeWindow.h"
#include "CanoeSharedFont.h"

#define CANOE_LABEL_TITLE_MAX_LENGTH   1024
#define CANOE_LABEL_DEFAULT_FONT_NAME  "Helvetica"
#define CANOE_LABEL_DEFAULT_FONT_SIZE  12
#define CANOE_LABEL_DEFAULT_FONT_COLOR canoeColor4f(1.0, 1.0, 1.0, 1.0)

struct CanoeLabel;
typedef struct CanoeLabel CanoeLabel;

struct CanoeLabel
{
    EXTEND(CanoeDrawableBase);
    
    CanoeSharedFont *font;
    char title[CANOE_LABEL_TITLE_MAX_LENGTH];
};

/* Memory allocation & free */
CanoeLabel *canoeLabelNew(CanoeVector2f location, char *string);

/* Propeties get & set */

/**
 *  Set the font of a given label
 *
 *  @param label    The pointer to the label
 *  @param fontName Font name
 *  @param size     Font size
 *
 *  @return The pointer to the input label
 */
CanoeLabel *canoeLabelSetFont(CanoeLabel *label, char *fontName, int size);

/**
 *  Set the color of font of a given label
 *
 *  @param label The pointer to the label
 *  @param color The color
 *
 *  @return The pointer to the input label
 */
CanoeLabel *canoeLabelSetFontColor(CanoeLabel *label, CanoeColor4f color);

/**
 *  Set the location of a given label
 *
 *  @param label    The pointer to the label
 *  @param location The location
 *
 *  @return The pointer to the input label
 */
CanoeLabel *canoeLabelSetLocation(CanoeLabel *label, CanoeVector2f location);

/**
 *  Set the title of a given label
 *
 *  @param label The pointer to the label
 *  @param title The title
 *
 *  @return The pointer to the input label
 */
CanoeLabel *canoeLabelSetTitle(CanoeLabel *label, char *title);

/**
 *  Update bounding box, which is a spatial identifier of the label
 *
 *  @param label The pointer to the label
 */
void canoeLabelUpdateBoundingBox(CanoeLabel *label);

/**
 *  Draw function, called by the renderer
 *
 *  @param label The pointer to the label
 */
void canoeLabelDraw(CanoeLabel *label);

void canoeLabelDestroy(CanoeLabel *label);

#endif /* defined(CANOE_LABEL_H__) */
