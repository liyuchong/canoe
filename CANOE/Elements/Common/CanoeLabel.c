/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeLabel.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"

void canoeLabelUpdateBoundingBox(CanoeLabel *label)
{
    if(!label->font->instance)
        return;
    
    CanoeVector2f lower, upper;
    sharedFontCalculateBoundingBox(label->font, label->title, &lower, &upper);
    
    label->base.size = upper;
    
    /* update boundary */
    SpatialBoundary newBoundary =
        spatialBoundaryNew(2,
            EXPLODE_COMPONENT2D(canoeVector2fAdd(label->base.location, lower)),
            EXPLODE_COMPONENT2D(canoeVector2fAdd(label->base.location, upper))
    );
    
    spatialIndexerUpdateBoundary(&label->base.spatialNode, newBoundary);
}

static void addedTo(void *element, void *parent)
{
    CanoeLabel *label = (CanoeLabel *)element;
    
    label->font = sharedFontRegisterUsage(parent,
                                          CANOE_LABEL_DEFAULT_FONT_NAME,
                                          CANOE_LABEL_DEFAULT_FONT_SIZE);
}

CanoeLabel *canoeLabelNew(CanoeVector2f location, char *string)
{
    CanoeLabel *label = malloc(sizeof(CanoeLabel));
    CANOE_MALLOC_CHK(label);
    
    canoeDrawableBaseInit(
        &label->base,
        NULL,
        (DrawFunction)canoeLabelDraw,
        (DestroyFunction)canoeLabelDestroy,
        location,
        canoeVector2f(0, 0),
        addedTo,
        NULL,
        "LABEL"
    );

    canoeLabelSetTitle(label, string);
    
    label->font = NULL;
    SET_DIRTY(label);
    
    return label;
}

void canoeLabelDraw(CanoeLabel *label)
{
    if(label->base.boundaryDirty == true && label->font && label->font->instance)
    {
        label->base.boundaryDirty = false;
        canoeLabelUpdateBoundingBox(label);
    }
    
    if(IS_DIRTY(label))
    {
        DIRTY_COUNTER_DEC(label);
        
        /* render font. if the font is (re)loaded, update the bounding box */
        sharedFontInstanceRender(
            label->font,
            label->base.location.x,
            label->base.location.y,
            label->title,
            ^(CanoeSharedFont *font) {
                canoeLabelUpdateBoundingBox(label);
        });
    }
}

void canoeLabelDestroy(CanoeLabel *label)
{
#ifdef CANOE_LOG_DESTROY
    printf("label destroy\n");
#endif /* CANOE_LOG_DESTROY */
    
    sharedFontRelease(label->font->context, label->font);
    SV_RELEASE(label->base.children);
    free(label->base.children);
}

CanoeLabel *canoeLabelSetFont(CanoeLabel *label, char *fontName, int size)
{
    SET_DIRTY(label);
    
    if(label->font)
    {
        label->font = sharedFontSetFont(label->font->context, &label->font, fontName, size);
    }
    return label;
}

CanoeLabel *canoeLabelSetTitle(CanoeLabel *label, char *title)
{
    SET_DIRTY(label);
    
    strcpy(label->title, title);
    
//    int lastPos = 0;
//    
//    for(int i = 0; i < strlen(label->title); i++)
//    {
//        if(label->title[i] == '\n')
//        {
//            char *line = malloc(i - lastPos + 1);
//            memcpy(line, label->title + lastPos, i - lastPos);
//            lastPos = i;
//            
//            SV_PUSH_BACK(&label->lines, line);
//        }
//    }
//    
//    char *line = malloc(strlen(label->title) - lastPos + 1);
//    memcpy(line, label->title + lastPos, strlen(label->title) - lastPos);
//    SV_PUSH_BACK(&label->lines, line);
    
    label->base.boundaryDirty = true;
    
    return label;
}

CanoeLabel *canoeLabelSetFontColor(CanoeLabel *label, CanoeColor4f color)
{
    SET_DIRTY(label);
    sharedFontSetColor(NULL, label->font, color);
    
    return label;
}

CanoeLabel *canoeLabelSetLocation(CanoeLabel *label, CanoeVector2f location)
{
    SET_DIRTY(label);
    label->base.location = location;
    label->base.boundaryDirty = true;
    
    return label;
}
