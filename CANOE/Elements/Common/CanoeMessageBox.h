/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_MESSAGE_BOX_H__
#define CANOE_MESSAGE_BOX_H__

#include "CanoeDrawableBase.h"
#include "CanoeWindow.h"
#include "CanoeSharedFont.h"
#include "CanoeButton.h"
#include "CanoeLabel.h"

#define CANOE_MESSAGE_BOX_TITLE_MAX_LENGTH   1024
#define CANOE_MESSAGE_BOX_DEFAULT_FONT_NAME  "Helvetica"
#define CANOE_MESSAGE_BOX_DEFAULT_FONT_SIZE  12
#define CANOE_MESSAGE_BOX_DEFAULT_FONT_COLOR canoeColor4f(1.0, 1.0, 1.0, 1.0)
#define CANOE_MESSAGE_BOX_DEFAULT_SIZE       canoeVector2f(420, 140)

struct CanoeMessageBox;
typedef struct CanoeMessageBox CanoeMessageBox;

struct CanoeMessageBox
{
    CanoeDrawableBase base;
    
    CanoeWindow *window;
    CanoeButton *okButton;
    CanoeLabel *messageLabel;
    
    CanoeSharedFont *font;
    char title[CANOE_MESSAGE_BOX_TITLE_MAX_LENGTH];
    char message[CANOE_MESSAGE_BOX_TITLE_MAX_LENGTH];
};

/**
 *  Create a new instance of button
 *
 *  @param location Location of the button
 *  @param string   Caption of the button
 *
 *  @return A pointer to the new button
 */
CanoeMessageBox *canoeMessageBoxNew(char *title, char *message);

/**
 *  Draw function, called by the renderer
 *
 *  @param label The pointer to the label
 */
void canoeMessageBoxDraw(CanoeMessageBox *button);

/**
 *  Clean allocated memory
 *
 *  @param button A pointer to the button
 */
void canoeMessageBoxDestroy(CanoeMessageBox *button);

#endif /* defined(CANOE_MESSAGE_BOX_H__) */
