//
//  CanoeWaveDisplay.h
//  CANOE
//
//  Created by NT on 3/11/15.
//  Copyright (c) 2015 nt. All rights reserved.
//

#ifndef __CANOE__CanoeWaveDisplay__
#define __CANOE__CanoeWaveDisplay__

#include <stdio.h>

#include "CanoeDrawableBase.h"
#include "CanoeWindow.h"
#include "CanoeFont.h"
#include "CanoeSharedFont.h"
#include "SimpleString.h"
#include "CircularBuffer.h"
#include "CanoeLabel.h"

typedef struct
{
    float *data;
    size_t maxLen;
    uint8_t r, g, b;
} DataEntry;

typedef struct
{
    EXTEND(CanoeDrawableBase);
    
    int startIndex, endIndex;
    
    VECTOR(DataEntry *) dataList;
    
    CanoeSharedFont *scaleFont;
    
    float min, max;
    
    SimpleString title;
    
    CanoeLabel *titleLabel, *sampleRateLabel;
    
    bool showBaseLine;
    float baseLineValue;
    
} CanoeWaveDisplay;

void canoeWaveDisplayDraw(CanoeWaveDisplay *element);

CanoeWaveDisplay *canoeWaveDisplaySetTitle(CanoeWaveDisplay *display, char *title);

CanoeWaveDisplay *canoeWaveDisplaySetMinMax(CanoeWaveDisplay *display, const float min, const float max);

CanoeWaveDisplay *canoeWaveDisplayNew(CanoeVector2f location, CanoeVector2f size);

CanoeWaveDisplay *canoeWaveDisplaySetLocation(CanoeWaveDisplay *display, CanoeVector2f location);

CanoeWaveDisplay *canoeWaveDisplaySetSize(CanoeWaveDisplay *display, CanoeVector2f size);

CanoeWaveDisplay *canoeWaveDisplayUpdateViewRange(CanoeWaveDisplay *display, int startIndex, int endIndex);

void canoeWaveDisplayDestroy(CanoeWaveDisplay *display);

void canoeWaveDisplayUpdateBoundary(CanoeWaveDisplay *display);

#endif /* defined(__CANOE__CanoeWaveDisplay__) */
