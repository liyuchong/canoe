/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeButton.h"
#include "CanoeGLUtils.h"

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeButton, ele)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(ele->base.location),
                       EXPLODE_COMPONENT2D(canoeVector2fAdd(ele->base.location, ele->base.size))
                       );
    
    spatialIndexerUpdateBoundary(&ele->base.spatialNode, newBoundary);
}

static void mousePressed(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_MOUSE_BUTTON_EXTRACT_ARGUMENTS(CanoeButton *, button, pos, but, act, mods);
 
    SET_DIRTY(button);
    
    if(but == CANOE_MOUSE_BUTTON_LEFT)
    {
        if(act == CANOE_MOUSE_PRESS)
            button->status = CANOE_BUTTON_STATUS_PRESSED;
        else
            button->status = CANOE_BUTTON_STATUS_IDLE;
    }
}

static void keyAction(CANOE_EVENT_ELEMENT, ...)
{

}

static void addedTo(void *element, void *parent)
{
    CanoeButton *button = (CanoeButton *)element;
    
    /* register font use */
    button->font = sharedFontRegisterUsage(
        parent,
        CANOE_BUTTON_DEFAULT_FONT_NAME,
        CANOE_BUTTON_DEFAULT_FONT_SIZE
    );
}

CanoeButton *canoeButtonNew(CanoeVector2f location, char *string)
{
    /* allocate memory for the new element */
    CanoeButton *button = malloc(sizeof(CanoeButton));
    CANOE_MALLOC_CHK(button);
    
    canoeDrawableBaseInit(&button->base,
                          NULL,
                          (DrawFunction)canoeButtonDraw,
                          (DestroyFunction)canoeButtonDestroy,
                          location,
                          CANOE_BUTTON_DEFAULT_SIZE,
                          addedTo,
                          NULL,
                          "BUTTON");
    
    /* reset button status */
    button->status = CANOE_BUTTON_STATUS_IDLE;
    
    /* copy button caption */
    strcpy(button->title, string);
    
    /* rebuild bounding box */
    canoeButtonUpdateBoundingBox(button);
    
    /* register a mouse action event so that the status of the button could
     * be modified accordingly. */
    canoeRegisterEvent(button, CANOE_EVENT_MOUSE_BUTTON_ACTION, mousePressed);
    canoeRegisterEvent(button, CANOE_EVENT_KEY_ACTION, keyAction);
    
    SET_DIRTY(button);
    
    return button;
}

CanoeButton *canoeButtonSetFont(CanoeButton *button, char *font, int size)
{
    SET_DIRTY(button);
    
    sharedFontSetFont(NULL, &button->font, font, size);
    
    return button;
}

CanoeButton *canoeButtonSetSize(CanoeButton *button, CanoeVector2f size)
{
    SET_DIRTY(button);
    
    button->base.size = size;
    button->base.boundaryDirty = true;
    
    return button;
}

void canoeButtonDraw(CanoeButton *button)
{
    CANOE_FOUNDATION_CHECK_REBUILD_INDEX(CanoeButton, button);
 
    if(IS_DIRTY(button))
    {
        DIRTY_COUNTER_DEC(button);
        
        CanoeVector2f lower = button->base.location;
        CanoeVector2f upper = canoeVector2fAdd(lower, button->base.size);
        
        if(button->status == CANOE_BUTTON_STATUS_IDLE)
            setColorGray(__CANOE_BUTTON_IDLE_BODY_COLOR);
        else
            setColorGray(__CANOE_BUTTON_PRESSED_BODY_COLOR);
        
        drawRect(EXPLODE_COMPONENT2D(button->base.location),
                 EXPLODE_COMPONENT2D(canoeVector2fAdd(button->base.location, button->base.size)));
        
        if(button->status == CANOE_BUTTON_STATUS_IDLE)
            setColorGray(__CANOE_BUTTON_IDLE_BORDER_COLOR);
        else
            setColorGray(__CANOE_BUTTON_PRESSED_BORDER_COLOR);
        
        lineStripBegin();
        {
            addVertex2f(lower.x, lower.y);
            addVertex2f(lower.x, upper.y);
            addVertex2f(upper.x, upper.y);
            addVertex2f(upper.x, lower.y);
            addVertex2f(lower.x, lower.y);
        }
        lineStripEnd();
        
        sharedFontCalculateBoundingBox(button->font, button->title, &lower, &upper);
        
        sharedFontInstanceRender(
            button->font,
            button->base.location.x + button->base.size.x / 2.0 - (upper.x - lower.x) / 2.0,
            button->base.location.y + button->base.size.y / 2.0 - (upper.y - lower.y) / 2.0,
            button->title,
            NULL
        );
    }
}

void canoeButtonUpdateBoundingBox(CanoeButton *button)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
        EXPLODE_COMPONENT2D(button->base.location),
        EXPLODE_COMPONENT2D(canoeVector2fAdd(button->base.location, button->base.size)));
    
    spatialIndexerUpdateBoundary(&button->base.spatialNode, newBoundary);
}

void canoeButtonDestroy(CanoeButton *button)
{
#ifdef CANOE_LOG_DESTROY
    printf("canoe button destroy\n");
#endif /* CANOE_LOG_DESTROY */
    
    sharedFontRelease(button->font->context, button->font);
    SV_RELEASE(button->base.children);
    free(button->base.children);
}
