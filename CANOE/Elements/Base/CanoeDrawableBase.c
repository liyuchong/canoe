/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeDrawableBase.h"

CanoeDrawableBase *canoeDrawableBaseInit(
        CanoeDrawableBase *base,
        CanoeDrawableBase *parent,
        DrawFunction      draw,
        DestroyFunction   destroy,
        CanoeVector2f     location,
        CanoeVector2f     size,
        AddedToFunction   addedToParent,
        RemovedFromFunction removedFrom,
        char *elementName
) {
    /* copy propeties and event functions */
    base->parent        = parent;
    base->location      = location;
    base->size          = size;
    base->draw          = draw;
    base->destroy       = destroy;
    base->addedToParent = addedToParent;
    base->removedFrom   = removedFrom;
    
    /* initialize the spatial indexer for the current node, will contain the
     * children nodes */
    spatialIndexerInit(&base->indexer, 2);
    
    /* initialize the spatial node, 
     * we don't know the boundary yet, so assign 0. */
    spatialNodeInit(&base->spatialNode,
                    spatialBoundaryNew(2, 0.0, 0.0, 0.0, 0.0),
                    base);
    
    /* copy element name */
    strncpy(base->elementName, elementName, CANOE_BASE_NAME_LENGTH);
    
    /* force update boundary */
    base->boundaryDirty = true;
    
    /* allocate memory for childern vector */
    base->children = malloc(sizeof(VECTOR(void *)));
    CANOE_MALLOC_CHK(base->children);
    SV_INIT(base->children, 2, void *);
    
    return base;
}

void canoeAddChild(CanoeDrawableBase *parent, void *child)
{
    /* check input pointers */
    if(!parent || !child)
        return;
    
    /* add child to the parent's children container */
    SV_PUSH_BACK(parent->children, child);
    
    /* assign parent and copy propeties */
    ((CanoeDrawableBase *)child)->parent = parent;
    ((CanoeDrawableBase *)child)->dpiRatio = parent->dpiRatio;
    
    /* call addedToParent callback function, if there is one */
    if(((CanoeDrawableBase *)child)->addedToParent)
        ((CanoeDrawableBase *)child)->addedToParent(child, parent);
    
    /* update spatial indexer */
    spatialIndexerAddElement(
        &parent->indexer,
        &((CanoeDrawableBase *)child)->spatialNode
    );
}

void canoeRenderChildren(CanoeDrawableBase *base)
{
    /* run collision test, find all elements to be rendered */
    VECTOR(SpatialNode *) nodes;
    SV_INIT(&nodes, 2, SpatialNode *);
    
    // TODO: change search API?
    SpatialBoundary searchBoundary =
    spatialBoundaryNew(
        2,
        EXPLODE_COMPONENT2D(canoeVector2f(0.0, 0.0)),
        EXPLODE_COMPONENT2D(base->size)
    );
    
    spatialIndexerSearch(
        (SpatialNodes *)&nodes, &base->indexer, &searchBoundary
    );
    
    spatialBoundaryDestroy(&searchBoundary);
    
    /* render them */
    for(size_t i = 0; i < SV_SIZE(&nodes); i++)
    {
        ((CanoeDrawableBase *)SV_AT(&nodes, i)->data)->draw(
            SV_AT(&nodes, i)->data
        );
    }
    
    SV_RELEASE(&nodes);
}
