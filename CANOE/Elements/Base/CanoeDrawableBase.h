/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_DRAWABLE_BASE_H__
#define CANOE_DRAWABLE_BASE_H__

#include <stdbool.h>

#include "CanoeFoundation.h"
#include "SpatialIndexer.h"

/* Draw function, will be called everytime when the element is being drawn */
typedef void (*DrawFunction)        (void *);

/* Destroy function, will be called when the element needs to be released  */
typedef void (*DestroyFunction)     (void *);

/* Added to function, will be called when the element is added to a parent */
typedef void (*AddedToFunction)     (void *, void *);

/* Removed from function, 
 * will be called when the element is removed to a parent */
typedef void (*RemovedFromFunction) (void *, void *);

#define EXTEND(__super) __super base;

#define SUPER(__obj)    (&((__obj)->base))

#define SET_DIRTY(_ele) \
do { \
    (_ele)->base.dirty = 3; \
} while(0)

#define IS_DIRTY(_ele)  (_ele)->base.dirty > 0

#define DIRTY_COUNTER_DEC(_ele) (_ele)->base.dirty--

/**
 *  Declare a CANOE element
 *
 *  @param element_type Type name of the element
 */
#define CANOE_DECLARE_ELEMENT(element_type)         \
    struct element_type;                            \
    typedef struct element_type element_type;       \
    struct element_type                             \

/** bounding box update */
/* function name */
#define CANOE_FOUNDATION_FUNC_UPDATE_BBOX(element_type)                     \
    __canoe_##element_type##UpdateBoundingBox
/* declare */
#define CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(element_type, element_name)  \
    void CANOE_FOUNDATION_FUNC_UPDATE_BBOX(element_type)                    \
    (element_type *element_name)

/** draw function */
/* function name */
#define CANOE_FOUNDATION_FUNC_DRAW(element_type)                            \
    __canoe_##element_type##Draw
/* declare */
#define CANOE_FOUNDATION_FUNC_DRAW_DECL(element_type, element_name)         \
    void CANOE_FOUNDATION_FUNC_DRAW(element_type)                           \
    (element_type *element_name)

/** destroy function */
/* function name */
#define CANOE_FOUNDATION_FUNC_DESTROY(element_type)                         \
    __canoe_##element_type##Destroy
/* declare */
#define CANOE_FOUNDATION_FUNC_DESTROY_DECL(element_type, element_name)      \
    void CANOE_FOUNDATION_FUNC_DESTROY(element_type)                        \
    (element_type *element_name)
    
/**
 *  Drawable element base structure.
 *  This is the base structure of every drawable elements, the first member of
 *  The actual element. It provides the ability to access the element's 
 *  propeties without the knowledge of the type of different elements.
 */
typedef struct
{
    DrawFunction            draw;           /* draw function                */
    DestroyFunction         destroy;        /* destroy function             */
    
    CanoeVector2f           location;       /* the location of the element  */
    CanoeVector2f           size;           /* the size of the element      */
    
    SpatialIndexer          indexer;        /* the pointer to the indexer   */
    SpatialNode             spatialNode;    /* the spatial indexer node     */
    
    void                    *parent;        /* element's parent element     */
    VECTOR(void *)          *children;      /* element's children elements  */
    
    AddedToFunction         addedToParent;  /* the element is added to...   */
    RemovedFromFunction     removedFrom;    /* the element is removed       */
    
    bool                    boundaryDirty;  /* a flag which indicates if the
                                             * boundary is dirty            */
    char
    elementName[CANOE_BASE_NAME_LENGTH];    /* name of the elemt            */
    
    double                  dpiRatio;       /* DPI ratio of the element     */
    
    int dirty;
    
} CanoeDrawableBase;

/**
 *  Calculate if the given point is inside the given boundary(inclusively) in
 *  2-dimensional space.
 *
 *  @param boundary The 2d boundary
 *  @param point    The 2d point
 *
 *  @return Returns true if is inside, false otherwise.
 */
static __INLINE__
bool canoePointInSpatialBoundary2d(
    const SpatialBoundary boundary, const CanoeVector2f point
) {
    return (
        point.x > boundary.lower[0] &&
        point.x < boundary.upper[0] &&
        point.y > boundary.lower[1] &&
        point.y < boundary.upper[1]
    );
}

/**
 *  Calculates the world coordinate of the given element
 *
 *  @param base A pointer to the base of an element
 *
 *  @return World coordinate
 */
static __INLINE__
CanoeVector2f canoeCalculateWorldCoordinate(CanoeDrawableBase *base)
{
    CanoeVector2f world = canoeVector2f(0.0, 0.0);
    CanoeDrawableBase *parent = base;
    
    while(parent->parent)
    {
        world = canoeVector2fAdd(world, parent->location);
        parent = parent->parent;
    }
    
    return world;
}

/**
 *  Check if the boundary of an element is dirty, if it is, then
 *  update the boundary using the registered update function and rebuild
 *  the spacial indexer.
 *
 *  @param element_type Type of element
 *  @param element_name Name of element
 */
#define CANOE_FOUNDATION_CHECK_REBUILD_INDEX(element_type, element_name)    \
do {                                                                        \
    if((element_name)->base.boundaryDirty == true)                          \
    {                                                                       \
        (element_name)->base.boundaryDirty = false;                         \
        CANOE_FOUNDATION_FUNC_UPDATE_BBOX(element_type)((element_name));    \
    }                                                                       \
} while(0)

/**
 *  Initialize the base structure
 *
 *  @param base     The pointer to the base structure
 *  @param parent   Parent of the element
 *  @param draw     The draw function
 *  @param destroy  The destroy function
 *  @param location Location of the element
 *  @param size     Size of the element
 *
 *  @return The pointer to the input bas structure
 */
CanoeDrawableBase *canoeDrawableBaseInit(
    CanoeDrawableBase   *base,
    CanoeDrawableBase   *parent,
    DrawFunction        draw,
    DestroyFunction     destroy,
    CanoeVector2f       location,
    CanoeVector2f       size,
    AddedToFunction     addedToParent,
    RemovedFromFunction removedFrom,
    char *elementName
);

/**
 *  Add a child to a parent
 *
 *  @param parent The pointer to the parent's base
 *  @param child  The pointer to the child to be added
 */
void canoeAddChild(CanoeDrawableBase *parent, void *child);

/**
 *  Render all children within the visible range
 *
 *  @param base The pointer to the drawable base
 */
void canoeRenderChildren(CanoeDrawableBase *base);

#endif /* defined(CANOE_DRAWABLE_BASE_H__) */
