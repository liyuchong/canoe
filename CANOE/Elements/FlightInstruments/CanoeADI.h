//
//  CanoeADI.h
//  CANOE
//
//  Created by NT on 3/21/15.
//  Copyright (c) 2015 nt. All rights reserved.
//

#ifndef __CANOE__CanoeADI__
#define __CANOE__CanoeADI__

#include <stdio.h>
#include "CanoeFoundation.h"
#include "CanoeDrawableBase.h"
#include "CanoeSharedFont.h"

typedef struct
{
    CanoeDrawableBase base;
    
    CanoeSharedFont *font, *hintFont;
    
    float airSpeed;
    float altitude;
    
    float yaw, pitch, roll;
    
    float verticalSpeed;
    
    /* altimeter refrence pressure(mmHg) */
    float altimeter;
    
    /* pre-set targets */
    float targetAltitude;
    float targetAirSpeed;
    
    /* status bar hints */
    char hintLeft[128];
    char hintMiddle[128];
    char hintRight[128];
    
} CanoeADI;

CanoeADI *canoeAdiNew(CanoeVector2f location, CanoeVector2f size);

void canoeAdiUpdateBoundingBox(CanoeADI *adi);

void canoeAdiDraw(CanoeADI *adi);

void canoeAdiDestroy(CanoeADI *adi);

#endif /* defined(__CANOE__CanoeADI__) */
