/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeFlightMap.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"
#include "CanoeButton.h"

#include <math.h>

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeFlightMap, label)
{
    /* update boundary */
    SpatialBoundary newBoundary =
        spatialBoundaryNew(2,
            EXPLODE_COMPONENT2D(label->base.location),
            EXPLODE_COMPONENT2D(canoeVector2fAdd(label->base.location, label->base.size))
    );
    
    spatialIndexerUpdateBoundary(&label->base.spatialNode, newBoundary);
}

static void addedTo(void *element, void *parent)
{
    CanoeFlightMap *label = (CanoeFlightMap *)element;
    
    label->font = sharedFontRegisterUsage(parent, "Helvetica", 9);
    label->hintFont = sharedFontRegisterUsage(parent, "Helvetica", 9);
    label->entFont = sharedFontRegisterUsage(parent, "Helvetica", 8);
    
    sharedFontSetColor(NULL, label->hintFont, canoeColor4f(0.0, 1.0, 0.0, 1.0));
    sharedFontSetColor(NULL, label->entFont, canoeColor4f(0.0, 1.0, 1.0, 1.0));
}

static void zoomInAction(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_MOUSE_BUTTON_EXTRACT_ARGUMENTS(CanoeButton *, button, pos, but, act, mods);
    
    if(act == CANOE_MOUSE_PRESS)
    {
        CanoeFlightMap *map = button->base.parent;
        
        if(map->rangeScale <= 100)
        {
            map->rangeScale -= 10;
        }
        else
        {
            map->rangeScale -= 100;
        }
        
        if(map->rangeScale < 10)
            map->rangeScale = 10;
    }
}

static void zoomOutAction(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_MOUSE_BUTTON_EXTRACT_ARGUMENTS(CanoeButton *, button, pos, but, act, mods);
    
    if(act == CANOE_MOUSE_PRESS)
    {
        CanoeFlightMap *map = button->base.parent;
        
        if(map->rangeScale < 100)
        {
            map->rangeScale += 10;
        }
        else
        {
            map->rangeScale += 100;
        }
    }
}

CanoeFlightMap *canoeFlightMapNew(CanoeVector2f location)
{
    CanoeFlightMap *map = malloc(sizeof(CanoeFlightMap));
    CANOE_MALLOC_CHK(label);
    
    canoeDrawableBaseInit(
        &map->base,
        NULL,
        (DrawFunction)CANOE_FOUNDATION_FUNC_DRAW(CanoeFlightMap),
        (DestroyFunction)CANOE_FOUNDATION_FUNC_DESTROY(CanoeFlightMap),
        location,
        canoeVector2f(300, 300),
        addedTo,
        NULL,
        "FLTMAP"
    );

    
    strcpy(map->gpsStatus, "NAV");
    strcpy(map->insStatus, "ALIGN");
    strcpy(map->laserStatus, "---");
    strcpy(map->masterNavStatus, "ALIGN");
    
    map->font = NULL;
    map->entFont = NULL;
    map->hintFont = NULL;
    
    map->logitude = -111.916280;
    map->latitude =   33.415517;
    map->altitude = 0;
    
    map->heading = 180.0;
    
    map->windDir = 310;
    map->windSpeed = 0;
    
    map->rangeScale = 100;
    
    map->zoomOutButton = canoeButtonNew(canoeVector2f(190, 16), "...");
    map->zoomInButton = canoeButtonNew(canoeVector2f(210, 16), "+");
    
    canoeButtonSetSize(map->zoomOutButton, canoeVector2f(20, 20));
    canoeButtonSetSize(map->zoomInButton, canoeVector2f(20, 20));

    canoeAddChild(&map->base, map->zoomOutButton);
    canoeAddChild(&map->base, map->zoomInButton);
    
    canoeRegisterEvent(map->zoomOutButton, CANOE_EVENT_MOUSE_BUTTON_ACTION, zoomOutAction);
    canoeRegisterEvent(map->zoomInButton, CANOE_EVENT_MOUSE_BUTTON_ACTION, zoomInAction);
    
    return map;
}

static void drawRectL(float x, float y, float w, float h)
{
    glBegin(GL_LINES);
    {
        glVertex2f(x, y);
        glVertex2f(x, y + h);
        
        glVertex2f(x, y + h);
        glVertex2f(x + w, y + h);
        
        glVertex2f(x + w, y + h);
        glVertex2f(x + w, y);
        
        glVertex2f(x + w, y);
        glVertex2f(x, y);
    }
    glEnd();
}

static void drawWayPoint(CanoeFlightMap *map, CanoeVector2f pos, char *name)
{
    glPushAttrib(GL_LINE_BIT);
    {
        glLineWidth(1.5);
        setColor(255, 255, 255);
        drawLine(pos.x - 5, pos.y, pos.x + 5, pos.y);
        drawLine(pos.x, pos.y - 5, pos.x, pos.y + 5);
        sharedFontInstanceRender(map->entFont, pos.x + 5, pos.y - 2, name, NULL);
    }
    glPopAttrib();
}

static void drawHeadingScale(CanoeFlightMap *map, double scale)
{
    char str[128];
    
    double ratio = map->base.dpiRatio * scale;
    
    // TODO: change this
    glEnable(GL_SCISSOR_TEST);
    glScissor(map->base.location.x * ratio, (map->base.location.y + 210) * ratio,
              300 * ratio, 300 * ratio);
    
    /* draw compass arc */
    glColor3f(1.0, 1.0, 1.0);
    glLineWidth(2.0);
    
    float rad = 200;
    glBegin(GL_LINE_STRIP);
    {
        for(int i = 45; i <= 135; i += 5)
        {
            glVertex2f(rad * cos(i * (3.1415926 / 180.0)) + 150,
                       rad * sin(i * (3.1415926 / 180.0)) + 50);
        }
    }
    glEnd();
    
    double offset = fmod(map->heading, 10.0);
    
    /* draw heading scale */
    
    float barLength;
    for(int i = 40; i <= 130; i += 5)
    {
        if(i % 10 == 0)
        {
            barLength = 7;
            glLineWidth(3.0);
        }
        else
        {
            barLength = 3;
            glLineWidth(1.5);
        }
        
        float angle = i + offset;
        
        float x = rad * cos(angle * (3.1415926 / 180.0)) + 150;
        float y = rad * sin(angle * (3.1415926 / 180.0)) + 50;
        
        glBegin(GL_LINES);
        {
            glVertex2f(x, y);
            glVertex2f((rad - barLength) * cos(angle * (3.1415926 / 180.0)) + 150,
                       (rad - barLength) * sin(angle * (3.1415926 / 180.0)) + 50);
        }
        glEnd();
        
        if(i % 10 != 0)
            continue;
        
        int comp = map->heading / 10;
        comp *= 20;
        
        int scale = -((int)(map->heading - offset - comp + i) - 90);
        
        while(scale > 360) scale -= 360;
        while(scale < 0) scale += 360;
        
        sprintf(str, "%d", scale);
        
        x = (rad - 17) * cos(angle * (3.1415926 / 180.0)) + 150;
        y = (rad - 17) * sin(angle * (3.1415926 / 180.0)) + 50;
        
        glPushMatrix();
        {
            glTranslatef(x, y, 0);
            glRotated(angle - 90, 0, 0, 1);
            sharedFontInstanceRender(map->font, -sharedFontCalcFontWidth(map->font, str) / 2.0, 0, str, NULL);
        }
        glPopMatrix();
    }
    
    glDisable(GL_SCISSOR_TEST);
}

CANOE_FOUNDATION_FUNC_DRAW_DECL(CanoeFlightMap, map)
{
    char str[128];

    if(map->base.boundaryDirty == true && map->font && map->font->instance)
    {
        map->base.boundaryDirty = false;
        CANOE_FOUNDATION_FUNC_UPDATE_BBOX(CanoeFlightMap)(map);
    }
    
    glPushAttrib(GL_LINE_BIT);
    glPushMatrix();

    glTranslatef(map->base.location.x, map->base.location.y, 0);
    
    glScalef(1, 1, 1);
    
    drawRectL(0, 0, 300, 300);
    
    /* draw background */
    glColor3f(16 / 255.0f, 18 / 255.0f, 47 / 255.0f);
    glBegin(GL_QUADS);
    {
        glVertex2f(0, 0);
        glVertex2f(0, 300);
        glVertex2f(300, 300);
        glVertex2f(300, 0);
    }
    glEnd();
    
    drawHeadingScale(map, 1.0);
    
    
    glColor3f(1.0, 1.0, 1.0);
    
    /* draw heading line */
    glBegin(GL_LINES);
    {
        glVertex2f(150, 50);
        glVertex2f(150, 260);
        
        for(int i = 50 + 35; i < 260; i += 35)
        {
            glVertex2f(147, i); glVertex2f(153, i);
        }
    }
    glEnd();
    
    /* draw heading triangle */
    glBegin(GL_POLYGON);
    {
        glVertex2f(150, 260);
        glVertex2f(155, 267);
        glVertex2f(145, 267);
    }
    glEnd();
    
    glBegin(GL_POLYGON);
    {
        glVertex2f(150, 50);
        glVertex2f(155, 43);
        glVertex2f(145, 43);
    }
    glEnd();
    
    sharedFontInstanceRender(map->hintFont, 163, 272, "TRU", NULL);
    sharedFontInstanceRender(map->hintFont, 118, 272, "TRK", NULL);
    
    glLineWidth(1.5);
    glBegin(GL_LINE_STRIP);
    {
        glVertex2f(140, 260 + 10);
        glVertex2f(160, 260 + 10);
        glVertex2f(160, 270 + 10);
        glVertex2f(140, 270 + 10);
        glVertex2f(140, 260 + 10);
    }
    glEnd();
    
    sprintf(str, "%.0f", map->heading);
    sharedFontInstanceRender(map->font, 149 - sharedFontCalcFontWidth(map->font, str) / 2.0, 272, str, NULL);
    
    
    
    sharedFontInstanceRender(map->entFont, 210, 280, "LONG", NULL);
    sharedFontInstanceRender(map->entFont, 210, 270, "LAT", NULL);
    sharedFontInstanceRender(map->entFont, 210, 260, "ALT", NULL);
    
    sharedFontInstanceRender(map->entFont, 20, 280, "GPS", NULL);
    sharedFontInstanceRender(map->entFont, 20, 270, "INS", NULL);
    sharedFontInstanceRender(map->entFont, 20, 260, "LSR", NULL);
    
    sharedFontInstanceRender(map->font, 45, 280, map->gpsStatus, NULL);
    sharedFontInstanceRender(map->font, 45, 270, map->insStatus, NULL);
    sharedFontInstanceRender(map->font, 45, 260, map->laserStatus, NULL);
    
    sprintf(str, "%.6f", map->logitude);
    sharedFontInstanceRender(map->font, 240, 280, str, NULL);
    sprintf(str, "%.6f", map->latitude);
    sharedFontInstanceRender(map->font, 240, 270, str, NULL);
    sprintf(str, "%.0f", map->altitude);
    sharedFontInstanceRender(map->font, 240, 260, str, NULL);
    sharedFontInstanceRender(map->hintFont, 270, 260, "ft.", NULL);
    
    sprintf(str, "%.0fm", map->rangeScale);
    sharedFontInstanceRender(map->font, 158, 82, str, NULL);
    
    /* wind direction / speed */
    sprintf(str, "%.0f° / %.0f kts", map->windDir, map->windSpeed);
    sharedFontInstanceRender(map->font, 20, 20, str, NULL);
    glPushMatrix();
    {
        glTranslatef(30, 45, 0);
        glRotated(360 - map->windDir + map->heading, 0, 0, 1);
        glBegin(GL_LINES);
        glVertex2f(0, -10);
        glVertex2f(0, 10);
        glVertex2f(0, 10); glVertex2f(3, 7);
        glVertex2f(0, 10); glVertex2f(-3, 7);
        glEnd();
    }
    glPopMatrix();
    
    /* master navigation system status */
    sharedFontInstanceRender(map->entFont, 240, 30, "MSTR NAV ", NULL);
    sharedFontInstanceRender(map->hintFont, 240, 20, map->masterNavStatus, NULL);
    
    glEnable(GL_SCISSOR_TEST);
    CanoeVector2f world = canoeCalculateWorldCoordinate(&map->base);
    glScissor(world.x * 2, world.y * 2, 600, 500);
    
    float wptScale = 1.0 / map->rangeScale * 35.0;
    
    if(map->waypoints)
    {
        INSCoordinateWGS84 currentWGS84 = {
            .longitude = map->logitude,
            .latitude = map->latitude,
            .altitude = map->altitude
        };
        
        INSVector3f currentECEF = convertWGS84toECEF(currentWGS84);
        
        glPushMatrix();
        glTranslatef(150, 50, 0);
        glRotatef(map->heading, 0, 0, 1);
        
        float lastWptX = 0.0, lastWptY = 0.0;
        
        for(int i = 0; i < SV_SIZE(map->waypoints); i++)
        {
            INSVector3f wpt = SV_AT(map->waypoints, i)->coordinate;
            
            INSVector3f vector = {
                .x = wpt.x - currentECEF.x,
                .y = wpt.y - currentECEF.y,
                .z = wpt.z - currentECEF.z
            };
            
            float wptX = vector.x * wptScale;
            float wptY = vector.y * wptScale;
            
            if(i > 0)
            {
                glLineWidth(3.0);
                setColor(255, 0, 255);
                drawLine(lastWptX, lastWptY, wptX, wptY);
            }
            
            glPushMatrix();
            {
                glTranslatef(wptX, wptY, 0.0);
                glRotatef(-map->heading, 0, 0, 1);
                drawWayPoint(map, canoeVector2f(0, 0), SV_AT(map->waypoints, i)->name);
            }
            glPopMatrix();
            
            lastWptX = wptX;
            lastWptY = wptY;
        }
        
        glPopMatrix();
    }

    glDisable(GL_SCISSOR_TEST);
    
    
    glPopAttrib();
    
    canoeRenderChildren(&map->base);
    
    glPopMatrix();
}

CANOE_FOUNDATION_FUNC_DESTROY_DECL(CanoeFlightMap, label)
{
#ifdef CANOE_LOG_DESTROY
    printf("flight map destroy\n");
#endif /* CANOE_LOG_DESTROY */
    
    sharedFontRelease(label->font->context, label->font);
    SV_RELEASE(label->base.children);
    free(label->base.children);
}

CanoeFlightMap *canoeFlightMapSetFont(CanoeFlightMap *label, char *fontName, int size)
{
    if(label->font)
    {
        label->font = sharedFontSetFont(label->font->context, &label->font, fontName, size);
    }
    return label;
}

CanoeFlightMap *canoeFlightMapSetLocation(CanoeFlightMap *label, CanoeVector2f location)
{
    label->base.location = location;
    label->base.boundaryDirty = true;
    
    return label;
}

CanoeFlightMap *canoeFlightMapSetWaypoints(CanoeFlightMap *map, void *wayPoints)
{
    map->waypoints = wayPoints;
    
    return map;
}
