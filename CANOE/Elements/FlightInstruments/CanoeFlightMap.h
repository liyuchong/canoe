/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_FLIGHT_MAP_H__
#define CANOE_FLIGHT_MAP_H__

#include "CanoeDrawableBase.h"
#include "CanoeWindow.h"
#include "CanoeSharedFont.h"
#include "GeographicAlgorithms.h"
#include "CanoeButton.h"

/* waypoint type */
#define CANOE_WAYPOINT_NAME_LENGTH 128

typedef struct
{
    /* ECEF coordinate */
    INSVector3f coordinate;
    char name[CANOE_WAYPOINT_NAME_LENGTH];
} CanoeWaypoint;

/* Flight map */
#define CANOE_FLTMAP_STATUS_HINT_LENGTH 32

CANOE_DECLARE_ELEMENT(CanoeFlightMap)
{
    CanoeDrawableBase base;
    
    VECTOR(CanoeWaypoint *) *waypoints;
    
    CanoeSharedFont *font, *hintFont, *entFont;
    
    char
    gpsStatus      [CANOE_FLTMAP_STATUS_HINT_LENGTH],
    insStatus      [CANOE_FLTMAP_STATUS_HINT_LENGTH],
    laserStatus    [CANOE_FLTMAP_STATUS_HINT_LENGTH],
    masterNavStatus[CANOE_FLTMAP_STATUS_HINT_LENGTH];
    
    double logitude, latitude, altitude;
    double heading;
    double windDir, windSpeed;
    
    double rangeScale;
    
    CanoeButton *zoomOutButton, *zoomInButton;
};

/* Memory allocation & free */
CanoeFlightMap *canoeFlightMapNew(CanoeVector2f location);

/* shared methods */
CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeFlightMap, map);
CANOE_FOUNDATION_FUNC_DRAW_DECL       (CanoeFlightMap, map);
CANOE_FOUNDATION_FUNC_DESTROY_DECL    (CanoeFlightMap, map);

CanoeFlightMap *canoeFlightMapSetWaypoints(CanoeFlightMap *map, void *wayPoints);

#endif /* defined(CANOE_FLIGHT_MAP_H__) */
