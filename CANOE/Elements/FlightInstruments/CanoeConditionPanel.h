/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_CONDITION_PANEL_H__
#define CANOE_CONDITION_PANEL_H__

#include "CanoeDrawableBase.h"
#include "CanoeWindow.h"
#include "CanoeSharedFont.h"
#include "CanoeCircularGague.h"
#include "CanoeSlider.h"

CANOE_DECLARE_ELEMENT(CanoeConditionPanel)
{
    CanoeDrawableBase base;
    
    CanoeSharedFont *font, *entFont;
    
    CanoeCircularGague *fuleGgaue, *satGgaue, *battGgaue;
    
    CanoeSlider *elevSlider, *aileron, *rudder, *throttle;
    
    double value;
};

/* Memory allocation & free */
CanoeConditionPanel *canoeCanoeConditionPanelNew(CanoeVector2f location);

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeConditionPanel, ele);

CANOE_FOUNDATION_FUNC_DRAW_DECL(CanoeConditionPanel, ele);

CANOE_FOUNDATION_FUNC_DESTROY_DECL(CanoeConditionPanel, ele);

#endif
