/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeSlider.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"
#include <math.h>

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeSlider, ele)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(ele->base.location),
                       EXPLODE_COMPONENT2D(canoeVector2fAdd(ele->base.location, ele->base.size))
                       );
    
    spatialIndexerUpdateBoundary(&ele->base.spatialNode, newBoundary);
}

static void addedTo(void *element, void *parent)
{
    ((CanoeSlider *)element)->font = sharedFontRegisterUsage(parent, "Helvetica", 10);
    ((CanoeSlider *)element)->hintFont = sharedFontRegisterUsage(parent, "Helvetica", 9);

    sharedFontSetColor(NULL, ((CanoeSlider *)element)->hintFont, canoeColor4f(0.0, 1.0, 1.0, 1.0));
}

CanoeSlider *canoeCanoeSliderNew(CanoeVector2f location, double width, char *title)
{
    CanoeSlider *ele = malloc(sizeof(CanoeSlider));
    CANOE_MALLOC_CHK(ele);
    
    canoeDrawableBaseInit(
                          &ele->base,
                          NULL,
                          (DrawFunction)CANOE_FOUNDATION_FUNC_DRAW(CanoeSlider),
                          (DestroyFunction)CANOE_FOUNDATION_FUNC_DESTROY(CanoeSlider),
                          location,
                          canoeVector2f(width, 25),
                          addedTo,
                          NULL,
                          "SLIDER"
                          );
    
    strncpy(ele->title, title, 32);
    ele->value = 0.0;
    
    return ele;
}

CANOE_FOUNDATION_FUNC_DRAW_DECL(CanoeSlider, ele)
{
    CANOE_FOUNDATION_CHECK_REBUILD_INDEX(CanoeSlider, ele);
    
    glPushAttrib(GL_LINE_BIT);
    glPushMatrix();
    {
        CanoeVector2f world = canoeCalculateWorldCoordinate(&ele->base);
        glTranslatef(world.x, world.y, 0.0);
        
        CanoeVector2f size = ele->base.size;
        
        setColor(255, 255, 255);
        
        glLineWidth(2.0);
        drawLine(0, 5, size.x, 5);
        drawLine(0, 0, 0, 10);
        drawLine(size.x, 0, size.x, 10);
        
        char str[32];
        sprintf(str, "%.1f%%", ele->value * 100.0);
        
        sharedFontInstanceRender(ele->font, size.x - sharedFontCalcFontWidth(ele->font, str), 16, str, NULL);
        sharedFontInstanceRender(ele->hintFont, 0, 16, ele->title, NULL);
        
        float x = size.x / 2.0 * ele->value + size.x / 2.0;
        
        glBegin(GL_POLYGON);
        {
            glVertex2f(x, 5);
            glVertex2f(x - 4, 12);
            glVertex2f(x + 4, 12);
            glVertex2f(x, 5);
        }
        glEnd();
        
        setColor(0, 255, 0);
        drawLine(size.x / 2.0 - 1.5, 0, size.x / 2.0, 5);
        drawLine(size.x / 2.0 + 1.5, 0, size.x / 2.0, 5);
    }
    glPopMatrix();
    glPopAttrib();
    
    //drawBoundary(ele);
}

CANOE_FOUNDATION_FUNC_DESTROY_DECL(CanoeSlider, ele)
{
#ifdef CANOE_LOG_DESTROY
    printf("...\n");
#endif /* CANOE_LOG_DESTROY */
    
    SV_RELEASE(ele->base.children);
    free(ele->base.children);
}
