/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeAltimeter.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"

#include <math.h>

static char *scales[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeAltimeter, ele)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
        EXPLODE_COMPONENT2D(ele->base.location),
        EXPLODE_COMPONENT2D(canoeVector2fAdd(ele->base.location, ele->base.size))
    );
    
    spatialIndexerUpdateBoundary(&ele->base.spatialNode, newBoundary);
}

static void addedTo(void *element, void *parent)
{
    ((CanoeAltimeter *)element)->font = sharedFontRegisterUsage(parent, "MS33558", 18);
    ((CanoeAltimeter *)element)->altHundsFont = sharedFontRegisterUsage(parent, "Menlo", 26);
    ((CanoeAltimeter *)element)->altThousFont = sharedFontRegisterUsage(parent, "Menlo", 32);
    
    ((CanoeAltimeter *)element)->hintFont = sharedFontRegisterUsage(parent, "MS33558", 16);
    
    sharedFontSetColor(NULL, ((CanoeAltimeter *)element)->hintFont, canoeColor4f(0, 1, 0, 1));
}

CanoeAltimeter *canoeAltimeterNew(CanoeVector2f location, double height)
{
    CanoeAltimeter *ele = malloc(sizeof(CanoeAltimeter));
    CANOE_MALLOC_CHK(label);
    
    canoeDrawableBaseInit(
        &ele->base,
        NULL,
        (DrawFunction)CANOE_FOUNDATION_FUNC_DRAW(CanoeAltimeter),
        (DestroyFunction)CANOE_FOUNDATION_FUNC_DESTROY(CanoeAltimeter),
        location,
        canoeVector2f(height, height),
        addedTo,
        NULL,
        "ALT"
    );
    
    ele->altitude = 80;
    ele->seaLevel = 30.04;
    
    ele->scale = height / 300.0;
    
    return ele;
}

CANOE_FOUNDATION_FUNC_DRAW_DECL(CanoeAltimeter, ele)
{
    CANOE_FOUNDATION_CHECK_REBUILD_INDEX(CanoeAltimeter, ele);
    
    char str[128];
    
    glPushAttrib(GL_LINE_BIT);
    glPushMatrix();
    {
        CanoeVector2f world = canoeCalculateWorldCoordinate((CanoeDrawableBase *)ele);
        glTranslatef(world.x, world.y, 0.0);
        
        setColor(60, 60, 60);
        glBegin(GL_QUADS);
        {
            glVertex2f(0, 0);
            glVertex2f(0, 300 * ele->scale);
            glVertex2f(300 * ele->scale, 300 * ele->scale);
            glVertex2f(300 * ele->scale, 0);
        }
        glEnd();
        
        glTranslatef(ele->base.size.x / 2.0, ele->base.size.y / 2.0, 0.0);
        
        glScalef(ele->scale, ele->scale, ele->scale);
        
        setColor(0, 0, 0);
        /* draw circle */
        glBegin(GL_POLYGON); circleVertexes(0, 360, 5, 0, 0, 146.0); glEnd();
        
        /* draw scales */
        setColor(255, 255, 255);
        glLineWidth(2.0);
        glBegin(GL_LINES);
        {
            for(int i = 0; i < 360; i += 36)
            {
                glVertex2f(sin(DEG2RAD(i)) * 146, cos(DEG2RAD(i)) * 146);
                glVertex2f(sin(DEG2RAD(i)) * 135, cos(DEG2RAD(i)) * 135);
                
                for(int j = 4; j < 40; j += 4)
                {
                    glVertex2f(sin(DEG2RAD(i + j)) * 146, cos(DEG2RAD(i + j)) * 146);
                    glVertex2f(sin(DEG2RAD(i + j)) * 141, cos(DEG2RAD(i + j)) * 141);
                }
            }
        }
        glEnd();
        
        /* draw scale text */
        for(int i = 0; i < 360; i += 36)
        {
            float x = sin(DEG2RAD(i)) * 118 - 5;
            float y = cos(DEG2RAD(i)) * 118 - 5;
            
            sharedFontInstanceRender(ele->font, x, y, scales[i / 36], NULL);
        }
        
        /* draw current altitude */
        drawLine(-50, -30, 50, -30);
        drawLine(-50, -70, 50, -70);
        drawLine(-50, -30, -50, -70);
        drawLine(50, -30, 50, -70);
        
        sprintf(str, "%02d", ((int)(ele->altitude + 0.5)) / 1000);
        sharedFontInstanceRender(ele->altThousFont, -42, -62, str, NULL);
        
        sprintf(str, "%03d", ((int)(ele->altitude + 0.5)) % 1000);
        sharedFontInstanceRender(ele->altHundsFont, -4, -62, str, NULL);
        
        drawLine(-50, 12, 60, 12);
        drawLine(-50, 40, 60, 40);
        drawLine(-50, 12, -50, 40);
        drawLine(60, 12, 60, 40);
        sprintf(str, "%05d", (int)(ele->altitude / 3.3 + 0.5));
        sharedFontInstanceRender(ele->altHundsFont, -40, 17, str, NULL);
        
        sharedFontSetColor(NULL, ele->hintFont, canoeColor4f(0, 1, 1, 1));
        sharedFontInstanceRender(ele->hintFont, 42, 17, "M", NULL);
        sharedFontSetColor(NULL, ele->hintFont, canoeColor4f(0, 1, 0, 1));
        
        /* draw altimeter 0 level pressure */
        sprintf(str, "%d %3dmm", (int)(ele->seaLevel * 100 + 0.5), (int)(ele->seaLevel * 25.4 + 0.5));
        sharedFontInstanceRender(ele->hintFont, -sharedFontCalcFontWidth(ele->hintFont, str) / 2.0, 58, str, NULL);
        
        float hunds = fmodf(ele->altitude, 1000.0);
        float angle = 360 * hunds / 1000.0;
        
        glRotatef(-angle, 0, 0, 1);
        
        /* draw pointer */
        glBegin(GL_POLYGON);
        {
            glVertex2f(-2, 0);
            glVertex2f(-2, 100);
            glVertex2f(2, 100);
            glVertex2f(2, 0);
        }
        glEnd();
        
        glBegin(GL_POLYGON);
        {
            glVertex2f(-7, 100);
            glVertex2f(0, 130);
            glVertex2f(7, 100);
        }
        glEnd();
    }
    glPopMatrix();
    glPopAttrib();
    
    drawBoundary(ele);
}

CANOE_FOUNDATION_FUNC_DESTROY_DECL(CanoeAltimeter, ele)
{
#ifdef CANOE_LOG_DESTROY
    printf("alt destroy\n");
#endif /* CANOE_LOG_DESTROY */
    
    SV_RELEASE(ele->base.children);
    free(ele->base.children);
}

CanoeAltimeter *canoeAltimeterSetAltitude(CanoeAltimeter *altimeter, double alt)
{
    altimeter->altitude = alt;
    
    return altimeter;
}

CanoeAltimeter *canoeAltimeterSetSeaLevel(CanoeAltimeter *altimeter, double seaLevel)
{
    altimeter->seaLevel = seaLevel;
    
    return altimeter;
}
