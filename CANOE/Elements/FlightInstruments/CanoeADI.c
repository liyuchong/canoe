//
//  CanoeADI.c
//  CANOE
//
//  Created by NT on 3/21/15.
//  Copyright (c) 2015 nt. All rights reserved.
//

#include "CanoeADI.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"
#include <math.h>

CanoeADI *canoeAdiNew(CanoeVector2f location, CanoeVector2f size)
{
    CanoeADI *adi = malloc(sizeof(CanoeADI));
    CANOE_MALLOC_CHK(adi);
    
    adi->airSpeed = 0.0;
    adi->altitude = 800;
    
    adi->roll = 0.0;
    adi->yaw = 0.0;
    adi->pitch = 0.0;
    adi->verticalSpeed = 0.0;
    
    adi->altimeter = 29.92;
    adi->targetAirSpeed = 100;
    adi->targetAltitude = 1000;
    
    strcpy(adi->hintLeft, "HDG");
    strcpy(adi->hintMiddle, "ALT");
    strcpy(adi->hintRight, "NAV PTH");
    
    canoeDrawableBaseInit(&adi->base,
                          NULL,
                          (DrawFunction)canoeAdiDraw,
                          (DestroyFunction)canoeAdiDestroy,
                          location,
                          size,
                          NULL,
                          NULL,
                          "ADI");
    
    adi->font = sharedFontRegisterUsage(NULL, "Helvetica", 18);
    adi->hintFont = sharedFontRegisterUsage(NULL, "Helvetica", 18);
    
    sharedFontSetColor(NULL, adi->hintFont, canoeColor4f(0.0, 1.0, 0.0, 1.0));
    
    return adi;
}


#define ADI_PITCH_PIXEL_RATIO   5.0f
#define ADI_PITCH_LINE_HEIGHT   2.5f
#define ADI_PITCH_BAR_WIDTH     (16 * 2.0f)
#define ADI_HORIZON_BAR_HEIGHT  (2.5 * 2.0)
#define ADI_HORIZON_BOX_SIZE    (2 * 2.0)

#define ADI_HORIZON_INDICATOR_HEIGHT (4 * 2.0f)

#define AIR_SPEED_SCALE_SEP 10
#define ALTITUDE_SCALE_SEP  100

#define SPEED_HINT_BOX_HEIGHT       15
#define SPEED_HINT_BOX_WIDTH        50
#define SPEED_HINT_BOX_MOUSE_WIDTH  9
#define SPEED_HINT_BOX_MOUSE_HEIGHT 8

#define ALT_HINT_BOX_WIDTH 60

#define LW_NORMAL 1.5f

#define COLOR_BYTE(value) ((value) / 255.0f)
#define GL_COLOR_GRAY(value) \
glColor3f(COLOR_BYTE(value), COLOR_BYTE(value), COLOR_BYTE(value))



void drawLineL(float x1, float y1, float x2, float y2)
{
    glBegin(GL_LINES);
    {
        glVertex2f(x1, y1);
        glVertex2f(x2, y2);
    }
    glEnd();
}

void drawRectL(float x, float y, float w, float h)
{
    glBegin(GL_LINES);
    {
        glVertex2f(x, y);
        glVertex2f(x, y + h);
        
        glVertex2f(x, y + h);
        glVertex2f(x + w, y + h);
        
        glVertex2f(x + w, y + h);
        glVertex2f(x + w, y);
        
        glVertex2f(x + w, y);
        glVertex2f(x, y);
    }
    glEnd();
}

#define DPI_SCALE_FACTOR 1.0

void canoeAdiDraw(CanoeADI *adi)
{
    int posX = adi->base.location.x * 2;
    int posY = adi->base.location.y * 2;
    
    
    //popMatrix();
    
    glPushAttrib(GL_LINE_BIT);
    
    glPushMatrix();
    
    float scaleSet = 1.0;
    float scale = 0.5 * scaleSet;
    glScalef(scale, scale, scale);
    glTranslatef(300 + adi->base.location.x * 2, 300 + adi->base.location.y * 2, 0);
    
    glEnable(GL_SCISSOR_TEST);
    //glScissor(80*scale * DPI_SCALE_FACTOR + posX , 70*scale*DPI_SCALE_FACTOR + posY, 380*scale*DPI_SCALE_FACTOR, 460*scale*DPI_SCALE_FACTOR);
    
    glScissor(posX, posY, 600, 600);
    
    glPushMatrix();
    {
        glTranslatef(-27.5, 0, 0);
        glRotatef(adi->roll, 0, 0, 1);
        
        float y = adi->pitch * ADI_PITCH_PIXEL_RATIO;
        
        /* draw sky & ground */
        glColor3f(0, 160 / 255.0f, 227 / 255.0f);
        glBegin(GL_QUADS);
        {
            glVertex2f(-500, y);
            glVertex2f(-500, 500);
            glVertex2f(500, 500);
            glVertex2f(500, y);
        }
        glEnd();
        
        glColor3f(130 / 255.0f, 62 / 255.0f, 11 / 255.0f);
        glBegin(GL_QUADS);
        {
            glVertex2f(-500, y);
            glVertex2f(-500, -500);
            glVertex2f(500, -500);
            glVertex2f(500, y);
        }
        glEnd();
        
        GL_COLOR_GRAY(230);
        
        glLineWidth(ADI_PITCH_LINE_HEIGHT * scaleSet);
        
        drawLineL(-500, y, 500, y);
        
        for(int i = -900 + 25; i <= 900 - 25; i += 25)
        {
            y = ((i / 10.0) + adi->pitch) * ADI_PITCH_PIXEL_RATIO;
            
            if(i % 100 == 0)
            {
                drawLineL(-ADI_PITCH_BAR_WIDTH * 1.8f, y, ADI_PITCH_BAR_WIDTH * 1.8f, y);
                if(i != 0 && (i <= 600 && i >= -600))
                {
                    char buf[16];
                    sprintf(buf, "%d", abs(i / 10));
                    
                    sharedFontInstanceRender(adi->font, ADI_PITCH_BAR_WIDTH * 1.8f + 5, y - 5, buf, NULL);
                    sharedFontInstanceRender(adi->font, -ADI_PITCH_BAR_WIDTH * 1.8f -25, y - 5, buf, NULL);
                }
            }
            else if(i % 50 == 0)
                drawLineL(-ADI_PITCH_BAR_WIDTH * 0.8, y, ADI_PITCH_BAR_WIDTH * 0.8, y);
            else if(i <= 300 && i > -300)
                drawLineL(-ADI_PITCH_BAR_WIDTH * 0.5, y, ADI_PITCH_BAR_WIDTH * 0.5, y);
        }
        
        /* draw horizon */
        glLineWidth(ADI_HORIZON_BAR_HEIGHT * scaleSet);
        
        y = adi->pitch * ADI_PITCH_PIXEL_RATIO;
        drawLineL(-ADI_PITCH_BAR_WIDTH * 2.3f, y, ADI_PITCH_BAR_WIDTH * 2.3f, y);
        
        glLineWidth(LW_NORMAL * scaleSet);
        
        /* draw roll scale */
        for(int i = 0; i <= 180; i += 15)
        {
            float r;
            float radius = 190;
            
            if(i % 30 == 0)
            {
                glLineWidth(3.0f * scaleSet);
                r = 15;
            }
            else
            {
                glLineWidth(LW_NORMAL * scaleSet);
                r = 10;
            }
            
            glBegin(GL_LINES);
            {
                glVertex2f(cosf(i * (3.1415 / 180.0)) * radius,
                           sinf(i * (3.1415 / 180.0)) * radius);
                glVertex2f(cosf(i * (3.1415 / 180.0)) * (radius - r),
                           sinf(i * (3.1415 / 180.0)) * (radius - r));
            }
            glEnd();
        }
        
        //if(!adi->attitudeValid)
        {
            glRotated(-adi->roll, 0, 0, 1);
            glColor3f(1.0, 0.2, 0.2);
            glLineWidth(20.0 * scaleSet);
            drawLineL(-100, -100, 100, 100);
            drawLineL(-100, 100, 100, -100);
            
            glColor3f(1, 1, 1);
        }
    }
    glPopMatrix();
    
    glDisable(GL_SCISSOR_TEST);
    
    glPushMatrix();
    {
        glTranslatef(-27.5, 0, 0);
        
        glLineWidth(3.0f * scaleSet);
        
        /* draw roll triangle */
        glBegin(GL_LINE_STRIP);
        {
            float y = 160;
            glVertex2f(-8, y);
            glVertex2f(0, y + 10);
            glVertex2f(8, y);
            glVertex2f(-8, y);
        }
        glEnd();
        
        GL_COLOR_GRAY(0);
        glBegin(GL_QUADS);
        {
            glVertex2f(-16, 135);
            glVertex2f(17, 135);
            glVertex2f(17, 156);
            glVertex2f(-16, 156);
        }
        glEnd();
        
        glLineWidth(LW_NORMAL * scaleSet);
        GL_COLOR_GRAY(230);
        char str[15];
        sprintf(str, "%d", abs((int)(adi->roll + 0.5f)));
        //drawStringL(adi->font, -getFontWidth(adi->font, str) / 2.0f, 139, str);
        sharedFontInstanceRender(adi->font,  -sharedFontCalcFontWidth(adi->font, str) / 2.0f, 139, str, NULL);
        
        glBegin(GL_LINE_STRIP);
        {
            glVertex2f(-16, 135);
            glVertex2f(17, 135);
            glVertex2f(17, 156);
            glVertex2f(-16, 156);
            glVertex2f(-16, 135);
        }
        glEnd();
        
        
        glDisable(GL_LINE_SMOOTH);
        /* draw horizon indicator */
        glLineWidth(3.0f * scaleSet);
#define _A ADI_HORIZON_BOX_SIZE
        
        drawLineL(-_A, -_A, -_A, _A);
        drawLineL(-_A - 1, _A, _A + 2, _A);
        drawLineL(_A, _A, _A, -_A);
        drawLineL(_A + 2, -_A, -_A - 1, -_A);
        
#undef _A
        
        GL_COLOR_GRAY(230);
        glLineWidth(ADI_HORIZON_INDICATOR_HEIGHT * scaleSet);
        glLineWidth(20.0);
        drawLineL(35, 0, 85, 0);
        drawLineL(-85, 0, -35, 0);
        drawLineL(35, 4, 35, -12);
        drawLineL(-35, 4, -35, -12);
        
        GL_COLOR_GRAY(0);
        glLineWidth(2 * 2 * scaleSet);
        
        drawLineL(36, 0, 83, 0);
        drawLineL(-83, 0, -36, 0);
        
        drawLineL(36, 2, 36, -10);
        drawLineL(-35, 2, -35, -10);
        
        glEnable(GL_LINE_SMOOTH);
    }
    glPopMatrix();
    
    /* areas around the adi */
    glColor3f(16 / 255.0, 18 / 255.0, 47 / 255.0);
    glBegin(GL_QUADS);
    {
        /* left */
        glVertex2f(-300, -300);
        glVertex2f(-300, 300);
        glVertex2f(-200, 300);
        glVertex2f(-200, -300);
        
        /* right */
        glVertex2f(145, -300);
        glVertex2f(145, 300);
        glVertex2f(300, 300);
        glVertex2f(300, -300);
        
        /* top */
        glVertex2f(-300, -300);
        glVertex2f(-300, -190);
        glVertex2f(300, -190);
        glVertex2f(300, -300);
        
        /* bottom */
        glVertex2f(-300, 190);
        glVertex2f(-300, 300);
        glVertex2f(300, 300);
        glVertex2f(300, 190);
    }
    glEnd();
    
    /* speed bar, altitude bar, status bar */
    glColor3f(54 / 255.0, 46 / 255.0, 62 / 255.0);
    glBegin(GL_QUADS);
    {
        /* left speed bar */
        glVertex2f(-290, -230);
        glVertex2f(-290, 230);
        glVertex2f(-220, 230);
        glVertex2f(-220, -230);
        
        /* right altitude bar */
        glVertex2f(240, -230);
        glVertex2f(240, 230);
        glVertex2f(165, 230);
        glVertex2f(165, -230);
        
        /* top status bar */
        glVertex2f(-220, 280);
        glVertex2f(164, 280);
        glVertex2f(164, 240);
        glVertex2f(-220, 240);
    }
    glEnd();
    
    /* right vertical speed bar */
    glBegin(GL_POLYGON);
    {
        glVertex2f(245, 160);
        glVertex2f(245, -160);
        glVertex2f(265, -160);
        glVertex2f(290, -80);
        glVertex2f(290, 80);
        glVertex2f(265, 160);
    }
    glEnd();
    
    /* draw bottom compass */
    
    glEnable(GL_SCISSOR_TEST);
    glScissor(posX, posY, adi->base.size.x * 2, 460*scale*2);
    
    float compassX = -50;
    float compassY = -530;
    float compassR = 300;
    glBegin(GL_POLYGON);
    {
        for(int i = 30; i <= 150; i += 5)
        {
            glVertex2f(cosf(i * (3.1415 / 180.0)) * compassR + compassX,
                       sinf(i * (3.1415 / 180.0)) * compassR + compassY);
        }
    }
    glEnd();
    
    GL_COLOR_GRAY(230);
    
    glLineWidth(LW_NORMAL * scaleSet);
    
    /* draw compass indicator */
    drawLineL(compassX - 10, compassY + 315, compassX + 10, compassY + 315);
    drawLineL(compassX - 10, compassY + 315, compassX, compassY + 315 - 12);
    drawLineL(compassX, compassY + 315 - 12, compassX + 10, compassY + 315);
    /* draw compass scale */
    float offset = ((int)(adi->yaw * 10.0) % 100);
    for(int i = 100 + offset; i <= 800; i += 10)
    {
        float angle = i / 5.0;
        float r = 10;
        
        if((int)(i - 50 - offset) % 100 == 0)
        {
            r = 20;
            glLineWidth(2.5f * scaleSet);
        }
        else
        {
            glLineWidth(LW_NORMAL * scaleSet);
        }
        
        drawLineL(cosf(angle * (3.1415 / 180.0)) * compassR + compassX,
                  sinf(angle * (3.1415 / 180.0)) * compassR + compassY,
                  cosf(angle * (3.1415 / 180.0)) * (compassR - r) + compassX,
                  sinf(angle * (3.1415 / 180.0)) * (compassR - r) + compassY);
    }
    
    /* draw scale text */
    glPushMatrix();
    {
        float offset = ((int)(adi->yaw * 10.0) % 100);
        
        for(int i = 100 + offset; i <= 800; i += 10)
        {
            float angle = i / 5.0;
            float r = 10;
            
            if((int)(i - 50 - offset) % 100 == 0)
            {
                char str[16];
                
                float x = cosf(angle * (3.1415 / 180.0)) * (compassR - r - 30) + compassX;
                float y = sinf(angle * (3.1415 / 180.0)) * (compassR - r - 30) + compassY;
                
                int value = -((i - offset) / 10 - ((adi->yaw)-((int)adi->yaw % 10)) - 45);
                
                while (value > 360)
                    value -= 360;
                while (value < 0)
                    value += 360;
                
                sprintf(str, "%d", value);
                
                //float bounds[6];
                //ftglGetFontBBox(adi->font, str, (int)strlen(str), bounds);
                
                
                glPushMatrix();
                glTranslatef(x, y, 0);
                glRotatef(angle + 270.0f, 0, 0, 1);
                //drawStringL(adi->font, -bounds[3] / 2.0, 0, str);
                sharedFontInstanceRender(adi->font, -sharedFontCalcFontWidth(adi->font, str) / 2.0, 0, str, NULL);
                glPopMatrix();
            }
        }
        
    }
    glPopMatrix();
    
    glDisable(GL_SCISSOR_TEST);
    
    glLineWidth(LW_NORMAL * scaleSet);
    /* top status bar seperator */
    drawLineL(-92, 280, -92, 240);
    drawLineL(36, 280, 36, 240);
    
    glColor3f(0, 1, 0);
    sharedFontInstanceRender(adi->hintFont, -28 - sharedFontCalcFontWidth(adi->font, adi->hintMiddle) / 2.0f, 260 - 5, adi->hintMiddle, NULL);
    sharedFontInstanceRender(adi->hintFont, -156 - sharedFontCalcFontWidth(adi->font, adi->hintLeft) / 2.0f, 260 - 5, adi->hintLeft, NULL);
    sharedFontInstanceRender(adi->hintFont, 100 - sharedFontCalcFontWidth(adi->font, adi->hintRight) / 2.0f, 260 - 5, adi->hintRight, NULL);
//
    /* draw air speed scale */
    GL_COLOR_GRAY(230);
    glEnable(GL_SCISSOR_TEST);
    glScissor(-300*scale*2 + posX, 70*scale*2 + posY, 600*scale*2, 460*scale*2);
    offset = AIR_SPEED_SCALE_SEP*10.0 - ((int)((adi->airSpeed*10.0)) % (AIR_SPEED_SCALE_SEP*10));
    offset /= 10.0;
    for(int i = -5; i < 5; i++)
    {
        char str[128];
        memset(str, 0, 128);
        
        int speed = (int)(((int)(adi->airSpeed)+AIR_SPEED_SCALE_SEP
                           - ((int)((adi->airSpeed)) % (AIR_SPEED_SCALE_SEP))) + i * (AIR_SPEED_SCALE_SEP));
        
        float y = ((float)(offset + i * AIR_SPEED_SCALE_SEP)) * 5.0;
        
        if(speed >= 0)
        {
            drawLineL(-220, y + 25, -230, y + 25);
            drawLineL(-220, y, -235, y);
            sprintf(str, "%d", speed);
        }
        else
        {
            glPushAttrib(GL_CURRENT_BIT);
            glColor3f(0.95, 0.15, 0.15);
            
            glBegin(GL_QUADS);
            {
                glVertex2f(-220, y);
                glVertex2f(-210, y);
                glVertex2f(-210, y + 5);
                glVertex2f(-220, y + 5);
                
                float off = 12.5;
                glVertex2f(-220, y + off);
                glVertex2f(-210, y + off);
                glVertex2f(-210, y + 5 + off);
                glVertex2f(-220, y + 5 + off);
                
                off = 25;
                glVertex2f(-220, y + off);
                glVertex2f(-210, y + off);
                glVertex2f(-210, y + 5 + off);
                glVertex2f(-220, y + 5 + off);
                
                off = 12.5 + 25;
                glVertex2f(-220, y + off);
                glVertex2f(-210, y + off);
                glVertex2f(-210, y + 5 + off);
                glVertex2f(-220, y + 5 + off);
            }
            glEnd();
            
            glPopAttrib();
        }
        sharedFontInstanceRender(adi->font, -240-sharedFontCalcFontWidth(adi->font, str), y-5, str, NULL);
    }
    glDisable(GL_SCISSOR_TEST);
    
    /* draw altitude scale */
    glEnable(GL_SCISSOR_TEST);
    glScissor(300*scale*2 + posX, 70*scale*2 + posY, 600*scale*2, 460*scale*2);
    offset = ALTITUDE_SCALE_SEP*10.0 - ((int)((adi->altitude*10.0)) % (ALTITUDE_SCALE_SEP*10));
    offset /= 10.0;
    for(int i = -3; i < 3; i++)
    {
        char str[8];
        float y = ((float)(offset + i * ALTITUDE_SCALE_SEP));
        
        int value = (int)(((int)(adi->altitude)+ALTITUDE_SCALE_SEP
                           - ((int)((adi->altitude)) % (ALTITUDE_SCALE_SEP)))
                          + i * (ALTITUDE_SCALE_SEP));
        
        if(value == 0)
        {
            const int markY = y;
            const int markX = 168;
            
            const int boxWidth = 70;
            const int boxHeight = 25;
            
            glPushAttrib(GL_CURRENT_BIT);
            glColor3f(0.91, 0.71, 0.25);
            
            
            for(int k = 0; k <= boxWidth; k += 5)
            {
                drawLineL(markX + k, y, markX + k, y - boxHeight);
            }
            for(int k = 0; k <= boxHeight; k += 5)
            {
                drawLineL(markX, markY - k, markX + boxWidth, markY - k);
            }
            
            glPopAttrib();
        }
        
        drawLineL(165, y, 180, y);
        drawLineL(165, y + 50, 175, y + 50);
        
        sprintf(str, "%d", value);
        
        float bounds[6];
        //ftglGetFontBBox(adi->font, str, (int)strlen(str), bounds);
        
        if(value % 1000 == 0)
        {
            glPushAttrib(GL_LINE_BIT);
            glLineWidth(2.5f * scaleSet);
            drawLineL(182, y + bounds[4] / 2 + 3, 182 + bounds[3], y + bounds[4] / 2 + 3);
            drawLineL(182, y - bounds[4] / 2 - 3, 182 + bounds[3], y - bounds[4] / 2 - 3);
            glPopAttrib();
        }
        
        //drawStringL(adi->font, 182, y-5, str);
        sharedFontInstanceRender(adi->font, 182, y-5, str, NULL);
    }
    glDisable(GL_SCISSOR_TEST);
    
    /* draw vertical speed scale */
    glLineWidth(LW_NORMAL * scaleSet);
    for(int i = 0; i < 9; i++)
    {
        char str[8];
        float y = i * 35 - 160 + 20;
        drawLineL(260, y, 265, y);
        
        sprintf(str, "%d", abs((i - 4) * 2));
        //drawStringL(adi->font, 247, y - 5, str);
        sharedFontInstanceRender(adi->font, 247, y - 5, str, NULL);
    }
    
    for(int i = 0; i < 8; i++)
    {
        float y = i * 35 - 160 + 37.5;
        drawLineL(260, y, 265, y);
    }
    
    glLineWidth(3.0f * scaleSet);
    drawLineL(290, 0, 269, (adi->verticalSpeed + 8) / 2 * 35 - 160 + 20);
    
    /* draw alt */
    glColor3f(0, 1, 0);
    char str[16];
    sprintf(str, "%.2f IN.", adi->altimeter);
    sharedFontInstanceRender(adi->hintFont, 168, -250, str, NULL);
    
    
    /* airspeed in mach */
    sprintf(str, "M %.2f", adi->airSpeed * 0.0015129279694993721f);
    sharedFontInstanceRender(adi->hintFont, -287, -250, str, NULL);
    
    /* air speed box */
    GL_COLOR_GRAY(230);
    
    glColor3f(16 / 255.0, 16 / 255.0, 48 / 255.0);
    /* air speed */
    glBegin(GL_POLYGON);
    {
        float x = -285;
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x + SPEED_HINT_BOX_WIDTH, -SPEED_HINT_BOX_HEIGHT);
        
        glVertex2f(x + SPEED_HINT_BOX_WIDTH, -SPEED_HINT_BOX_HEIGHT + SPEED_HINT_BOX_MOUSE_HEIGHT);
        glVertex2f(x + SPEED_HINT_BOX_WIDTH + SPEED_HINT_BOX_MOUSE_WIDTH, 0);
        glVertex2f(x + SPEED_HINT_BOX_WIDTH, SPEED_HINT_BOX_HEIGHT - SPEED_HINT_BOX_MOUSE_HEIGHT);
        
        glVertex2f(x + SPEED_HINT_BOX_WIDTH, SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x, SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT);
    }
    glEnd();
    
    /* altitude */
    float x = 180;
    glBegin(GL_POLYGON);
    {
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x + ALT_HINT_BOX_WIDTH, -SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x + ALT_HINT_BOX_WIDTH, SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x, SPEED_HINT_BOX_HEIGHT);
        
        glVertex2f(x, SPEED_HINT_BOX_HEIGHT - SPEED_HINT_BOX_MOUSE_HEIGHT);
        glVertex2f(x - SPEED_HINT_BOX_MOUSE_WIDTH, 0);
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT + SPEED_HINT_BOX_MOUSE_HEIGHT);
        
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT);
    }
    glEnd();
    
    GL_COLOR_GRAY(230);
    
    /* air speed */
    glBegin(GL_LINE_STRIP);
    {
        float x = -285;
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x + SPEED_HINT_BOX_WIDTH, -SPEED_HINT_BOX_HEIGHT);
        
        glVertex2f(x + SPEED_HINT_BOX_WIDTH, -SPEED_HINT_BOX_HEIGHT + SPEED_HINT_BOX_MOUSE_HEIGHT);
        glVertex2f(x + SPEED_HINT_BOX_WIDTH + SPEED_HINT_BOX_MOUSE_WIDTH, 0);
        glVertex2f(x + SPEED_HINT_BOX_WIDTH, SPEED_HINT_BOX_HEIGHT - SPEED_HINT_BOX_MOUSE_HEIGHT);
        
        glVertex2f(x + SPEED_HINT_BOX_WIDTH, SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x, SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT);
    }
    glEnd();
    
    /* altitude */
    glBegin(GL_LINE_STRIP);
    {
        float x = 180;
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x + ALT_HINT_BOX_WIDTH, -SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x + ALT_HINT_BOX_WIDTH, SPEED_HINT_BOX_HEIGHT);
        glVertex2f(x, SPEED_HINT_BOX_HEIGHT);
        
        glVertex2f(x, SPEED_HINT_BOX_HEIGHT - SPEED_HINT_BOX_MOUSE_HEIGHT);
        glVertex2f(x - SPEED_HINT_BOX_MOUSE_WIDTH, 0);
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT + SPEED_HINT_BOX_MOUSE_HEIGHT);
        
        glVertex2f(x, -SPEED_HINT_BOX_HEIGHT);
    }
    glEnd();
    
    sprintf(str, "%03d", (int)adi->airSpeed);
    sharedFontInstanceRender(adi->font, -260 - sharedFontCalcFontWidth(adi->font, str) / 2.0f, -6, str, NULL);
    
    sprintf(str, "%05d", (int)adi->altitude);
    sharedFontInstanceRender(adi->font, 210 - sharedFontCalcFontWidth(adi->font, str) / 2.0f, -6, str, NULL);
    
    glPopMatrix();
    glPopAttrib();
}

void canoeAdiDestroy(CanoeADI *adi)
{
    
}