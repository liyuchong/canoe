/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeCircularGague.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"
#include <math.h>

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeCircularGague, ele)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(ele->base.location),
                       EXPLODE_COMPONENT2D(canoeVector2fAdd(ele->base.location, ele->base.size))
                       );
    
    spatialIndexerUpdateBoundary(&ele->base.spatialNode, newBoundary);
}

static void addedTo(void *element, void *parent)
{
    ((CanoeCircularGague *)element)->font = sharedFontRegisterUsage(parent, "Menlo", 24);
}

CanoeCircularGague *canoeCanoeCircularGagueNew(CanoeVector2f location, double height)
{
    CanoeCircularGague *ele = malloc(sizeof(CanoeCircularGague));
    CANOE_MALLOC_CHK(ele);
    
    canoeDrawableBaseInit(
                          &ele->base,
                          NULL,
                          (DrawFunction)CANOE_FOUNDATION_FUNC_DRAW(CanoeCircularGague),
                          (DestroyFunction)CANOE_FOUNDATION_FUNC_DESTROY(CanoeCircularGague),
                          location,
                          canoeVector2f(height, height),
                          addedTo,
                          NULL,
                          "C_GAGUE"
                          );
    
    ele->value = 122.4;
    ele->min = 0;
    ele->max = 200.0;
    
    ele->ratio = height / 150.0;
    
    SET_DIRTY(ele);
    
    return ele;
}

void renderPointer(float angle, float startR, float endR)
{
    float x1 = startR * sin(DEG2RAD(angle));
    float y1 = startR * cos(DEG2RAD(angle));
    
    float x2 = endR * sin(DEG2RAD(angle));
    float y2 = endR * cos(DEG2RAD(angle));
    
    glBegin(GL_LINES);
    glVertex2f(x1, y1);
    glVertex2f(x2, y2);
    glEnd();
}

CANOE_FOUNDATION_FUNC_DRAW_DECL(CanoeCircularGague, ele)
{
    CANOE_FOUNDATION_CHECK_REBUILD_INDEX(CanoeCircularGague, ele);
 
    if(IS_DIRTY(ele))
    {
        DIRTY_COUNTER_DEC(ele);
    
    glPushAttrib(GL_LINE_BIT);
    glPushMatrix();
    {
        CanoeVector2f world = canoeCalculateWorldCoordinate(&ele->base);
        
        glTranslatef(world.x + ele->base.size.x / 2.0,
                     world.y + ele->base.size.y / 2.0, 0);
        
        glScaled(ele->ratio, ele->ratio, ele->ratio);
        
        setColor(255, 255, 255);
        
        glLineWidth(2.0);
        
        glBegin(GL_LINE_STRIP); circleVertexes(90, 300, 5, 0, 0, 60); glEnd();
        
        drawLine(0, 0, 75, 0);
        drawLine(0, 27, 75, 27);
        drawLine(0, 0, 0, 27);
        drawLine(75, 0, 75, 27);
                
        float ratio = (ele->value - ele->min) / (ele->max - ele->min);
        float angle = 90 + 210.0 * ratio;
        
        char str[32];
        sprintf(str, "%.1f", ele->value);
        
        sharedFontInstanceRender(ele->font, 70 - sharedFontCalcFontWidth(ele->font, str), 5, str, NULL);
        
        /* max level */
        renderPointer(300, 60, 75);
        
        glColor4f(1.0, 1.0, 1.0, 0.4);
        {
            glBegin(GL_POLYGON);
            glVertex2f(0, 0);
            circleVertexes(90, angle, 1, 0, 0, 60);
        }
        glEnd();
        
        /* pointer */
        glLineWidth(3.0);
        glColor4f(1.0, 1.0, 1.0, 1.0);
        renderPointer(angle, 0, 70);
    }
    glPopMatrix();
    glPopAttrib();
    
    
    //drawBoundary(ele);
    }
}

float canoeCircularGageSetValue(CanoeCircularGague *cg, float val)
{
    cg->value = val;
    
    SET_DIRTY(cg);
    
    return val;
}

CANOE_FOUNDATION_FUNC_DESTROY_DECL(CanoeCircularGague, ele)
{
#ifdef CANOE_LOG_DESTROY
    printf("...\n");
#endif /* CANOE_LOG_DESTROY */
    
    SV_RELEASE(ele->base.children);
    free(ele->base.children);
}
