/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeConditionPanel.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"
#include <math.h>

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeConditionPanel, ele)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(ele->base.location),
                       EXPLODE_COMPONENT2D(canoeVector2fAdd(ele->base.location, ele->base.size))
                       );
    
    spatialIndexerUpdateBoundary(&ele->base.spatialNode, newBoundary);
}

static void addedTo(void *element, void *parent)
{
    ((CanoeConditionPanel *)element)->font = sharedFontRegisterUsage(parent, "Helvetica", 10);
    
    ((CanoeConditionPanel *)element)->entFont = sharedFontRegisterUsage(parent, "Helvetica", 9);
    sharedFontSetColor(NULL, ((CanoeConditionPanel *)element)->entFont, canoeColor4f(0.0, 1.0, 1.0, 1.0));
}

CanoeConditionPanel *canoeCanoeConditionPanelNew(CanoeVector2f location)
{
    CanoeConditionPanel *ele = malloc(sizeof(CanoeConditionPanel));
    CANOE_MALLOC_CHK(ele);
    
    canoeDrawableBaseInit(
                          &ele->base,
                          NULL,
                          (DrawFunction)CANOE_FOUNDATION_FUNC_DRAW(CanoeConditionPanel),
                          (DestroyFunction)CANOE_FOUNDATION_FUNC_DESTROY(CanoeConditionPanel),
                          location,
                          canoeVector2f(300, 300),
                          addedTo,
                          NULL,
                          "COND_DPLY"
                          );
    
    ele->fuleGgaue = canoeCanoeCircularGagueNew(canoeVector2f(20, 40), 80.0);
    ele->satGgaue = canoeCanoeCircularGagueNew(canoeVector2f(20, 120), 80.0);
    ele->battGgaue = canoeCanoeCircularGagueNew(canoeVector2f(20, 200), 80.0);
    
    canoeAddChild(&ele->base, ele->fuleGgaue);
    canoeAddChild(&ele->base, ele->satGgaue);
    canoeAddChild(&ele->base, ele->battGgaue);
    
    ele->elevSlider = canoeCanoeSliderNew(canoeVector2f(125, 245), 150, "ELEVATOR");
    ele->aileron = canoeCanoeSliderNew(canoeVector2f(125, 205), 150, "AILRON");
    ele->rudder = canoeCanoeSliderNew(canoeVector2f(125, 165), 150, "RUDDER");
    ele->throttle = canoeCanoeSliderNew(canoeVector2f(125, 125), 150, "THROTTLE");

    canoeAddChild(&ele->base, ele->elevSlider);
    canoeAddChild(&ele->base, ele->aileron);
    canoeAddChild(&ele->base, ele->rudder);
    canoeAddChild(&ele->base, ele->throttle);
    
    ele->throttle->value = 0.763;
    
    return ele;
}

CANOE_FOUNDATION_FUNC_DRAW_DECL(CanoeConditionPanel, ele)
{
    CANOE_FOUNDATION_CHECK_REBUILD_INDEX(CanoeConditionPanel, ele);
    
    glPushAttrib(GL_LINE_BIT);
    glPushMatrix();
    {
        CanoeVector2f world = canoeCalculateWorldCoordinate(&ele->base);
        CanoeVector2f size = ele->base.size;
        glTranslatef(world.x, world.y, 0);
        
        /* draw background */
        setColor(16, 18, 47);
        glBegin(GL_QUADS);
        {
            glVertex2f(0, 0);
            glVertex2f(0, size.y);
            glVertex2f(size.x, size.y);
            glVertex2f(size.x, 0);
        }
        glEnd();
        
        sharedFontInstanceRender(ele->entFont, 48, 268, "BATT. A", NULL);
        sharedFontInstanceRender(ele->entFont, 48, 268-80, "BATT. B", NULL);
        sharedFontInstanceRender(ele->entFont, 48, 268-160, "M. FUEL", NULL);
        
        /* temperature info */
        sharedFontInstanceRender(ele->entFont, 20, 20, "SAT: ", NULL);
        sharedFontInstanceRender(ele->font, 43, 20, "+32°C", NULL);
        
        sharedFontInstanceRender(ele->entFont, 80, 20, "IMU: ", NULL);
        sharedFontInstanceRender(ele->font, 100, 20, " +48°C", NULL);
        
        /* data link info */
        sharedFontInstanceRender(ele->entFont, 220, 30, "TX: ", NULL);
        sharedFontInstanceRender(ele->entFont, 220, 20, "RX: ", NULL);
        
        sharedFontInstanceRender(ele->font, 240, 30, "15dBm", NULL);
        sharedFontInstanceRender(ele->font, 240, 20, "-107dBm", NULL);
    }
    glPopMatrix();
    glPopAttrib();
    
    canoeRenderChildren(&ele->base);
    
    drawBoundary(ele);
}

CANOE_FOUNDATION_FUNC_DESTROY_DECL(CanoeConditionPanel, ele)
{
#ifdef CANOE_LOG_DESTROY
    printf("...\n");
#endif /* CANOE_LOG_DESTROY */
    
    SV_RELEASE(ele->base.children);
    free(ele->base.children);
}
