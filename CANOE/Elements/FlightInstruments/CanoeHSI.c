/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeHSI.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"
#include <math.h>

CANOE_FOUNDATION_FUNC_UPDATE_BBOX_DECL(CanoeHSI, ele)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
        EXPLODE_COMPONENT2D(ele->base.location),
        EXPLODE_COMPONENT2D(canoeVector2fAdd(ele->base.location, ele->base.size))
    );
    
    spatialIndexerUpdateBoundary(&ele->base.spatialNode, newBoundary);
}

static void addedTo(void *element, void *parent)
{
    ((CanoeHSI *)element)->font = sharedFontRegisterUsage(parent, "MS33558", 14);
}

CanoeHSI *canoeHSINew(CanoeVector2f location, double height)
{
    CanoeHSI *ele = malloc(sizeof(CanoeHSI));
    CANOE_MALLOC_CHK(label);
    
    canoeDrawableBaseInit(
                          &ele->base,
                          NULL,
                          (DrawFunction)CANOE_FOUNDATION_FUNC_DRAW(CanoeHSI),
                          (DestroyFunction)CANOE_FOUNDATION_FUNC_DESTROY(CanoeHSI),
                          location,
                          canoeVector2f(height, height),
                          addedTo,
                          NULL,
                          "HSI"
                          );
    
    ele->currentCourse = 0.0;
    ele->selectedCourse = 0.0;
    ele->courseDev = 0.8;
    
    ele->scale = height / 300.0;
    
    return ele;
}



CANOE_FOUNDATION_FUNC_DRAW_DECL(CanoeHSI, ele)
{
    if(ele->base.boundaryDirty == true)
    {
        ele->base.boundaryDirty = false;
        CANOE_FOUNDATION_FUNC_UPDATE_BBOX(CanoeHSI)(ele);
    }
    
    glPushAttrib(GL_LINE_BIT);
    glPushMatrix();
    {
        CanoeVector2f world = canoeCalculateWorldCoordinate((CanoeDrawableBase *)ele);
        glTranslatef(world.x, world.y, 0.0);
     
        glScalef(ele->scale, ele->scale, ele->scale);
        
        setColor(60, 60, 60);
        glBegin(GL_QUADS);
        {
            glVertex2f(0, 0);
            glVertex2f(0, 300);
            glVertex2f(300, 300);
            glVertex2f(300, 0);
        }
        glEnd();
        
        glTranslatef(ele->base.size.x / 2.0 / ele->scale, ele->base.size.y / 2.0 / ele
                     ->scale, 0.0);
        
        setColor(0, 0, 0);
        /* draw circle */
        glBegin(GL_POLYGON);
        {
            circleVertexes(0, 360, 5, 0, 0, 146.0);
        }
        glEnd();
        
        glLineWidth(2.0);
        
        /* draw indicator */
        {
            setColor(255, 255, 255);
            
            drawLine(-7, 25, -7, -25);
            drawLine(7, 25, 7, -25);
            drawLine(-15, -25, -7, -25);
            drawLine(15, -25, 7, -25);
            drawLine(-25, 0, -7, 0);
            drawLine(25, 0, 7, 0);
        }
        
        /* current course pointer */
        glBegin(GL_LINE_STRIP);
        {
            glVertex2f(0, 140);
            glVertex2f(5, 130);
            glVertex2f(-5, 130);
            glVertex2f(0, 140);
        }
        glEnd();
        
        /* rotate */
        glRotatef(ele->currentCourse, 0, 0, 1);
        
        /* draw scales */
        setColor(255, 255, 255);
        glBegin(GL_LINES);
        {
            for(int i = 0; i < 360; i += 10)
            {
                glVertex2f(sin(DEG2RAD(i + 5)) * 146, cos(DEG2RAD(i + 5)) * 146);
                glVertex2f(sin(DEG2RAD(i + 5)) * 141, cos(DEG2RAD(i + 5)) * 141);
            }
        }
        glEnd();
        
        glLineWidth(2.5);
        glBegin(GL_LINES);
        {
            for(int i = 0; i < 360; i += 10)
            {
                glVertex2f(sin(DEG2RAD(i)) * 146, cos(DEG2RAD(i)) * 146);
                glVertex2f(sin(DEG2RAD(i)) * 136, cos(DEG2RAD(i)) * 136);
            }
        }
        glEnd();
        
        for(int i = 0; i < 360; i += 30)
        {
            char str[16];
            sprintf(str, "%d", i / 10);
            
            float x = sin(DEG2RAD(i)) * 112;
            float y = cos(DEG2RAD(i)) * 112;
            
            glPushMatrix();
            glTranslated(x, y, 0);
            glRotatef(90 - i + 270, 0, 0, 1);
            
            
            sharedFontInstanceRender(ele->font,
                                    -sharedFontCalcFontWidth(ele->font, str) / 2, 0,
                                     str,
                                     NULL);
            
            glPopMatrix();
        }
        
        glRotatef(ele->selectedCourse, 0, 0, 1);
        
        /* draw selected course pointer */
        setColor(255, 255, 255);
        glBegin(GL_LINE_STRIP);
        {
            glVertex2f(0, 140); glVertex2f(5, 135); glVertex2f(5, 100);
            glVertex2f(17, 100); glVertex2f(17, 90); glVertex2f(5, 90);
            glVertex2f(5, 80); glVertex2f(-5, 80); glVertex2f(-5, 90);
            glVertex2f(-17, 90); glVertex2f(-17, 100); glVertex2f(-5, 100);
            glVertex2f(-5, 135); glVertex2f(0, 140);
        }
        glEnd();
        
        glBegin(GL_LINE_STRIP);
        {
            glVertex2f(0, -140);
            glVertex2f(5, -135);
            glVertex2f(5, -80);
            glVertex2f(-5, -80);
            glVertex2f(-4, -135);
            glVertex2f(0, -140);
        }
        glEnd();
        
        glBegin(GL_LINE_STRIP);
        {
            glVertex2f(0, 60);
            glVertex2f(10, 50);
            glVertex2f(-10, 50);
            glVertex2f(0, 60);
        }
        glEnd();
        
        glBegin(GL_LINE_STRIP); circleVertexes(0, 370, 10, -30, 0.0, 4.0); glEnd();
        glBegin(GL_LINE_STRIP); circleVertexes(0, 370, 10, -60, 0.0, 4.0); glEnd();
        glBegin(GL_LINE_STRIP); circleVertexes(0, 370, 10, 30, 0.0, 4.0); glEnd();
        glBegin(GL_LINE_STRIP); circleVertexes(0, 370, 10, 60, 0.0, 4.0); glEnd();
        
        /* draw Course Deviation Indicator */
        setColor(255, 0, 255);
        glPushMatrix();
        glTranslated(ele->courseDev * 60.0, 0, 0);
        glBegin(GL_LINE_STRIP);
        {
            glVertex2f(-5, 78); glVertex2f(5, 78);
            glVertex2f(5, -78); glVertex2f(-5, -78);
            glVertex2f(-5, 78);
        }
        glEnd();
        glPopMatrix();
    }
    glPopMatrix();
    glPopAttrib();
    
    drawBoundary(ele);
}

CANOE_FOUNDATION_FUNC_DESTROY_DECL(CanoeHSI, ele)
{
#ifdef CANOE_LOG_DESTROY
    printf("hsi destroy\n");
#endif /* CANOE_LOG_DESTROY */
    
    SV_RELEASE(ele->base.children);
    free(ele->base.children);
}
