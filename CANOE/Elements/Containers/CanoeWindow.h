/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_WINDOW_H__
#define CANOE_WINDOW_H__

#include <stdio.h>
#include "CanoeDrawableBase.h"

#define CYAN_WINDOW_TITLE_MAX_LENGTH 1024

#define CANOE_WINDOW_DEFAULT_SIZE canoeVector2f(640, 480)

typedef struct
{
    EXTEND(CanoeDrawableBase);
    
    char title[CYAN_WINDOW_TITLE_MAX_LENGTH];
    CanoeColor3f backgroundColor;
    
    void *backendInstance;
} CanoeWindow;

SV_DECLARE(CanoeWindowContainer, CanoeWindow *);

/**
 *  Create a new window instance using default settings
 *
 *  @param title Title of the window
 *
 *  @return Returns a pointer to the window instance
 */
#define canoeWindowNewDefault(title)    \
    canoeWindowNew(title, CANOE_WINDOW_DEFAULT_SIZE, canoeVector2f(10.0, 10.0))

#define canoeWindowNewSize(title, size) \
    canoeWindowNew(title, size, canoeVector2f(10.0, 10.0))

CanoeWindow *canoeWindowNew(const char *restrict title, CanoeVector2f size, CanoeVector2f location);

/**
 *  Set the size of a given window instance
 *
 *  @param window A pointer to the window instance
 *  @param size   The size
 *
 *  @return Returns the input window instance pointer
 */
CanoeWindow *canoeWindowSetSize(CanoeWindow *window, const CanoeVector2f size);

/**
 *  Set the location of a given window instance
 *
 *  @param window   A pointer to the window instance
 *  @param location The location
 *
 *  @return A pointer to the input window
 */
CanoeWindow *canoeWindowSetLocation(CanoeWindow *window, const CanoeVector2f location);

/**
 *  Set the color of a given window instance
 *
 *  @param window A pointer to the window instance
 *  @param color  The color
 *
 *  @return Returns the input window instance pointer
 */
CanoeWindow *canoeWindowSetBackgroundColor(CanoeWindow *window, const CanoeColor3f color);

/**
 *  Initialize the window instance and its backend instance. Also initialize 
 *  the OpenGL environment and register callback functions of the GLFW.
 *
 *  @param window Window instance
 *
 *  @return A pointer to the input instance, returns NULL on failure
 */
CanoeWindow *canoeWindowInit(CanoeWindow *window);

/**
 *  Close a window
 *
 *  @param window A pointer to the window
 */
void canoeWindowClose(CanoeWindow *window);

/**
 *  Window rendering function, will be called by the renderer
 *
 *  @param window A pointer to the window instance
 */
void canoeWindowDraw(CanoeWindow *window);

/**
 *  Release the memory used by the window
 *
 *  @param window The pointer to the window
 */
void canoeWindowDestroy(CanoeWindow *window);

#endif /* defined(CANOE_WINDOW_H__) */
