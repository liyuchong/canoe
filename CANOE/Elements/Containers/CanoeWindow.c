/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeWindow.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"

void canoeWindowClose(CanoeWindow *window)
{
    glfwSetWindowShouldClose(window->backendInstance, 1);
}

CanoeWindow *canoeWindowInit(CanoeWindow *window)
{
    glfwSetErrorCallback(glfwErrorCallback);
    
    GLFWwindow *glfwWindow = glfwCreateWindow(window->base.size.x,
                                              window->base.size.y,
                                              window->title, NULL, NULL);
    if(!glfwWindow)
    {
        glfwTerminate();
        return NULL;
    }
    
    glfwMakeContextCurrent(glfwWindow);
    
    openglInit();
    
    glfwSwapInterval(1);
    glfwWindowHint(GLFW_SAMPLES, 4);
    
    glfwSetKeyCallback(glfwWindow, glfwKeyCallback);
    glfwSetMouseButtonCallback(glfwWindow, glfwMouseButtonActionCallback);
    glfwSetCursorPosCallback(glfwWindow, glfwMouseCursorPositionCallback);
    glfwSetWindowFocusCallback(glfwWindow, glfwWindowFocusCallback);
    glfwSetWindowSizeCallback(glfwWindow, glfwWindowSizeCallback);
    glfwSetScrollCallback(glfwWindow, glfwScrollCallback);
    glfwSetCursorPosCallback(glfwWindow, glfwCursorPositionCallback);
    
    glfwSetCharCallback(glfwWindow, glfwCharCallback);
    
    int width, height;
    glfwGetFramebufferSize(glfwWindow, &width, &height);
    
    float dpiRatio = width / window->base.size.x;
        
    window->backendInstance = (void *)glfwWindow;
    window->base.dpiRatio = dpiRatio;
    
    return window;
}

CanoeWindow *canoeWindowNew(const char *restrict title, CanoeVector2f size, CanoeVector2f location)
{
    CanoeWindow *window = malloc(sizeof(CanoeWindow));
    CANOE_MALLOC_CHK(window);
    
    /* initialize the base structure */
    canoeDrawableBaseInit(
        &window->base,                 /* the base structure */
        NULL,                          /* no parent yet */
        (DrawFunction)canoeWindowDraw, /* the draw function */
        (DestroyFunction)canoeWindowDestroy,
        location,         /* initialize position */
        size,      /* window size */
        NULL,
        NULL,
        "WINDOW"
    );
    
    SET_DIRTY(window);
    
    window->backendInstance = NULL;
    window->backgroundColor = canoeColor3f(0.235, 0.235, 0.235);
    
    strcpy(window->title, title);
        
    return canoeWindowSetLocation(canoeWindowInit(window), location);
}

CanoeWindow *canoeWindowSetSize(CanoeWindow *window, const CanoeVector2f size)
{
    window->base.size = size;
    glfwSetWindowSize((GLFWwindow *)window->backendInstance, size.x, size.y);
    return window;
}

CanoeWindow *canoeWindowSetLocation(CanoeWindow *window, const CanoeVector2f location)
{
    glfwSetWindowPos((GLFWwindow *)window->backendInstance, location.x, location.y);
    
    return window;
}

CanoeWindow *canoeWindowSetBackgroundColor(CanoeWindow *window, const CanoeColor3f color)
{
    window->backgroundColor = color;
    
    return window;
}

void canoeWindowDraw(CanoeWindow *window)
{
    if(IS_DIRTY(window))
    {
        DIRTY_COUNTER_DEC(window);
        
        glClear(GL_COLOR_BUFFER_BIT);
        //glfwSwapBuffers(window->backendInstance);
        //glClear(GL_COLOR_BUFFER_BIT);
    }
    
    canoeRenderChildren(&window->base);
}

void canoeWindowDestroy(CanoeWindow *window)
{
#ifdef CANOE_LOG_DESTROY
    printf("destroy window\n");
#endif /* CANOE_LOG_DESTROY */
    
    for(size_t i = 0; i < SV_SIZE(window->base.children); i++)
    {
        ((CanoeDrawableBase *)SV_AT(window->base.children, i))->destroy(SV_AT(window->base.children, i));
    }
    
    SV_RELEASE(window->base.children);
    free(window->base.children);
}
