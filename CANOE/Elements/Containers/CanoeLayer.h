/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef __CANOE_LAYER_H__
#define __CANOE_LAYER_H__

#include <stdio.h>
#include "CanoeDrawableBase.h"

typedef struct
{
    CanoeDrawableBase base;
} CanoeLayer;

/**
 *  Create a new layer
 *
 *  @param size Size of the layer
 *
 *  @return A pointer to the new layer
 */
CanoeLayer *canoeLayerNew(CanoeVector2f size);

/**
 *  Set the location of the layer
 *
 *  @param layer    A pointer to the layer
 *  @param location New location
 *
 *  @return A pointer to the input layer
 */
CanoeLayer *canoeLayerSetLocation(CanoeLayer *layer, CanoeVector2f location);

/**
 *  Add a child to the layer
 *
 *  @param layer A pointer to the layer
 *  @param child Child to be added
 *
 *  @return A pointer to the input layer
 */
CanoeLayer *canoeLayerAddChild(CanoeLayer *layer, void *child);

/**
 *  Draw function
 *
 *  @param layer A pointer to the layer
 */
void canoeLayerDraw(CanoeLayer *layer);

/**
 *  Clean up used memory
 *
 *  @param window A pointer to the layer
 */
void canoeLayerDestroy(CanoeLayer *layer);

#endif /* defined(__CANOE_LAYER_H__) */
