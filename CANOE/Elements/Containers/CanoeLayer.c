/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeLayer.h"
#include "CanoeRenderer.h"
#include "CanoeGLUtils.h"

void canoeLayerUpdateBoundingBox(CanoeLayer *layer)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(layer->base.location),
                       EXPLODE_COMPONENT2D(canoeVector2fAdd(layer->base.location, layer->base.size)));
    
    spatialIndexerUpdateBoundary(&layer->base.spatialNode, newBoundary);
}


CanoeLayer *canoeLayerNew(CanoeVector2f size)
{
    CanoeLayer *layer = malloc(sizeof(CanoeLayer));
    CANOE_MALLOC_CHK(layer);
    
    /* initialize the base structure */
    canoeDrawableBaseInit(
        &layer->base,                        /* the base structure */
        NULL,                                /* no parent yet */
        (DrawFunction)canoeLayerDraw,        /* the draw function */
        (DestroyFunction)canoeLayerDestroy,  /* destroy function */
        canoeVector2f(0, 0),                 /* initialize position */
        size,                                /* layer size */
        NULL,                                /* added to parent */
        NULL,                                /* removed from parent */
        "LAYER"                              /* element name */
    );

    canoeLayerUpdateBoundingBox(layer);
    
    return layer;
}

CanoeLayer *canoeLayerSetLocation(CanoeLayer *layer, CanoeVector2f location)
{
    layer->base.location = location;
    canoeLayerUpdateBoundingBox(layer);
    
    return layer;
}

void canoeLayerDraw(CanoeLayer *layer)
{
    VECTOR(SpatialNode *) nodes;
    SV_INIT(&nodes, 2, SpatialNode *);
    
    drawBoundary(layer);
    
    pushMatrix();
    
    translatef(layer->base.location.x, layer->base.location.y, 0);
    
    // TODO: change search API?
    SpatialBoundary searchBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(canoeVector2f(0.0, 0.0)),
                       EXPLODE_COMPONENT2D(layer->base.size));
    
    //SpatialNodes *nodes = spatialIndexerSearch(&window->base.indexer, &searchBoundary);
    spatialIndexerSearch((SpatialNodes *)&nodes, &layer->base.indexer, &searchBoundary);
    
    spatialBoundaryDestroy(&searchBoundary);
    
    for(size_t i = 0; i < SV_SIZE(&nodes); i++)
    {
        ((CanoeDrawableBase *)SV_AT(&nodes, i)->data)->draw(SV_AT(&nodes, i)->data);
    }
    
    SV_RELEASE(&nodes);
    
    popMatrix();
}

void canoeLayerDestroy(CanoeLayer *layer)
{
#ifdef CANOE_LOG_DESTROY
    printf("destroy layer\n");
#endif /* CANOE_LOG_DESTROY */
    
    for(size_t i = 0; i < SV_SIZE(layer->base.children); i++)
    {
        ((CanoeDrawableBase *)SV_AT(layer->base.children, i))->destroy(SV_AT(layer->base.children, i));
    }
    
    SV_RELEASE(layer->base.children);
    free(layer->base.children);
}

CanoeLayer *canoeLayerAddChild(CanoeLayer *layer, void *child)
{
    if(!layer)
        return NULL;
    
    SV_PUSH_BACK(layer->base.children, child);
    ((CanoeDrawableBase *)child)->parent = layer;
    
    if(((CanoeDrawableBase *)child)->addedToParent)
        ((CanoeDrawableBase *)child)->addedToParent(child, layer);
    
    // TODO: add element to the spatial indexer(window->base.indexer)
    spatialIndexerAddElement(&layer->base.indexer, &((CanoeDrawableBase *)child)->spatialNode);
    
    return layer;
}

