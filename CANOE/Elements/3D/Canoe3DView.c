/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "Canoe3DView.h"
#include "CanoeGLUtils.h"

#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include <GLUT/GLUT.h>

void handler(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_MOUSE_SCROLL_EXTRACT_ARGUMENTS(Canoe3DView *, c3d, x, y);
    
    printf("%f %f\n", x, y);
    
    c3d->scale += y / 10.0;
}


double cursorX = 0;
double cursorY = 0;

static void cursorMoved(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_CURSOR_POSITION_EXTRACT_ARGUMENTS(Canoe3DView *, c3d, x, y);
    
    if (c3d->locked)
    {
        c3d->alpha += (x - cursorX) / 5.0;
        c3d->beta += (y - cursorY) / 5.0;
    }
    
    cursorX = x;
    cursorY = y;
}

static void mouseAction(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_MOUSE_BUTTON_EXTRACT_ARGUMENTS(Canoe3DView *, c3d, pos, button, action, mods);
    
    if (button == CANOE_MOUSE_BUTTON_LEFT && action == CANOE_MOUSE_PRESS)
    {
        c3d->locked = true;
    }
    
    if (button == CANOE_MOUSE_BUTTON_LEFT && action == CANOE_MOUSE_RELEASE)
    {
        c3d->locked = false;
    }
}

Canoe3DView *canoe3dViewNew(CanoeVector2f location, CanoeVector2f size)
{
    /* allocate memory for the new element */
    Canoe3DView *c3d = malloc(sizeof(Canoe3DView));
    CANOE_MALLOC_CHK(c3d);
    
    canoeDrawableBaseInit(&c3d->base,
                          NULL,
                          (DrawFunction)canoe3dViewDraw,
                          (DestroyFunction)canoe3dViewDestroy,
                          location,
                          size,
                          NULL,
                          NULL,
                          "3DVIEW");
    
    c3d->scale = 1.0;
    c3d->locked = false;
    c3d->alpha = 0.0;
    c3d->beta = 0.0;
    
    /* rebuild bounding box */
    canoe3dViewUpdateBoundingBox(c3d);
    
    canoeRegisterEvent(&c3d->base, CANOE_EVENT_MOUSE_SCROLL, handler);
    canoeRegisterEvent(&c3d->base, CANOE_EVENT_CURSOR_POSITION, cursorMoved);
    canoeRegisterEvent(&c3d->base, CANOE_EVENT_MOUSE_BUTTON_ACTION, mouseAction);
    
    return c3d;
}

void canoe3dViewUpdateBoundingBox(Canoe3DView *canoe3d)
{
    /* update boundary */
    SpatialBoundary newBoundary =
    spatialBoundaryNew(2,
                       EXPLODE_COMPONENT2D(canoe3d->base.location),
                       EXPLODE_COMPONENT2D(canoeVector2fAdd(canoe3d->base.location, canoe3d->base.size)));
    
    spatialIndexerUpdateBoundary(&canoe3d->base.spatialNode, newBoundary);
}

static const float vertex_list[][3] =
{
    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    -0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    -0.5f, -0.5f, 0.5f,
    0.5f, -0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,
    0.5f, 0.5f, 0.5f,
};

// 将要使用的顶点的序号保存到一个数组里面

static const GLint index_list[][2] =
{
    {0, 1},
    {2, 3},
    {4, 5},
    {6, 7},
    {0, 2},
    {1, 3},
    {4, 6},
    {5, 7},
    {0, 4},
    {1, 5},
    {7, 3},
    {2, 6}
};

// 绘制立方体

void DrawCube(void)
{
    int i,j;
    
    glBegin(GL_LINES);
    for(i=0; i<12; ++i) // 12 条线段
        
    {
        for(j=0; j<2; ++j) // 每条线段 2个顶点
            
        {
            glVertex3fv(vertex_list[index_list[i][j]]);
        }
    }
    glEnd();
}

void canoe3dViewDraw(Canoe3DView *c3d)
{
    drawBoundary(c3d);
    
    pushMatrix();
    
    glColor3f(1, 1, 1);
    
    //glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    
//    glDisable(GL_DEPTH_TEST);
    //gluPerspective(90, ((float)canoe3d->base.size.x) / (float)(canoe3d->base.size.y), 0.01, 3000);
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
//    gluLookAt(0.0, 0, canoe3d->base.size.y / 2.0f, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    
    //glRotatef(20, 0, 0, 1);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glScalef(c3d->scale, c3d->scale, c3d->scale);
    
    glRotatef(c3d->beta, 1.0, 0.0, 0.0);
    glRotatef(c3d->alpha, 0.0, 1.0, 0.0);
    
    DrawCube();
    
    popMatrix();
}

void canoe3dViewDestroy(Canoe3DView *canoe3d)
{
#ifdef CANOE_LOG_DESTROY
    printf("canoe 3d view destroy\n");
#endif /* CANOE_LOG_DESTROY */
}
