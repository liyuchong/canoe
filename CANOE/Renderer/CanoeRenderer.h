/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef __CANOE_RENDERER_H__
#define __CANOE_RENDERER_H__

#include <stdio.h>
#include <math.h>

#include "CanoeDrawableBase.h"
#include "CanoeLabel.h"
#include "CanoeWaveDisplay.h"
#include "CanoeWindow.h"

#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>

#include <FTGL/ftgl.h>

/** Event callbacks,
 *  File: EventCallbacks.c */
void glfwErrorCallback(int, const char *);

void glfwCharCallback               (GLFWwindow *, unsigned int);
void glfwKeyCallback                (GLFWwindow *, int, int, int, int);
void glfwWindowFocusCallback        (GLFWwindow *, int);
void glfwMouseButtonActionCallback  (GLFWwindow *, int, int, int);
void glfwMouseCursorPositionCallback(GLFWwindow *, double, double);
void glfwWindowSizeCallback         (GLFWwindow *, int, int);
void glfwScrollCallback             (GLFWwindow *, double, double);
void glfwCursorPositionCallback     (GLFWwindow *, double, double);


/**
 *  Initialize the OpenGL environment
 */
void openglInit(void);

/**
 *  Find the frontend window instance by a given GLFW window instance
 *
 *  @param glfwWindow The GLFW window instance
 *  @param windows    The window container
 *
 *  @return Returns a pointer to the frontend window instance, returns NULL if
 *          Not found.
 */
CanoeWindow *findFrontEndWindow(
    GLFWwindow *glfwWindow, CanoeWindowContainer *windows
);

// TODO: change to struct
void renderWaveDisplay(CanoeWaveDisplay *display);


void rendererMakeContext(CanoeWindow *window);
int canoeRendererInit(void);
void canoeRenderLoop(CanoeWindowContainer *stuff);

#define DEG2RAD(x) ((x) * (3.1415926 / 180.0))

static inline
void circleVertexes(int startAngle, int endAngle, int step, float x, float y, float radius)
{
    for(int i = startAngle; i <= endAngle; i += step)
    {
        glVertex2f(sin(DEG2RAD(i)) * radius + x,
                   cos(DEG2RAD(i)) * radius + y);
    }
}

#endif /* defined(__CANOE_RENDERER_H__) */
