/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef __CANOE_GLUTILS_H__
#define __CANOE_GLUTILS_H__

/**
 *  Save transform matrix
 */
void pushMatrix(void);

/**
 *  Restore transform matrix
 */
void popMatrix(void);

/**
 *  Translate
 *
 *  @param x x
 *  @param y y
 *  @param z z
 */
void translatef(float x, float y, float z);

/**
 *  Set grayscale color
 *
 *  @param gray color
 */
void CANOE_BUTTON_IDLE_BODY_COLOR__(int gray);

/**
 *  Set RGB color(0 - 255)
 *
 *  @param r R
 *  @param g G
 *  @param b B
 */
void setColor(int r, int g, int b);

/**
 *  Set RGB color(0 - 255)
 *
 *  @param r R
 *  @param g G
 *  @param b B
 */
void setColorGray(int gray);

/**
 *  Draw line
 *
 *  @param x1 x1
 *  @param y1 y1
 *  @param x2 x2
 *  @param y2 y2
 */
void drawLine(float x1, float y1, float x2, float y2);

/**
 *  Draw filled rect
 *
 *  @param x1 x1
 *  @param y1 y1
 *  @param x2 x2
 *  @param y2 y2
 */
void drawRect(float x1, float y1, float x2, float y2);

/**
 *  Set width of lines
 *
 *  @param width width
 */
void setLineWidth(float width);

/**
 *  Begin line strip mode
 */
void lineStripBegin(void);

/**
 *  End line strip mode
 */
void lineStripEnd(void);

/**
 *  Add 2d vertex
 *
 *  @param x x
 *  @param y y
 */
void addVertex2f(float x, float y);

/**
 *  Draw boundary of an item
 *
 *  @param item A pointer to the item
 */
void drawBoundary(void *item);

void canoeScissor(float x, float y, float w, float h);

#endif /* defined(__CANOE_GLUTILS_H__) */
