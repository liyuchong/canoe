/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeRenderer.h"

CanoeWindow *findFrontEndWindow(
        GLFWwindow *glfwWindow, CanoeWindowContainer *windows
) {
    for(size_t i = 0; i < SV_SIZE(windows); i++)
    {
        if((GLFWwindow *)SV_AT(windows, i)->backendInstance == glfwWindow)
            return SV_AT(windows, i);
    }
    
    return NULL;
}

void rendererMakeContext(CanoeWindow *window)
{
    glfwMakeContextCurrent((GLFWwindow *)window->backendInstance);
}

void openglInit(void)
{
    glShadeModel(GL_SMOOTH);
    glEnable(GL_MULTISAMPLE);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glPointSize(2.0);
    glClearColor(0.15, 0.15, 0.15, 0.0);
    
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

int canoeRendererInit(void)
{
    return glfwInit();
}

void canoeRenderLoop(CanoeWindowContainer *stuff)
{
    while(1)
    {
        /* close those windows which requested to be closed */
        for(size_t i = 0; i < SV_SIZE(stuff); i++)
        {
            GLFWwindow *window = (GLFWwindow *)SV_AT(stuff, i)->backendInstance;
            if(glfwWindowShouldClose(window))
            {
                canoeWindowDestroy(SV_AT(stuff, i));
                
                // TODO: clean up
                glfwDestroyWindow(window);
                SV_ERASE_AT(stuff, i);
                
                i = 0;
            }
        }
        
        /* render each window */
        for(size_t i = 0; i < SV_SIZE(stuff); i++)
        {
            int width, height;
            
            /* get glfw instance */
            GLFWwindow *window = (GLFWwindow *)SV_AT(stuff, i)->backendInstance;
            
            /* switch context */
            glfwMakeContextCurrent(window);
            
            /* update height */
            glfwGetWindowSize(window, &width, &height);
            SV_AT(stuff, i)->base.size = canoeVector2f(width, height);
            
            /* initialize */
            //glClear(GL_COLOR_BUFFER_BIT);
            
            CanoeColor3f backgroundColor = SV_AT(stuff, i)->backgroundColor;
            glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, 0);
            
            glLoadIdentity();
            glOrtho(0, width, 0, height, -10, 10);
            
            /* actually draw window */
            SV_AT(stuff, i)->base.draw(SV_AT(stuff, i));
            
            glfwSwapBuffers(window);
        }
        
        glfwPollEvents();
        
        /* terminate if there is no more window exist */
        if(SV_SIZE(stuff) == 0)
        {
            glfwTerminate();
            break;
        }
    }
}

void drawBoundary(void *item)
{
    CanoeDrawableBase *base = (CanoeDrawableBase *)item;
    
    glPushMatrix();
    glPushAttrib(GL_CURRENT_BIT);
    {
        CanoeColor4f color = colorFromHash(base);
        glColor4f(color.r, color.g, color.b, color.a);
        
        glBegin(GL_LINE_STRIP);
        {
            CanoeVector2f lower = canoeVector2f(base->spatialNode.boundary.lower[0], base->spatialNode.boundary.lower[1]);
            CanoeVector2f upper = canoeVector2f(base->spatialNode.boundary.upper[0], base->spatialNode.boundary.upper[1]);
            
            glVertex3f(lower.x, lower.y, 0);
            glVertex3f(lower.x, upper.y, 0);
            glVertex3f(upper.x, upper.y, 0);
            glVertex3f(upper.x, lower.y, 0);
            glVertex3f(lower.x, lower.y, 0);
        }
        glEnd();
    }
    glPopAttrib();
    glPopMatrix();
}

void pushMatrix(void)
{
    glPushMatrix();
}

void popMatrix(void)
{
    glPopMatrix();
}

void translatef(float x, float y, float z)
{
    glTranslatef(x, y, z);
}

void setColorGray(int gray)
{
    glColor3f(gray / 255.0, gray / 255.0, gray / 255.0);
}

void setColor(int r, int g, int b)
{
    glColor3f(r / 255.0, g / 255.0, b / 255.0);
}

void setLineWidth(float width)
{
    glLineWidth(width);
}

void drawLine(float x1, float y1, float x2, float y2)
{
    glBegin(GL_LINES);
    
    glVertex3f(x1, y1, 0.0f);
    glVertex3f(x2, y2, 0.0f);
    
    glEnd();
}

void lineStripBegin(void)
{
    glBegin(GL_LINE_STRIP);
}

void lineStripEnd(void)
{
    glEnd();
}

void addVertex2f(float x, float y)
{
    glVertex3f(x, y, 0.0);
}

void drawRect(float x1, float y1, float x2, float y2)
{
    glBegin(GL_QUADS);
    
    glVertex3f(x1, y1, 0.0f);
    glVertex3f(x1, y2, 0.0f);
    glVertex3f(x2, y2, 0.0f);
    glVertex3f(x2, y1, 0.0f);
    
    glEnd();
}

void renderWaveDisplay(CanoeWaveDisplay *display)
{
    if(display->base.boundaryDirty)
    {
        display->base.boundaryDirty = false;
        canoeWaveDisplayUpdateBoundary(display);
    }
    
    //drawBoundary(display);
}

void canoeScissor(float x, float y, float w, float h)
{
    glScissor(x * 2, y * 2, w * 2, h * 2);
}
