/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include <stdarg.h>

#include "EventManager.h"
#include "SimpleMap.h"

EventManager *eventManagerCreate(void)
{
    return simpleMapCreate();
}

void eventManagerDestroy(EventManager *eventManager)
{
    // TODO: clean up
}

void evmRegisterEvent(
        EventManager *manager, void *majorKey, long minorKey, void *handler
) {
    /* create new handler structure */
    EventHandler *newHandler = malloc(sizeof(EventHandler));
    
    if(!newHandler)
    {
        fprintf(stderr, "Event manager: error: out of memory\n");
        return;
    }
    
    newHandler->handler = handler;
    newHandler->next = NULL;
    
    /* check if this event has been registered */
    EventHandler *currentHandler = evmFindEvent(manager, majorKey, minorKey);
    if(currentHandler)
    {
        /* find the last handler */
        EventHandler *lastHandler = currentHandler;
        
        while (lastHandler->next) {
            lastHandler = lastHandler->next;
        }
        
        /* assign the new handler to be the next handler of the last one */
        lastHandler->next = newHandler;
        
        return;
    }
    
    /* find inner map */
    SimpleMap *innerEventMap = (SimpleMap *)simpleMapFind(manager, majorKey);
    
    /* if inner map was not found, create one and insert it to the outer map */
    if(!innerEventMap)
    {
        innerEventMap = simpleMapCreate();
        simpleMapInsert(manager, majorKey, innerEventMap);
    }

    /* insert data to the inner map */
    simpleMapInsert(innerEventMap, (void *)minorKey, newHandler);
}

void *evmFindEvent(EventManager *manager, void *majorKey, long minorKey)
{
    SimpleMap *innerMap = (SimpleMap *)simpleMapFind(manager, majorKey);
    
    return innerMap ? simpleMapFind(innerMap, (void *)minorKey): NULL;
}
