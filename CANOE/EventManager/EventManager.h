/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef __EVENT_MANAGER_H__
#define __EVENT_MANAGER_H__

#include <stdio.h>

#include "SimpleMap.h"

typedef void (*EventHandlerFunc) (void *, ...);
typedef SimpleMap EventManager;

typedef struct EventHandler EventHandler;

struct EventHandler
{
    void *handler;
    struct EventHandler *next;
};

/**
 *  Register an event
 *
 *  @param manager  The event manager
 *  @param majorKey The major key of the event id
 *  @param minorKey the minor key of the event id
 *  @param data     Event handler function
 */
void evmRegisterEvent(
    EventManager *manager, void *majorKey, long minorKey, void *handler
);

/**
 *  Find an event handler by a given major key and a given minor key
 *
 *  @param manager  The event manager
 *  @param majorKey The major key of the event id
 *  @param minorKey The minor key of the event id
 *
 *  @return The registered event handler, returns NULL if not found
 */
void *evmFindEvent(EventManager *manager, void *majorKey, long minorKey);

/**
 *  Create a new event manager and initialize it
 *
 *  @return The pointer to the new event manager
 */
EventManager *eventManagerCreate(void);

/**
 *  Clean the memory of the event manager
 *
 *  @param eventManager The pointer to the eventManager
 */
void eventManagerDestroy(EventManager *eventManager);

#endif /* defined(__EVENT_MANAGER_H__) */
