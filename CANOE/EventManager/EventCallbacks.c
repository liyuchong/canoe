/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include <stdio.h>

#include "CanoeRenderer.h"
#include "EventManager.h"
#include "CanoeExterns.h"
#include "CanoeEventDispatcher.h"

void glfwErrorCallback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

void glfwKeyCallback(GLFWwindow *window,
                     int key, int scanCode, int action, int modifier)
{
    canoeDispatchKeyPressedEvenet(key, scanCode, action, modifier);
}

void glfwCharCallback(GLFWwindow *window, unsigned int key)
{
    canoeDispatchCharPressedEvent(key);
}

void glfwWindowFocusCallback(GLFWwindow *window, int status)
{
    CanoeWindow *frontendWindow = findFrontEndWindow(window, &rootWindowContainer);

    /* Ignore lost focus event from GLFW, because we will process the lost focus
     * event using our own mechanism. */
    if(status == 1)
    {
        canoeAssignInputFocusDispatch(&currentInputFocusedElement,
                              &frontendWindow->base,
                              frontendWindow);
    }
}

void glfwMouseButtonActionCallback(GLFWwindow *window, int button, int action, int mods)
{
    double mouseX, mouseY;
    glfwGetCursorPos(window, &mouseX, &mouseY);

    /* find the frontend window */
    CanoeWindow *frontendWindow = findFrontEndWindow(window, &rootWindowContainer);
    
    /* Y direction flipped */
    CanoeVector2f mouse = canoeVector2f(
        mouseX, frontendWindow->base.size.y - mouseY);
    
    canoeDispatchMouseEvent(frontendWindow, mouse, button, action, mods);
}

void glfwWindowSizeCallback(GLFWwindow *window, int width, int height)
{
    CanoeWindow *frontEnd = findFrontEndWindow(window, &rootWindowContainer);
    
    canoeDispatchWindowSizeChangedEvent(frontEnd, width, height);
}

void glfwMouseCursorPositionCallback(GLFWwindow* window, double x, double y)
{

}

void glfwScrollCallback(GLFWwindow* window, double x, double y)
{
    canoeDispatchMouseScrollEvent(x, y);
}

void glfwCursorPositionCallback(GLFWwindow *window, double x, double y)
{
    canoeDispatchCursorPositionEvent(x, y);
}

#undef __CANOE_EVENT_DISPATCH
