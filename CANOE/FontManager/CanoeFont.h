/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_FONT_H__
#define CANOE_FONT_H__

#include "CanoeDrawableBase.h"

#include <stdio.h>
#include <FTGL/ftgl.h>
#include <stdbool.h>

#define FONT_DIRECTORY                  "/System/Library/Fonts"
#define FONT_LIST_FILE FONT_DIRECTORY   "/fonts.list"
#define FONT_DEFAULT_FONT_FILE          "/System/Library/Fonts/Helvetica.dfont"

#define FONT_NAME_MAX_LENGTH            1024
#define __LINE_BUFFER_LENGTH            1024

/**
 *  Load a font by its name. Font files are searched in a given directory which
 *  defined in 'CyanFont.h'
 *
 *  @param fontName Name of the font
 *  @param fontSize Size of the font
 *
 *  @return Returns the font instance, returns NULL on failure
 */
FTGLfont *fontLoad(char *fontName, const int fontSize);

/**
 *  Locate the path to a font by its name
 *
 *  @param font Font name
 *  @param path A pointer to the path buffer
 *
 *  @return Returns the input pointer to the path buffer
 */
char *fontLocateFileByName(char *font, char *path);

/**
 *  Destroy the font's memory and clean up
 *
 *  @param font A pointer to the font instance
 */
void fontDestroy(FTGLfont *font);

#endif /* defined(CANOE_FONT_H__) */
