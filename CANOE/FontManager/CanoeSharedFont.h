/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef __CANOE_SHARED_FONT_H__
#define __CANOE_SHARED_FONT_H__

#include <stdio.h>
#include <assert.h>

#include "FontHashMap.h"
#include "CanoeExterns.h"
#include "CanoeFont.h"
#include <FTGL/ftgl.h>

#define CANOE_SHARED_FONT_NAME_MAX_LENGTH 256

typedef struct
{
    char name[CANOE_SHARED_FONT_NAME_MAX_LENGTH]; /* font name        */
    int size;                                     /* font size        */
    CanoeColor4f color;                           /* font color       */
    
    void *instance;                               /* backend instance */
    void *context;
    int reference;                                /* reference count  */
    
} CanoeSharedFont;

/**
 *  Register a font usage in the shared font manager. Note that the font
 *  instance is not initialized at this point.
 *
 *  @param name Name of the font
 *  @param size Size of the font
 *
 *  @return The pointer to the shared font object
 */
CanoeSharedFont *sharedFontRegisterUsage(void *context, char *restrict name, const int size);

/**
 *  Release the shared font object
 *
 *  @param font The pointer to the shared font object
 */
void sharedFontRelease(void *context, CanoeSharedFont *font);

/**
 *  Render the shared font instance. Shall be called in an OpenGL context.
 *
 *  @param font          The pointer to the shared font
 *  @param x             x position
 *  @param y             y position
 *  @param string        The string to be rendered
 *  @param ^fontReloaded Will be invoked when the font is (re)loaded
 *
 *  @return The pointer to the input shared font instance
 */
CanoeSharedFont *sharedFontInstanceRender(
    CanoeSharedFont *font,
    const float x, const float y,
    char *string,
    void(^fontReloaded)(CanoeSharedFont *font)
);

/**
 *  Calculate the bounding box of the font when rendered using a given string
 *
 *  @param font   The pointer to the shared font
 *  @param string The string to be measured
 *  @param lower  The lower coordinate(bottom-left)
 *  @param upper  The upper coordinate(upper-right)
 *
 *  @return 0 on success, 1 on failure
 */
int sharedFontCalculateBoundingBox(
    CanoeSharedFont *font,
    char *restrict string, CanoeVector2f *lower, CanoeVector2f *upper
);

/**
 *  Calculate the width of the font when rendered using a given string
 *
 *  @param font The pointer to the font
 *  @param str  The string
 *
 *  @return Width
 */
static inline float sharedFontCalcFontWidth(CanoeSharedFont *font, char *str)
{
    CanoeVector2f upper, lower;
    sharedFontCalculateBoundingBox(font, str, &upper, &lower);
    
    return lower.x - upper.x;
}

CanoeSharedFont *sharedFontSetFont(void *context, CanoeSharedFont **font, char *name, int size);

CanoeSharedFont *sharedFontSetColor(void *context, CanoeSharedFont *font, CanoeColor4f color);

#endif /* defined(__CANOE_SHARED_FONT_H__) */
