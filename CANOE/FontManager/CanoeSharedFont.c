/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeSharedFont.h"
#include "CanoeFont.h"
#include "CanoeRenderer.h"

CanoeSharedFont *sharedFontRegisterUsage(void *context, char *restrict name, const int size)
{
    CanoeSharedFont *font = malloc(sizeof(CanoeSharedFont));
    CANOE_MALLOC_CHK(font);
    
    /* clear instance pointer */
    font->instance = NULL;
    
    font->context = context;
    
    /* copy font name */
    strcpy(font->name, name);
    
    /* set font size */
    font->size = size;
    
    /* refered once */
    font->reference = 1;
    
    /* initialize color */
    font->color = canoeColor4f(1.0, 1.0, 1.0, 1.0);
    
    return font;
}

CanoeSharedFont *sharedFontSetFont(void *context, CanoeSharedFont **font, char *name, int size)
{
    sharedFontRelease(context, *font);
    
    *font = sharedFontRegisterUsage(context, name, size);
    
    return *font;
}

CanoeSharedFont *sharedFontSetColor(void *context, CanoeSharedFont *font, CanoeColor4f color)
{
    font->color = color;
    
    return font;
}

void sharedFontRelease(void *context, CanoeSharedFont *font)
{
    assert(font);
    if(!font->instance)
        return;
    
    void *instance = fontMapFind(fontMap, font->context, font->name, font->size);
    ((CanoeSharedFont *)instance)->reference--;
    
    canoeLog("LOG", "font [%p:%s:%d] disposed\n", font->context, font->name, font->size);
    
    if(((CanoeSharedFont *)instance)->reference == 0)
    {
        canoeLog("LOG", "font [%p:%s:%d] is dead\n", font->context, font->name, font->size);
        
        free(fontMapRemove(fontMap, font->context, font->name, font->size));
        
        fontDestroy(font->instance);
    }
    
    free(font);
    font = NULL;
}

CanoeSharedFont *sharedFontInstanceRender(
        CanoeSharedFont *font,
        const float x, const float y,
        char *string,
        void(^fontReloaded)(CanoeSharedFont *font))
{
    if(!font->instance)
    {
        void *instance = fontMapFind(fontMap, font->context, font->name, font->size);
        
        if(!instance)
        {
            // TODO: alloc font
            font->instance = fontLoad(font->name, font->size * 2);

            if(fontReloaded)
                fontReloaded(font);
            
            fontMapInsert(fontMap, font, font->context, font->name, font->size);
            
            canoeLog("LOG", "font [%p:%s:%d] born\n", font->context, font->name, font->size);
        }
        else
        {
            font->instance = ((CanoeSharedFont *)instance)->instance;
            /* retain */
            canoeLog("LOG", "font [%p:%s:%d] shared\n", font->context, font->name, font->size);
            ((CanoeSharedFont *)instance)->reference++;
        }
    }
    
    //printf("render: instance %p, %s\n", font->instance, string);
    
    glPushMatrix();
    {
        glTranslatef(x, y, 0);
        glScalef(0.5, 0.5, 0);
        
        glPushAttrib(GL_CURRENT_BIT);
        {
            glColor4f(font->color.r, font->color.g, font->color.b, font->color.a);
            ftglRenderFont(font->instance, string, FTGL_RENDER_ALL);
        }
        glPopAttrib();
    }
    glPopMatrix();
    
    return font;
}

int sharedFontCalculateBoundingBox(
        CanoeSharedFont *font,
        char *restrict string, CanoeVector2f *lower, CanoeVector2f *upper)
{
    if(!font->instance)
        return 1;
    
    float bounds[6];
    ftglGetFontBBox(font->instance, string,
                    (int)strlen(string), bounds);
    
    lower->x = bounds[0] / 2.0;
    lower->y = bounds[1] / 2.0;
    upper->x = bounds[3] / 2.0;
    upper->y = bounds[4] / 2.0;
    
    return 0;
}
