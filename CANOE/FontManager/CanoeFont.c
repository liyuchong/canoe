/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeFont.h"

FTGLfont *fontLoadDefault(int fontSize)
{
    FTGLfont *font = ftglCreateTextureFont(FONT_DEFAULT_FONT_FILE);
    ftglSetFontFaceSize(font, fontSize, 0);
    
    return font;
}

FTGLfont *fontLoad(char *fontName, const int fontSize)
{
    char fontFile[__LINE_BUFFER_LENGTH];
    strcpy(fontFile, FONT_DEFAULT_FONT_FILE);
    fontLocateFileByName(fontName, fontFile);

    FTGLfont *font =  ftglCreateTextureFont(fontFile);
    ftglSetFontFaceSize(font, fontSize, 0);
    
    return font;
}

void fontDestroy(FTGLfont *font)
{
    if(font) ftglDestroyFont(font);
}

char *fontLocateFileByName(char *font, char *path)
{
    bool fontFound = false;
    FILE *fontListFile = fopen(FONT_LIST_FILE, "r");
    
    if(!fontListFile)
    {
        fprintf(stderr, "Cannot open font list file\n");
        return NULL;
    }
    
    while (!feof(fontListFile))
    {
        char line[__LINE_BUFFER_LENGTH];
        memset(line, 0, __LINE_BUFFER_LENGTH);
        
        fgets(line, __LINE_BUFFER_LENGTH, fontListFile);
        line[strlen(line) - 1] = 0; /* remove the newline */
        
        size_t leftPosition = 0;
        size_t rightPosition = strlen(line) - 1;
        
        if(strlen(line) < 1)
            continue;
        
        /* find last '.' and last '/' */
        for(size_t i = 0; i < strlen(line); i++)
        {
            if(line[i] == '.') rightPosition = i;
            if(line[i] == '/') leftPosition = i;
        }
        
        char fontName[__LINE_BUFFER_LENGTH];
        memset(fontName, 0, __LINE_BUFFER_LENGTH);
        
        strncpy(fontName, line + leftPosition + 1, rightPosition - leftPosition - 1);
        
        if(strcmp(fontName, font) == 0)
        {
            fontFound = true;
            strcpy(path, line);
            break;
        }
    }
    
    if(!fontFound)
    {
        fprintf(stdout, "font '%s' not found, use %s instead.\n", font, FONT_DEFAULT_FONT_FILE);
        strcpy(path, FONT_DEFAULT_FONT_FILE);
    }
    
    fclose(fontListFile);
    
    return path;
}
