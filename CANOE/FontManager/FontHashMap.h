/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef __CANOE__FontHashMap__
#define __CANOE__FontHashMap__

#include <stdio.h>

#include "StringHashMap.h"

typedef StringHashMap FontHashMap;

/**
 *  Insert a font map:
 *      (name, size) -> void *
 *
 *  @param map   The pointer to the map instance
 *  @param value The value stored
 *  @param name  Font name
 *  @param size  Font size
 *
 *  @return The pointer to the input map
 */
FontHashMap *fontMapInsert(FontHashMap *map, void *value, void *context, char *name, int size);

/**
 *  Find a font instance by (name, size)
 *
 *  @param map  The pointer to the map
 *  @param name Font name
 *  @param size Font size
 *
 *  @return The value
 */
void *fontMapFind(FontHashMap *map, void *context, char *name, int size);

/**
 *  Remove a font map record
 *
 *  @param map  The pointer to the map
 *  @param name Font name
 *  @param size Font size
 *
 *  @return The pointer to the value stored
 */
void *fontMapRemove(FontHashMap *map, void *context, char *name, int size);

/**
 *  Release the memory allocated to the font map
 *
 *  @param map The pointer to the font map
 */
void fontMapDestroy(FontHashMap *map);

#endif /* defined(__CANOE__FontHashMap__) */
