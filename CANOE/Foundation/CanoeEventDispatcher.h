/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_EVENT_DISPATCHER_H__
#define CANOE_EVENT_DISPATCHER_H__

/**
 *  Assign the input focus to a new element and dispatch en event
 *
 *  @param current Current element base
 *  @param new     New element base
 *  @param element New element
 */
void canoeAssignInputFocusDispatch(
    CanoeDrawableBase **current, CanoeDrawableBase *new, void *element
);

/**
 *  <#Description#>
 *
 *  @param base       <#base description#>
 *  @param element    <#element description#>
 *  @param mousePoint <#mousePoint description#>
 *  @param button     <#button description#>
 *  @param action     <#action description#>
 *  @param mods       <#mods description#>
 */
void canoeDispatchMouseButtonEvent(
    CanoeDrawableBase *base, void *element,
    CanoeVector2f mousePoint, int button, int action, int mods
);

/**
 *  <#Description#>
 *
 *  @param key      <#key description#>
 *  @param scanCode <#scanCode description#>
 *  @param action   <#action description#>
 *  @param modifier <#modifier description#>
 */
void canoeDispatchKeyPressedEvenet(
    int key, int scanCode, int action, int modifier
);

void canoeDispatchCharPressedEvent(int key);

/**
 *  <#Description#>
 *
 *  @param window <#window description#>
 *  @param pos    <#pos description#>
 *  @param button <#button description#>
 *  @param action <#action description#>
 *  @param mods   <#mods description#>
 */
void canoeDispatchMouseEvent(
    CanoeWindow *window, CanoeVector2f pos, int button, int action, int mods
);

/**
 *  <#Description#>
 *
 *  @param window <#window description#>
 *  @param width  <#width description#>
 *  @param height <#height description#>
 */
void canoeDispatchWindowSizeChangedEvent(
    CanoeWindow *window, int width, int height
);

void canoeDispatchMouseScrollEvent(double x, double y);

void canoeDispatchCursorPositionEvent(double x, double y);

#endif /* CANOE_EVENT_DISPATCHER_H__ */
