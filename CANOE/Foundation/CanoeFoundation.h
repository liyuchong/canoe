/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_H__
#define CANOE_H__

#include <vector.h>

#include "CanoeVersion.h"
#include "CanoeEvents.h"
#include "EventManager.h"
#include "FontHashMap.h"

#define CANOE_LOG_DESTROY
#define CANOE_LOG_COLLISION_QUERY
#define CANOE_LOG_FOCUS_CHANGE

#define EXPLODE_COMPONENT2D(__vector) (__vector).x, (__vector).y

#define SCREEN_RESOLUTION canoeVector2f(1280, 800)

#define CANOE_BASE_NAME_LENGTH 16

#define MUTE(a) (a = a)

#define CANOE_NEW(element_name) canoe##element_name##New

#define CANOE_MALLOC_CHK(ptr__)

#ifndef __INLINE__
#   define __INLINE__ inline
#endif /* __INLINE__ */

SV_DECLARE(CanoeDrawableElements, void *);

typedef struct
{
    float x, y;
} CanoeVector2f;

typedef struct
{
    float x, y, z;
} CanoeVector3f;

typedef struct
{
    float r, g, b;
} CanoeColor3f;

typedef struct
{
    float r, g, b, a;
} CanoeColor4f;

/**
 *  Create a CanoeVector2f object
 *
 *  @param x X
 *  @param y Y
 *
 *  @return Object
 */
static __INLINE__
CanoeVector2f canoeVector2f(const float x, const float y)
{
    CanoeVector2f vector = { x, y };
    return vector;
}

/**
 *  Calculate the sum of 2 vectors
 *
 *  @param a A
 *  @param b B
 *
 *  @return Sum
 */
static __INLINE__
CanoeVector2f canoeVector2fAdd(const CanoeVector2f a, const CanoeVector2f b)
{
    CanoeVector2f result = {
        .x = a.x + b.x,
        .y = a.y + b.y
    };
    
    return result;
}

/**
 *  Calculate the difference of 2 vectors
 *
 *  @param a A
 *  @param b B
 *
 *  @return Difference
 */
static __INLINE__
CanoeVector2f canoeVector2fSubtract(
    const CanoeVector2f a, const CanoeVector2f b
) {
    CanoeVector2f result = {
        .x = a.x - b.x,
        .y = a.y - b.y
    };
    
    return result;
}

/**
 *  Create a CanoeVector3f object
 *
 *  @param x X
 *  @param y Y
 *  @param z Z
 *
 *  @return The object
 */
static __INLINE__
CanoeVector3f canoeVector3f(const float x, const float y, const float z)
{
    CanoeVector3f vector = { x, y, z };
    return vector;
}

/**
 *  Create a CanoeColor4f object
 *
 *  @param r R
 *  @param g G
 *  @param b B
 *  @param a A
 *
 *  @return The object
 */
static __INLINE__
CanoeColor4f  canoeColor4f(float r, float g, float b, float a)
{
    CanoeColor4f color = { .r = r, .g = g, .b = b, .a = a };
    return color;
}

/**
 *  Create 2 CanoeVector3f object
 *
 *  @param r R
 *  @param g G
 *  @param b B
 *
 *  @return The object
 */
static __INLINE__
CanoeColor3f canoeColor3f(float r, float g, float b)
{
    CanoeColor3f color = { .r = r, .g = g, .b = b };
    return color;
}

/**
 *  Calculate a color representation of an memory address
 *
 *  @param hash The memory address
 *
 *  @return The color
 */
CanoeColor4f  colorFromHash(void *hash);

/**
 *  Initialize the shared font map
 *
 *  @return A pointer to the shared font map
 */
FontHashMap *sharedFontInit(void);

/**
 *  Print a log to stdout
 *
 *  @param title Log title
 *  @param fmt   Format
 *  @param ...   Args
 */
#define canoeLog(title, fmt, ...) printf("["title"]" fmt, ## __VA_ARGS__)

#endif /* defined(CANOE_H__) */
