/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_EVENTS_H__
#define CANOE_EVENTS_H__

#define CANOE_EVENT_ELEMENT void *__data

/* -------------------------------------------------------------------------- */
/** Event: CANOE_EVENT_MOUSE_BUTTON_ACTION
 *         -------------------------------
 *  Triggers if the registered elements has focus and the mouse action happened
 *  inside its boundary. */
#define CANOE_EVENT_MOUSE_BUTTON_ACTION  0x9000

#define CANOE_MOUSE_PRESS                0x01
#define CANOE_MOUSE_RELEASE              0x00
#define CANOE_MOUSE_BUTTON_RIGHT         0x01
#define CANOE_MOUSE_BUTTON_LEFT          0x00

/**
 *  Extact the arguments of a mouse button action event callback
 *
 *  @param __element_type Type of the element
 *  @param __element_name Type CyanLabel, the identifier of the element
 *  @param __position     Type CyanVactor2f, the mouse cursor position
 *  @param __button       Type int, indicates which button was triggered
 *  @param __action       Type int, indicates the action of the key
 *  @param __mods         Type int, indicates which modifier key was held
 */
#define CANOE_EVENT_MOUSE_BUTTON_EXTRACT_ARGUMENTS(                           \
    __element_type, __element_name, __position, __button, __action, __mods)   \
    __element_type __element_name = __data;                                   \
    va_list __args;                                                           \
    va_start(__args, __data);                                                 \
    CanoeVector2f __position = va_arg(__args, CanoeVector2f);                 \
    int __button             = va_arg(__args, int);                           \
    int __action             = va_arg(__args, int);                           \
    int __mods               = va_arg(__args, int);                           \
    va_end(__args);                                                           \
    MUTE(__element_name); MUTE(__position); MUTE(__button);                   \
    MUTE(__action); MUTE(__mods);

/* -------------------------------------------------------------------------- */
/** Event: CANOE_EVENT_MOUSE_SCROLL
 *         -------------------------------
 *  Triggers if the registered elements has focus and the mouse scroll
 *  action happened inside its boundary. */
#define CANOE_EVENT_MOUSE_SCROLL 0x9002

/**
 *  Extact the arguments of a mouse scroll action event callback
 *
 *  @param __element_type Type of the element
 *  @param __element_name Name of the element
 *  @param __x            X
 *  @param __y            Y
 *
 */
#define CANOE_EVENT_MOUSE_SCROLL_EXTRACT_ARGUMENTS(                           \
    __element_type, __element_name, __x, __y)                                 \
    __element_type __element_name = __data;                                   \
    va_list __args;                                                           \
    va_start(__args, __data);                                                 \
    double __x = va_arg(__args, double);                                      \
    double __y = va_arg(__args, double);                                      \
    va_end(__args);                                                           \
    MUTE(__element_name); MUTE(__x); MUTE(__y);

/* -------------------------------------------------------------------------- */
/** Event: CANOE_EVENT_CURSOR_POSITION
 *         -------------------------------
 *  Triggers if the registered elements has focus and the mouse cursor position
 *  changed. */
#define CANOE_EVENT_CURSOR_POSITION 0x9003

/**
 *  Extact the arguments of a cursor position event callback
 *
 *  @param __element_type Type of the element
 *  @param __element_name Name of the element
 *  @param __x            X
 *  @param __y            Y
 *
 */
#define CANOE_EVENT_CURSOR_POSITION_EXTRACT_ARGUMENTS(                        \
    __element_type, __element_name, __x, __y)                                 \
    __element_type __element_name = __data;                                   \
    va_list __args;                                                           \
    va_start(__args, __data);                                                 \
    double __x = va_arg(__args, double);                                      \
    double __y = va_arg(__args, double);                                      \
    va_end(__args);                                                           \
    MUTE(__element_name); MUTE(__x); MUTE(__y);

/* -------------------------------------------------------------------------- */
/** Event: CANOE_EVENT_KEY_ACTION
 *         ----------------------
 *  Triggers if the registered elements has focus and the 
 *  keyboard action happened */
#define CANOE_EVENT_KEY_ACTION 0x9001

#define CANOE_KEY_PRESSED      0x01
#define CANOE_KEY_RELEASE      0x00


#define CANOE_EVENT_CHAR_ACTION 0x9011

/**
 *  Extract the arguments of a focus changed event callback
 *
 *  @param __element_type Type of the element
 *  @param __element_name Type CyanLabel, the identifier of the element
 *  @param __focus_status Type int, the focus status
 */
#define CANOE_EVENT_CHAR_PRESSED_EXTRACT_ARGUMENTS(                         \
    __element_type, __element_name, __key)                                  \
    __element_type *__element_name = __data;                                \
    va_list __args;                                                         \
    va_start(__args, __data);                                               \
    int __key = va_arg(__args, int);                                        \
    /* mute warnings if unused */                                           \
    va_end(__args);                                                         \
    MUTE(__element_name); MUTE(__key);

/**
 *  Extract the arguments of a key action event callback
 *
 *  @param __element_type Type of the element
 *  @param __element_name Type __element_type, the identifier of the element
 *  @param __key          Type int, the key
 *  @param __scan_code    Type int, scan code of the key
 *  @param __action       Type int, action
 *  @param __modifier     Type int, indicates which modifier key was held
 */
#define CANOE_EVENT_KEY_ACTION_EXTRACT_ARGUMENTS(                             \
    __element_type, __element_name, __key, __scan_code, __action, __modifier) \
    __element_type  __element_name = __data;                                  \
    va_list __args;                                                           \
    va_start(__args, __data);                                                 \
    int __key       = va_arg(__args, int);                                    \
    int __scan_code = va_arg(__args, int);                                    \
    int __action    = va_arg(__args, int);                                    \
    int __modifier  = va_arg(__args, int);                                    \
    va_end(__args);                                                           \
    /* mute warnings if unused */                                             \
    MUTE(__element_name); MUTE(__key); MUTE(__scan_code);                     \
    MUTE(__action); MUTE(__modifier);

/* -------------------------------------------------------------------------- */
/** Event: CANOE_EVENT_INPUT_FOCUS_CHANGED
 *         -------------------------------
 *  Triggers if the registered elements have a change of focus */
#define CANOE_EVENT_INPUT_FOCUS_CHANGED  0x9100

#define CANOE_INPUT_FOCUS_GOT            0x01
#define CANOE_INPUT_FOCUS_LOST           0x00

/**
 *  Extract the arguments of a focus changed event callback
 *
 *  @param __element_type Type of the element
 *  @param __element_name Type CyanLabel, the identifier of the element
 *  @param __focus_status Type int, the focus status
 */
#define CANOE_EVENT_FOCUS_CHANGED_EXTRACT_ARGUMENTS(                        \
    __element_type, __element_name, __focus_status)                         \
    __element_type *__element_name = __data;                                \
    va_list __args;                                                         \
    va_start(__args, __data);                                               \
    int __focus_status = va_arg(__args, int);                               \
    /* mute warnings if unused */                                           \
    va_end(__args);                                                         \
    MUTE(__element_name); MUTE(__focus_status);

/* -------------------------------------------------------------------------- */
/** Event: CANOE_EVENT_WINDOW_SIZE_CHANGED
 *         -------------------------------
 *  Triggers if the registered elements have a change of size */
#define CANOE_EVENT_WINDOW_SIZE_CHANGED  0x9200

/**
 *  Extract window size arguments when window size changed event is triggered
 *
 *  @param __element_name The name of the window variable
 *  @param __width        The width of the window
 *  @param __height       The height of the window
 */
#define CANOE_EVENT_WINDOW_SIZE_CHANGED_EXTRACT_ARGUMENTS(__element_name,   \
    __width, __height)                                                      \
    CanoeLabel *__element_name = __data;                                    \
    va_list __args;                                                         \
    va_start(__args, __data);                                               \
    int __width = va_arg(__args, int);                                      \
    int __height = va_arg(__args, int);                                     \
    va_end(__args);                                                         \
    MUTE(__element_name); MUTE(__width); MUTE(__height);

/**
 *  Init the global event manager
 *
 *  @return 0 on success, 1 on failure
 */
int canoeEventManagerInit(void);

/**
 *  Register a handler to an event of a specific element
 *
 *  @param element The element to be listened
 *  @param event   The event to be listened
 *  @param handler Handling function
 *
 *  @return 0 on success
 */
int canoeRegisterEvent(void *element, long event, void *handler);

#endif /* defined(CANOE_EVENTS_H__) */
