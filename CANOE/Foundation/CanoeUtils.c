/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeFoundation.h"

CanoeColor4f colorFromHash(void *hash)
{
    float R, G, B;
    
    unsigned long val = (unsigned long)hash;
    
    val = (val + 0x7ed55d16) + (val << 12);
    val = (val ^ 0xc761c23c) ^ (val >> 19);
    val = (val + 0x165667b1) + (val << 5);
    val = (val + 0xd3a2646c) ^ (val << 9);
    val = (val + 0xfd7046c5) + (val << 3);
    val = (val ^ 0xb55a4f09) ^ (val >> 16);
    
    float r = (float)((val >> 0) & 0xFF);
    float g = (float)((val >> 8) & 0xFF);
    float b = (float)((val >> 16) & 0xFF);
    
    if(r < 100) r += 100;
    if(g < 100) g += 100;
    if(b < 100) g += 100;
    
    R = r / 255.0;
    G = g / 255.0;
    B = b / 255.0;
    
    return canoeColor4f(R, G, B, 0.8f);
}
