/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "CanoeFoundation.h"
#include "CanoeWindow.h"
#include "EventManager.h"
#include "FontHashMap.h"

/* Root event manager,
 * all the events in this GUI was managed by this event manager. */
EventManager *rootEventManager = NULL;

/* Root window container,
 * all the pointer to the windows in this GUI is stored here. */
CanoeWindowContainer rootWindowContainer;

/* The object which has current input focus */
CanoeDrawableBase *currentInputFocusedElement = NULL;

FontHashMap *fontMap = NULL;

int canoeEventManagerInit(void)
{
    rootEventManager = eventManagerCreate();
    
    return rootEventManager ? 0 : 1;
}

int canoeRegisterEvent(void *element, long event, void *handler)
{
    evmRegisterEvent(rootEventManager, element, event, handler);
    
    return 0;
}

FontHashMap *sharedFontInit(void)
{
    return (fontMap = stringHashMapCreate(17));
}
