/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include <stdio.h>

#include "EventManager.h"
#include "CanoeExterns.h"
#include "CanoeEventDispatcher.h"

#define CANOE_EVENT_DISPATCH__(handler_var__, ...)                  \
do {                                                                \
    ((EventHandlerFunc)(handler_var__)->handler)(__VA_ARGS__);      \
                                                                    \
    while (handler->next)                                           \
    {                                                               \
        handler_var__ = (handler_var__)->next;                      \
        ((EventHandlerFunc)(handler_var__)->handler)(__VA_ARGS__);  \
    }                                                               \
} while(0)

void canoeAssignInputFocusDispatch(
        CanoeDrawableBase **current, CanoeDrawableBase *new, void *element
) {
    if(*current != new)
    {
        EventHandler *handler;
        
        /* get focus */
        if((handler = evmFindEvent(rootEventManager, new,
                                   CANOE_EVENT_INPUT_FOCUS_CHANGED)))
        {
            CANOE_EVENT_DISPATCH__(handler, new, CANOE_INPUT_FOCUS_GOT);
        }
        
        /* lost focus */
        if((handler = evmFindEvent(rootEventManager,
                                   currentInputFocusedElement,
                                   CANOE_EVENT_INPUT_FOCUS_CHANGED)))
        {
            CANOE_EVENT_DISPATCH__(handler, *current, CANOE_INPUT_FOCUS_LOST);
        }
        
#ifdef CANOE_LOG_FOCUS_CHANGE
        canoeLog("LOG", "element %p[%s] lost focus, %p[%s] get focus\n",
                 *current, (*current) ? (*current)->elementName : "NULL",
                 new, new ? new->elementName : "NULL");
#endif /* CANOE_LOG_FOCUS_CHANGE */
        
        *current = new;
    }
}

void canoeDispatchMouseButtonEvent(
        CanoeDrawableBase *base, void *element,
        CanoeVector2f mousePoint, int button, int action, int mods
) {
    EventHandler *handler;
    
    if((handler = evmFindEvent(rootEventManager, base,
                               CANOE_EVENT_MOUSE_BUTTON_ACTION))
    ) {
        CANOE_EVENT_DISPATCH__(
            handler, element, mousePoint, button, action, mods
        );
    }
}

void canoeDispatchCharPressedEvent(int key)
{
    //CANOE_EVENT_CHAR_ACTION
    
    EventHandler *handler;
    
    /* If we have an element with input focused, dispatch the key event. */
    if(currentInputFocusedElement)
    {
        if((handler = evmFindEvent(rootEventManager,
                                   currentInputFocusedElement,
                                   CANOE_EVENT_CHAR_ACTION))
           ) {
            CANOE_EVENT_DISPATCH__(
                                   handler,
                                   currentInputFocusedElement,
                                   key
                                   );
        }
    }
}

void canoeDispatchKeyPressedEvenet(
        int key, int scanCode, int action, int modifier
) {
    EventHandler *handler;
    
    /* If we have an element with input focused, dispatch the key event. */
    if(currentInputFocusedElement)
    {
        if((handler = evmFindEvent(rootEventManager,
                                   currentInputFocusedElement,
                                   CANOE_EVENT_KEY_ACTION))
        ) {
            CANOE_EVENT_DISPATCH__(
                handler,
                currentInputFocusedElement,
                key, scanCode, action, modifier
            );
        }
    }
}

typedef struct
{
    CanoeDrawableBase *obj;
    int depth;
} SpatialQueryResult__;

static
VECTOR(SpatialQueryResult__) *queryCollision__(
        VECTOR(SpatialQueryResult__) *results,
        CanoeDrawableBase *const base, CanoeVector2f const mouse, int depth
) {
    depth++;
    
    /* find transform from root */
    CanoeVector2f world = canoeCalculateWorldCoordinate(base);
    
    /* apply transform */
    CanoeVector2f queryBound = canoeVector2fSubtract(mouse, world);

#ifdef CANOE_LOG_COLLISION_QUERY
    canoeLog("LOG", "collision query: depth: %d transform: %.1f %.1f\n",
             depth, world.x, world.y);
#endif
    
    VECTOR(SpatialNode *) nodes;
    SV_INIT(&nodes, 2, SpatialNode *);
    
    /* construct search boundary */
    SpatialBoundary searchBoundary =
        spatialBoundaryNew(2,
             EXPLODE_COMPONENT2D(queryBound),
             EXPLODE_COMPONENT2D(queryBound)
        );
    
    spatialIndexerSearch((SpatialNodes *)&nodes, &base->indexer, &searchBoundary);
    
    /* add the current element to the results */
    SpatialQueryResult__ current = { .obj = base, .depth = depth };
    SV_PUSH_BACK(results, current);
    
    /* run collision query for every child node */
    for(size_t i = 0; i < SV_SIZE(&nodes); i++)
    {
        SpatialQueryResult__ result = {
            .obj   = SV_AT(&nodes, i)->data,
            .depth = depth
        };
        
        queryCollision__(results, result.obj, queryBound, depth);
    }
    
    /* clean up */
    free(nodes.data);
    
    return (void *)results;
}

void canoeDispatchMouseEvent(
    CanoeWindow *window,
    CanoeVector2f position, int button, int action, int modifiers
) {
    if(action == 0)
    {
        canoeDispatchMouseButtonEvent(
            currentInputFocusedElement, currentInputFocusedElement,
            position, button, action, modifiers
        );

        return;
    }
    
    /* allocate memory for collision query */
    VECTOR(SpatialQueryResult__) results;
    SV_INIT(&results, 2, SpatialQueryResult__);
    
    /* run the collision query */
    queryCollision__((void *)&results, &window->base, position, 0);
    
    /* find deepest element */
    int deepest = 0;
    
    for(size_t i = 0; i < SV_SIZE(&results); i++)
    {
        deepest = SV_AT(&results, i).depth > deepest ?
        SV_AT(&results, i).depth : deepest;
    }
    
#ifdef CANOE_LOG_COLLISION_QUERY
    canoeLog("LOG", "collision query: deepest: %d\n", deepest);
#endif
    
    // TODO: sort Z index?
    for(size_t i = 0; i < SV_SIZE(&results); i++)
    {
        SpatialQueryResult__ result = SV_AT(&results, i);
        
        if(result.depth == deepest)
        {
            canoeDispatchMouseButtonEvent(
                result.obj, result.obj,
                position, button, action, modifiers
            );
                
            /* assign the input focus to the element */
            canoeAssignInputFocusDispatch(
                &currentInputFocusedElement, result.obj,
                (SV_AT(window->base.children, i))
            );
            
            free(results.data);
            
            return;
        }
    }
    
    free(results.data);
}

void canoeDispatchWindowSizeChangedEvent(CanoeWindow *window, int width, int height)
{
    /* find handler */
    EventHandler *handler = evmFindEvent(
        rootEventManager, window, CANOE_EVENT_WINDOW_SIZE_CHANGED);
    
    if(handler)
    {
        ((EventHandlerFunc)handler->handler)(window, width, height);
    }
}

void canoeDispatchMouseScrollEvent(double x, double y)
{
    EventHandler *handler;
    
    if((handler = evmFindEvent(rootEventManager,
                               currentInputFocusedElement,
                               CANOE_EVENT_MOUSE_SCROLL))
       ) {
            CANOE_EVENT_DISPATCH__(
                handler, currentInputFocusedElement, x, y
        );
    }
}

void canoeDispatchCursorPositionEvent(double x, double y)
{
    EventHandler *handler;
    
    if((handler = evmFindEvent(rootEventManager,
                               currentInputFocusedElement,
                               CANOE_EVENT_CURSOR_POSITION))
       ) {
            CANOE_EVENT_DISPATCH__(
                handler, currentInputFocusedElement, x, y
        );
    }
}
