/*
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SimpleString.h"

SimpleString *stringInit(SimpleString *string)
{
    SV_INIT(&string->string, 64, char);
    
    return string;
}

void stringRelease(SimpleString *string)
{
    free(string->string.data);
}

SimpleString *stringPurge(SimpleString *string)
{
    memset(string->string.data, 0, string->string.__allocatedLength);
    string->string.size = 0;
    
    return string;
}

SimpleString *stringCat(SimpleString *obj, char *str)
{
    char *p = str;
    
    while(*p)
    {
        SV_PUSH_BACK(&obj->string, *p);
        p++;
    }
    
    return obj;
}

SimpleString *stringAssign(SimpleString *obj, char *str)
{
    return stringCat(stringPurge(obj), str);
}

int stringCompare(SimpleString *str1, SimpleString *str2)
{
    return strcmp(str1->string.data, str2->string.data);
}