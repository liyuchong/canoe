/*
 * Copyright 2012 - 2015 李雨翀(Yuchong Li) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the author nor the names of its contributors may be
 *    used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>

#include "GeographicAlgorithms.h"

/** convert WGS84 coordinate to ECEF(Earth-centered Earth-fixed) coordinate */
INSVector3f convertWGS84toECEF(const INSCoordinateWGS84 coordinate)
{
    const double phi    = DEG2RAD(coordinate.latitude);
    const double lambda = DEG2RAD(coordinate.longitude);
    const double height = coordinate.altitude;
    const double normalPhi =
    WGS84_A / sqrt(1.0 - WGS84_ESQ * pow(sin(phi), 2.0));
    
    const INSVector3f result = {
        .x = (normalPhi + height) * cos(phi) * cos(lambda),
        .y = (normalPhi + height) * cos(phi) * sin(lambda),
        .z = (normalPhi * WGS84_1MESQ + height) * sin(phi)
    };
    
    return result;
}

/** convert ECEF coordinate to ENU(East, North, Up) coordinate
 *  the reference point is in WGS84 coordinate format */
INSVector3f convertECEFtoENU(const INSCoordinateWGS84 reference,
                             const INSVector3f coordinate)
{
    const double sinPhi    = sin(DEG2RAD(reference.latitude));
    const double cosPhi    = cos(DEG2RAD(reference.latitude));
    const double sinLambda = sin(DEG2RAD(reference.longitude));
    const double cosLambda = cos(DEG2RAD(reference.longitude));
    
    const INSVector3f ecef = convertWGS84toECEF(reference);
    const INSVector3f translated = {
        ecef.x - coordinate.x,
        ecef.y - coordinate.y,
        ecef.z - coordinate.z
    };
    
    const INSVector3f ENU = {
        .x = -sinLambda * translated.x + cosLambda * translated.y,
        
        .y = -sinPhi * cosLambda * translated.x -
        sinPhi * sinLambda * translated.y +
        cosPhi * translated.z,
        
        .z =  cosPhi * cosLambda * translated.x +
        cosPhi * sinLambda * translated.y +
        sinPhi * translated.z
    };
    
    return ENU;
}

INSCoordinateWGS84 convertECEFtoGeodetic(const INSVector3f ecef,
                                         const double a, const double e2)
{
    int count = 0;
    
    const double ep2 = e2 / (1.0 - e2);
    const double f   = 1.0 - sqrt(1.0 - e2);
    const double b   = a * (1 - f);
    
    const double lambda = atan2(ecef.y, ecef.x);
    const double rho    = hypot(ecef.x, ecef.y);
    
    double beta = atan2(ecef.z, (1.0 - f) * rho);
    double phi  = atan2(ecef.z + b * ep2 * pow(sin(beta), 3.0),
                        rho - a * e2  * pow(cos(beta), 3));
    
    double betaNew = atan2((1.0 - f) * sin(phi), cos(phi));
    
    while(count++ < 5 && fabs(betaNew - betaNew) <= DBL_EPSILON * 2.0)
    {
        beta = betaNew;
        phi = atan2(ecef.z + b * ep2 * pow(sin(beta), 3.0),
                    rho - a * e2  * pow(cos(beta), 3.0));
        betaNew = atan2((1.0 - f) * sin(phi), cos(phi));
    }
    
    const double sinPhi = sin(phi);
    const double N = a / sqrt(1 - e2 * pow(sinPhi, 2));
    const double h = rho * cos(phi) + (ecef.z + e2 * N * sinPhi) * sinPhi - N;
    
    const INSCoordinateWGS84 geodetic = {
        .longitude = RAD2DEG(lambda),
        .latitude  = RAD2DEG(phi),
        .altitude  = h
    };
    
    return geodetic;
}
