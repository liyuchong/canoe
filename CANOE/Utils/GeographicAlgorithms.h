/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef GEOGRAPHIC_ALGORITHMS_H__
#define GEOGRAPHIC_ALGORITHMS_H__

#include <math.h>
#include <float.h>

/**
 *  WGS84 coordinate,
 *  longitude & latitude are in degrees, altitude is in meters.
 */
typedef struct
{
    double longitude, latitude, altitude;
} INSCoordinateWGS84;

typedef struct { double x, y, z; } INSVector3f;

/* constants */
#define EARTH_ROTATION_RATE_WGS84_RAD   7.292115e-5
#define WGS84_A                         6378137.0
#define WGS84_1MESQ                     0.9933056199957394
#define WGS84_ESQ                       0.006694380004260591

#define DEG2RAD(x) ((x) * (3.1415926 / 180.0))
#define RAD2DEG(x) ((x) * (180.0 / 3.1415926))

/**
 *  Convert WGS84 coordinate to ECEF coordinate
 *
 *  @param coordinate WGS84
 *
 *  @return ECEF
 */
INSVector3f convertWGS84toECEF(const INSCoordinateWGS84 coordinate);

/**
 *  convert ECEF coordinate to ENU(East, North, Up) coordinate
 *  the reference point is in WGS84 coordinate format
 *
 *  @param reference  Reference point in WGS84 coordinate
 *  @param coordinate ECEF coordinate
 *
 *  @return ENU coordinate
 */
INSVector3f convertECEFtoENU(const INSCoordinateWGS84 reference,
                             const INSVector3f coordinate);

/**
 *  Convert ECEF coordinate to geodetic coordinate
 *
 *  @param ecef ECEF
 *  @param a    ...
 *  @param e2   ...
 *
 *  @return Geodetic coordinate
 */
INSCoordinateWGS84 convertECEFtoGeodetic(const INSVector3f ecef,
                                         const double a, const double e2);

#endif /* defined(GEOGRAPHIC_ALGORITHMS_H__) */
