/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

/*
 *  SimpleMap.h
 *
 *  Created by Li-Yuchong <liyuchong@cyanclone.com> on 13-9-4.
 *  Copyright (c) 2013 Cyanclone Co., Ltd.. All rights reserved.
 */

#ifndef __SIMPLE_MAP_H__
#define __SIMPLE_MAP_H__

#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>

#ifdef	__cplusplus
extern "C" {
#endif

/* AVL-Tree implementation from OpenSolaris kernel. */
#include "avl.h"

typedef struct MapEntry
{
    avl_node_t link;
    
    void *key;
    void *value;
} MapEntry;

typedef avl_tree_t SimpleMap;

/**
 *  Get the first element in the map
 *
 *  @param __map The map
 *
 *  @return The first element
 */
#define simpleMapFirst(__map) avl_first(__map)
    
/**
 *  Get the next entry in the map
 *
 *  @param __map  The map
 *  @param __node The current node
 *
 *  @return The next node
 */
#define simpleMapNext(__map, __node) AVL_NEXT(__map, __node)

/**
 *  Create a simple map object and allocate memory
 *
 *  @return The pointer to the new object
 */
SimpleMap *simpleMapCreate(void);

/**
 *  Initialize a simple map object
 *
 *  @param map The pointer to the map
 *
 *  @return The pointer to the input map
 */
 SimpleMap *simpleMapInit(SimpleMap *map);

/**
 *  Release the memory of a simple map object
 *
 *  @param map The pointer to the map
 */
void simpleMapDestroy(SimpleMap *map);

/**
 *  Insert an entry to the simple map
 *
 *  @param map   The pointer to the simple map
 *  @param key   The key of the entry
 *  @param value The value of the entry
 *
 *  @return True on success, false otherwise
 */
bool simpleMapInsert(SimpleMap *map, void *key, void *value);

/**
 *  insert a double mapped entry to the simple map object
 *
 *  @param map The pointer to the simple map
 *  @param kv1 The first key(value)
 *  @param kv2 The second key(value)
 *
 *  @return True on success, false otherwise
 */
/** insert a double mapped entry to the simple map object */
bool simpleMapDoubleInsert(SimpleMap *map, void *kv1, void *kv2);

/**
 *  Search for the entry by a given key in the simple map object
 *
 *  @param map The pointer to the map
 *  @param key The key
 *
 *  @return The data stored in the entry
 */
void *simpleMapFind(const SimpleMap *map, void *key);

#ifdef	__cplusplus
}
#endif
    
#endif /* __SIMPLE_MAP_H__ */
