#include <stdlib.h>
#include <stdio.h>

#include "CanoeExterns.h"

#include "CanoeDrawableBase.h"
#include "CanoeLabel.h"

#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>

#include "CanoeRenderer.h"
#include "CanoeWindow.h"
#include <stdbool.h>
#include <pthread/pthread.h>

#include <dispatch/dispatch.h>
#include "CanoeFoundation.h"

#include "SpatialIndexer.h"
#include "CanoeSharedFont.h"
#include "EventManager.h"
#include "CanoeButton.h"
#include "CanoeMessageBox.h"

#include "SimpleString.h"
#include <math.h>

#include "CanoeADI.h"
#include "CanoeLayer.h"
#include "Canoe3DView.h"
#include "CanoeFlightMap.h"

CanoeLabel *label;
CanoeLabel *label2;
CanoeLabel *label3;

CanoeButton *button;

CanoeColor4f anyColourYouLike(void)
{
    float r = rand() / (float)RAND_MAX;
    float g = rand() / (float)RAND_MAX;
    float b = rand() / (float)RAND_MAX;
    
    CanoeColor4f color = { .r = r, .g = g, .b = b, .a = 1.0 };
    
    return color;
}

CanoeWaveDisplay *display;
CanoeWaveDisplay *gDisplay;

void windowSizeChanged(CANOE_EVENT_ELEMENT, ...)
{
    //if(!display)
    //    return;
    
    CANOE_EVENT_WINDOW_SIZE_CHANGED_EXTRACT_ARGUMENTS(window, width, height);
    window=window;

    canoeWaveDisplaySetSize(display, canoeVector2f(width, height));
//    canoeWaveDisplaySetSize(gDisplay, canoeVector2f(width, height / 2.0));
    
    
    //canoeWaveDisplaySetLocation(gDisplay, canoeVector2f(0, height));
}

void keya(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_KEY_ACTION_EXTRACT_ARGUMENTS(CanoeWindow *, window, key, scode, act, mod);
    
    printf("%s\n", window->title);
}


//CanoeADI *adi;

void test1(void *dats, ...)
{
    printf("handler1\n");
}

void test2(CANOE_EVENT_ELEMENT, ...)
{
    printf("handler2\n");
    canoeLabelSetTitle(label2, "fuck");
    
    CANOE_EVENT_MOUSE_BUTTON_EXTRACT_ARGUMENTS(CanoeButton *, but, pos, butt, act, mods);
    if(butt == CANOE_MOUSE_BUTTON_LEFT && act == CANOE_MOUSE_RELEASE)
        canoeMessageBoxNew("fuck", "dont click this fucking button");
}

void mouseDownHandler(CANOE_EVENT_ELEMENT, ...)
{
    CANOE_EVENT_MOUSE_BUTTON_EXTRACT_ARGUMENTS(CanoeLabel *, label, point, button, action, mods);
    
    if(action == CANOE_MOUSE_PRESS && button == CANOE_MOUSE_BUTTON_LEFT)
    {
        canoeLabelSetFont(label, "Optima", rand() % 40 + 10);
        canoeLabelSetFontColor(label, anyColourYouLike());
        
        canoeMessageBoxNew("Some sort of title", "Lynnfield College students exposed to perspectives different");
    }
    
    //printf("<label: %s> %f %f, action %d, button %d, %d\n", label->title, point.x, point.y, action, button, mods);
}

void setupUI(void)
{
    SV_INIT(&rootWindowContainer, 2, CanoeWindow *);
    
    CanoeWindow *w = (canoeWindowNewDefault("中文"));
    
    
    
    //canoeRegisterEvent(w, CANOE_EVENT_WINDOW_SIZE_CHANGED, windowSizeChanged);
    
    
    //gDisplay = canoeWaveDisplayNew(canoeVector2f(0, 0), canoeVector2f(500, 80));
    
    button = canoeButtonNew(canoeVector2f(10, 10), "Feels bad");
    CanoeButton *button2 = canoeButtonNew(canoeVector2f(10, 40), "Stop it bro");
    
    //canoeButtonSetFont(button, "Optima", 12);
    
    label = canoeLabelNew(canoeVector2f(10, 200), "LYNNFIELD, MA—In an effort to provide sanctuary for\n Lynnfield College students exposed to perspectives different from their own, a new campus safe space was dedicated Wednesday in honor of Alexis Stigmore, a 2009 graduate who felt kind of weird in class one time.");
    label2 = canoeLabelNew(canoeVector2f(100, 100), "asdasdssad");
    
    
    //canoeWindowAddChild(w, gDisplay);
    canoeWindowAddChild(w, button);
    canoeWindowAddChild(w, button2);
    canoeWindowAddChild(w, label);

    //adi = canoeAdiNew(canoeVector2f(10, 10), canoeVector2f(300, 300));
    
    //canoeWindowAddChild(w2, adi);
    //canoeWindowAddChild(w2, label2);
    
    SV_PUSH_BACK(&rootWindowContainer, w);
    //SV_PUSH_BACK(&rootWindowContainer, w2);
    
    canoeWindowSetSize(w, canoeVector2f(500, 500));
    
    //canoeRegisterEvent(button, CANOE_EVENT_MOUSE_BUTTON_ACTION, test2);
    canoeRegisterEvent(label, CANOE_EVENT_MOUSE_BUTTON_ACTION, mouseDownHandler);
    
    canoeRegisterEvent(w, CANOE_EVENT_KEY_ACTION, keya);
    
    display = canoeWaveDisplayNew(canoeVector2f(0, 0), canoeVector2f(500, 80));
    CanoeWindow *signalsWindow = (canoeWindowNewDefault("Signals view"));
    canoeRegisterEvent(signalsWindow, CANOE_EVENT_WINDOW_SIZE_CHANGED, windowSizeChanged);
    
    canoeWindowAddChild(signalsWindow, display);
    
    SV_PUSH_BACK(&rootWindowContainer, signalsWindow);
    
    windowSizeChanged(NULL, (int)signalsWindow->base.size.x, (int)signalsWindow->base.size.y);
}

typedef struct
{
    size_t length;
    size_t validUpdate;
    
    CB_DECLARE(float) buffer;
    float *linear;
    
    CanoeWaveDisplay *display;
} CanoeRealtimeWave;


CanoeRealtimeWave *canoeRealtimeAppend(CanoeRealtimeWave *wave, float data)
{
    CB_APPEND(&wave->buffer, data);
    CB_FILL_SEQUENCE(&wave->buffer, wave->linear, memcpy);
    
    if(wave->validUpdate < wave->length)
    {
        wave->validUpdate++;
        canoeWaveDisplayUpdateViewRange(wave->display, 0, (int)wave->validUpdate);
    }
    
    return wave;
}

CanoeRealtimeWave *canoeRealtimeInit(CanoeRealtimeWave *wave, CanoeWaveDisplay *display, size_t length)
{
    wave->length = length;
    wave->validUpdate = 0;
    wave->linear = malloc(sizeof(float) * length);
    CB_INIT(&wave->buffer, float, (int)length, malloc);
    
    memset(wave->linear, 0, sizeof(float) * length);
    
    wave->display = display;
    canoeWaveDisplaySetDataBuffer(wave->display, wave->linear);
    canoeWaveDisplayUpdateViewRange(wave->display, 0, (int)length);
    
    return wave;
}

void *renderThread(void *ptr)
{
    while (1) {
        canoeStartRenderLoop(&rootWindowContainer);
    }
    return NULL;
}

uint32_t hash( uint32_t a)
{
    a = (a+0x7ed55d16) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3); // <<和 +的组合是可逆的
    a = (a^0xb55a4f09) ^ (a>>16);
    return a;
}

void setupMFD(void)
{
    CanoeWindow *mfd = canoeWindowNewSize("MFD", canoeVector2f(600, 300));
    
    CanoeADI *adi = canoeAdiNew(canoeVector2f(0, 0), canoeVector2f(300, 300));
    canoeWindowAddChild(mfd, adi);
    
    CanoeFlightMap *map = canoeFlightMapNew(canoeVector2f(300, 0));
    canoeWindowAddChild(mfd, map);
    
    SV_PUSH_BACK(&rootWindowContainer, mfd);
}

int main(void)
{
    sharedFontInit();
    canoeEventManagerInit();
    canoeRendererInit();
    
    
    
//    //setupUI();
//
////    display->buffer = malloc(sizeof(float) * 3000);
////    display->startIndex = 0;
////    display->endIndex = 3000;
//    
////    for(int i = 0; i < 3000; i++)
////        display->buffer[i] = sinf((float)i / 100.0);
//    
//    
    SV_INIT(&rootWindowContainer, 2, CanoeWindow *);
    
    setupMFD();
//    
//    //CanoeWindow *w = canoeWindowNewDefault("1st");
//    CanoeWindow *w2 = canoeWindowNewSize("2nd", canoeVector2f(800,400));
//    
//    //label = canoeLabelNew(canoeVector2f(10, 200), "aaaaa");
//    label2 = canoeLabelNew(canoeVector2f(100, 100), "asdasdssad");
//    button = canoeButtonNew(canoeVector2f(10, 10), "test butt");
//    
//    //canoeWindowAddChild(w, label);
//    canoeWindowAddChild(w2, label2);
//    canoeWindowAddChild(w2, button);
//
//    //SV_PUSH_BACK(&rootWindowContainer, w);
//    SV_PUSH_BACK(&rootWindowContainer, w2);
//
//    
//    canoeRegisterEvent(button, CANOE_EVENT_MOUSE_BUTTON_ACTION, test1);
//    canoeRegisterEvent(button, CANOE_EVENT_MOUSE_BUTTON_ACTION, test2);
//    
//    
//    CanoeLayer *layer = CANOE_NEW(Layer)(canoeVector2f(100, 100));
//    
//    CanoeLabel *layerLabel = CANOE_NEW(Label)(canoeVector2f(10, 10), "Layer test");
//    canoeLayerAddChild(layer, layerLabel);
//    
//    canoeWindowAddChild(w2, layer);
//    
//    canoeLayerSetLocation(layer, canoeVector2f(230, 140));
//    
//    
//    Canoe3DView *c3d = canoe3dViewNew(canoeVector2f(10, 120), canoeVector2f(180, 200));
//    canoeWindowAddChild(w2, c3d);
//    
//    CanoeADI *adi = canoeAdiNew(canoeVector2f(10, 80), canoeVector2f(300, 300));
//    canoeWindowAddChild(w2, adi);
//    
//    CanoeFlightMap *map = canoeFlightMapNew(canoeVector2f(350, 80));
//    canoeWindowAddChild(w2, map);
//    
//    
//    char testMessage[] = "LYNNFIELD, MA—In an effort to provide sanctuary for\n Lynnfield College students exposed to perspectives different from their own, a new campus safe space was dedicated Wednesday in honor of Alexis Stigmore, a 2009 graduate who felt kind of weird in class one time.";
//    
//    VECTOR(char *) lines;
//    SV_INIT(&lines, 2, char *);
//    
//    int lastPos = 0;
//    
//    for(int i = 0; i < strlen(testMessage); i++)
//    {
//        if(testMessage[i] == '\n')
//        {
//            char *line = malloc(i - lastPos + 1);
//            memcpy(line, testMessage + lastPos, i - lastPos);
//            lastPos = i;
//            
//            SV_PUSH_BACK(&lines, line);
//        }
//    }
//    
//    char *line = malloc(strlen(testMessage) - lastPos + 1);
//    memcpy(line, testMessage + lastPos, strlen(testMessage) - lastPos);
//    SV_PUSH_BACK(&lines, line);
    
    //return 0;

//
//        __block int count = 0;
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            while (1)
//            {
//
//                count++;
//                usleep(10000);
//    
//                adi->airSpeed += 0.1;
//                adi->altitude += 1;
//                printf("%f\n", adi->altitude);
//                //canoeLabelSetTitle(label2, test);
//            }
//    
//        });
    
    
    
//    CanoeRealtimeWave realtime2;
//    canoeRealtimeInit(&realtime2, display, 3000);
//
//    canoeWaveDisplaySetMinMax(display, -1.5, 1.5);
//    canoeWaveDisplaySetTitle(display, "Signal - F(X)");
//    
    __block int count = 0;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        while (1)
        {
            count++;
            
            if(count > 3600)
                count = 0;
            
            //map->heading = count / 10.0;
            //adi->yaw = count / 10.0;
            
            usleep(10000);
        }
        
    });
    
//    pthread_t thread;
//    pthread_create(&thread, NULL, renderThread, NULL);
//    //pthread_join(thread, NULL);
//    
//    while (1) {
//        sleep(1);
//        glfwPollEvents();
//    }


    canoeStartRenderLoop(&rootWindowContainer);
}
