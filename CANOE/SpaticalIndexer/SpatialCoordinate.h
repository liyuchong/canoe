/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef __SPATIAL_COORDINATE_H__
#define __SPATIAL_COORDINATE_H__

#include <stdbool.h>
#include <assert.h>

typedef struct
{
    float *lower, *upper;
    int dimension;
} SpatialBoundary;

/**
 *  Create a new spatial coordinate and set the coordinates
 *
 *  @param dimension Dimension of the spatial coordinate. Followed by the
 *         coordinate values.
 *         Example: 3-dimensional (x1, y1, z1, x2, y2, z2)
 *
 *  @return The spatial coordinate instance
 */
SpatialBoundary spatialBoundaryNew(int dimension, ...);

/**
 *  Destroy the memory of the given spatial coordinate instance
 *
 *  @param coordinate A pointer to the spatial coordinate
 */
void spatialBoundaryDestroy(SpatialBoundary *coordinate);

/**
 *  Test if the given spatial rectangle intersects with the boundary
 *
 *  @param rect     The pointer to the rectangle boundary
 *  @param boundary The pointer to the boundary
 *
 *  @return True if intersected, false otherwise
 */
bool spatialPointCollide(SpatialBoundary *rect, SpatialBoundary *boundary);

#endif /* defined(__SPATIAL_COORDINATE_H__) */
