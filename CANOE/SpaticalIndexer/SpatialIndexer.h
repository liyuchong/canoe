/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#ifndef CANOE_SPATIAL_INDEXER_H__
#define CANOE_SPATIAL_INDEXER_H__

#include <stdio.h>

#include <vector.h>

#include "CanoeFoundation.h"
#include "SpatialCoordinate.h"

/* data type stored in the spatial node */
typedef void * SpatialNodeData;

/* spatial node structure */
typedef struct
{
    SpatialBoundary boundary;
    SpatialNodeData data;
} SpatialNode;

/* spatial node container */
SV_DECLARE(SpatialNodes, SpatialNode *);

/* spatial indexer structure */
typedef struct
{
    int dimension;
    
    SpatialNodes nodes;
    
} SpatialIndexer;

/**
 *  Initialize a spatial node
 *
 *  @param node     The pointer to the spatial node
 *  @param boundary The boundary of the spatial node
 *  @param data     The data inside the spatial node
 *
 *  @return The pointer to the input spatial node
 */
SpatialNode *spatialNodeInit(SpatialNode *node, SpatialBoundary boundary, SpatialNodeData data);

/**
 *  Create a new spatial indexer
 *
 *  @param dimension The dimension of the space
 *
 *  @return A pointer to the new spatial indexer
 */
SpatialIndexer *spatialIndexerNew(int dimension);

/**
 *  Initialize a spatial indexer
 *
 *  @param indexer   The pointer to the spatial indexer
 *  @param idmension The dimension of the spatial indexer
 *
 *  @return The pointer to the input spatial indexer
 */
SpatialIndexer *spatialIndexerInit(SpatialIndexer *indexer, int dimension);

/**
 *  Add a new node to the spatial indexer
 *
 *  @param indexer The pointer to the spatial indexer instance
 *  @param node    The pointer to the spatial node instance
 *
 *  @return The pointer to the input spatial indexer instance
 */
SpatialIndexer *spatialIndexerAddElement(SpatialIndexer *indexer, SpatialNode *node);

/**
 *  Search the spatial indexer
 *
 *  @param indexer  The pointer to the spatial indexer
 *  @param boundary The pointer to the search boundary
 *
 *  @return A container of intersected nodes
 */
SpatialNodes *spatialIndexerSearch(SpatialNodes *result, SpatialIndexer *indexer, SpatialBoundary *boundary);

/**
 *  Search the given node in the spatial indexer 
 *  and remove it... move it move it!!
 *
 *  @param indexer The pointer to the spatial indexer
 *  @param node    The node which will be removed
 *
 *  @return If the given node is found in the indexer, returns a pointer to the
 *          input node. Returns NULL otherwise
 */
SpatialNode *spatialIndexerRemoveElement(SpatialIndexer *indexer, SpatialNode *node);


SpatialNode *spatialIndexerUpdateBoundary(SpatialNode *node, SpatialBoundary newBoundary);

SpatialNode *spatialIndexerQueryBoundary(SpatialIndexer *indexer, SpatialNode *node);



#endif /* defined(CANOE_SPATIAL_INDEXER_H__) */
