/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "SpatialCoordinate.h"

SpatialBoundary spatialBoundaryNew(int dimension, ...)
{
    SpatialBoundary coordinate = {
        .dimension = dimension,
        .lower = malloc(sizeof(float) * dimension),
        .upper = malloc(sizeof(float) * dimension)
    };
    
    va_list args;
    va_start(args, dimension);
    
    /* assign coordinate values */
    for(int i = 0; i < dimension; i++)
        coordinate.lower[i] = va_arg(args, double);
    
    for(int i = 0; i < dimension; i++)
        coordinate.upper[i] = va_arg(args, double);
    
    va_end(args);

    return coordinate;
}

void spatialBoundaryDestroy(SpatialBoundary *coordinate)
{
    if(coordinate)
    {
        if(coordinate->lower)
            free(coordinate->lower);
        if(coordinate->upper)
            free(coordinate->upper);
        
        coordinate->lower = NULL;
        coordinate->upper = NULL;
    }
}

bool spatialPointCollide(SpatialBoundary *rect, SpatialBoundary *boundary)
{
    assert(rect->dimension == boundary->dimension);
    
    for(int i = 0; i < boundary->dimension; i++)
    {
        if(boundary->lower[i] > rect->upper[i] ||
           boundary->upper[i] < rect->lower[i])
            return false;
    }
    
    return true;
}
