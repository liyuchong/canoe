/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "SpatialIndexer.h"

SpatialNode *spatialNodeInit(SpatialNode *node, SpatialBoundary boundary, SpatialNodeData data)
{
    node->boundary = boundary;
    node->data = data;
    
    return node;
}

SpatialIndexer *spatialIndexerNew(int dimension)
{
    SpatialIndexer *indexer = malloc(sizeof(SpatialIndexer));
    
    if(!indexer)
    {
        fprintf(stderr, "Spatial indexer: error: out of memory\n");
        return NULL;
    }
    
    spatialIndexerInit(indexer, dimension);
    
    return indexer;
}

SpatialIndexer *spatialIndexerInit(SpatialIndexer *indexer, int dimension)
{
    indexer->dimension = dimension;
    SV_INIT(&indexer->nodes, 2, SpatialNode *);
    
    return indexer;
}

SpatialIndexer *spatialIndexerAddElement(SpatialIndexer *indexer, SpatialNode *node)
{
    SV_PUSH_BACK(&indexer->nodes, node);
    
    return indexer;
}

SpatialNodes *spatialIndexerSearch(SpatialNodes *result, SpatialIndexer *indexer, SpatialBoundary *boundary)
{
    for(size_t i = 0; i < SV_SIZE(&indexer->nodes); i++)
    {
        if(spatialPointCollide(&SV_AT(&indexer->nodes, i)->boundary, boundary))
        {
            SV_PUSH_BACK(result, SV_AT(&indexer->nodes, i));
        }
    }

    return result;
}

SpatialNode *spatialIndexerUpdateBoundary(SpatialNode *node, SpatialBoundary newBoundary)
{
    spatialBoundaryDestroy(&node->boundary);
    node->boundary = newBoundary;
    
    return node;
}

SpatialNode *spatialIndexerRemoveElement(SpatialIndexer *indexer, SpatialNode *node)
{
    for(size_t i = 0; i < SV_SIZE(&indexer->nodes); i++)
    {
        if(spatialPointCollide(&SV_AT(&indexer->nodes, i)->boundary, &node->boundary))
        {
            if(SV_AT(&indexer->nodes, i)->data == node->data)
            {
                SV_ERASE_AT(&indexer->nodes, i);
                
                return node;
            }
        }
    }
    
    return NULL;
}
